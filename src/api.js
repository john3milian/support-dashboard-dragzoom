// First we need to import axios.js
import axios from "axios";
// Next we make an 'instance' of it
const API = axios.create({
  // .. where we make our configurations
  // baseURL: "http://localhost:2380",
  baseURL: "http://139.59.20.84:2385",
});

// Where you would set stuff like your 'Authorization' header, etc ...
API.defaults.headers.common["Authorization"] =
  "Bearer " + localStorage.getItem("authToken");

// Also add/ configure interceptors && all the other cool stuff

export default API;

// errorMsg === "Not Authorized to view this Page!!" ? (
// <NotAuthorized />
// ) : (
//   <Alert severity='error'>{errorMsg}</Alert>
// )
