import React, {useState} from 'react'
import {addNewUser} from '../../helpers/adminHelper'
import AdminAlert from '../admin/AdminAlert'

import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';

import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';


const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
        width: '25ch',
        flexGrow: 1
      },
    },
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[15],
      padding: theme.spacing(2, 4, 3),
      minWidth: 540,
      margin: 'auto'
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 440,
      marginTop: theme.spacing(2),
    },
    selectEmpty: {
      marginTop: theme.spacing(4),
    },
  }));

const AdminModal = ({open, handleClose, getAllUsers, setUsers, setAlert, changeAlert, setAlertType, users, setAlertMessage}) => {
    const classes = useStyles();

    const [email, setEmail] = useState('')
    const [Access, setAccess] = useState('')
    const [error, setError] = useState(false);
    const [errorMsg, setErrorMsg] = useState("");

    const userCred = []
    
    users.map(user => userCred.push(user.Cred))

    const handleEmail = (event) => {
      setEmail(event.target.value + '@dozee.io');
    }
    const handleAccess = (event) => {
      setAccess(event.target.value);
    }
    const onClickCancel = () => {
      handleClose()
      setEmail('')
      setAccess('')
    }
   const handleSubmit = (event) => {
    event.preventDefault();  
      addNewUser(Access, email, setError, setErrorMsg)
      handleClose()
      setEmail('')
      setAccess('')
      if(error) {
        setAlertType('serverError')
        setAlertMessage(errorMsg)
      } else {
        setAlertType('success')
      }
      setAlert(true)
      changeAlert()
      getAllUsers(setUsers)
    }  


    return (
        <Modal
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
        <Paper className = {classes.paper}>
          {userCred.includes(email) ? <AdminAlert type = {'existingUser'} /> : null}
          <h4>
            <PersonAddIcon></PersonAddIcon> New User 
            <IconButton style = {{marginLeft : 300}} color="secondary" aria-label="Close Modal" onClick = {onClickCancel}>
              <CloseIcon />
            </IconButton>
          </h4>
          <form onSubmit = {handleSubmit}>
            <FormControl className={classes.formControl} onSubmit = {handleSubmit}>
              <TextField
                label="E-Mail"
                id="outlined-start-adornment"
                onChange={handleEmail}
                // className={clsx(classes.margin, classes.textField)}
                InputProps={{
                  endAdornment: <InputAdornment position="end">@dozee.io</InputAdornment>,
                }}
                variant="outlined"
              />
              <Select
                value={Access}
                onChange={handleAccess}
                displayEmpty
                className={classes.selectEmpty}
                inputProps={{ 'aria-label': 'Without label' }}
              >
                <MenuItem value="validation">validation</MenuItem>
                <MenuItem value="Support Team">Support Team</MenuItem>
                <MenuItem value="Data Science">Data Science</MenuItem>
                <MenuItem value="Operations - RCSE">Operations-RCSE</MenuItem>
                <MenuItem value="Operations - Field">Operations-Field</MenuItem>
                <MenuItem value="Operations - Alerts">Operations-Alerts</MenuItem>
                <MenuItem value="Hardware Team">Hardware Team</MenuItem>
                <MenuItem value="Software Development">Software Development</MenuItem>
                <MenuItem value="Software Admin">Software Admin</MenuItem>
                <MenuItem value="Management">Management</MenuItem>
                <MenuItem value="Product">Product</MenuItem>
              </Select>
              <FormHelperText>Choose the Access Level for the New User</FormHelperText>
              <div>
                <Button type = 'submit' disabled = {email.length < 1 || userCred.includes(email) || Access.length < 1} 
                style = {{marginTop: 21}} variant="contained" color="Primary" >
                  Submit
                </Button>
                <Button type = 'button' style = {{marginTop: 21, marginLeft: 15}} onClick= {onClickCancel} variant="contained" color="Secondary">
                  Cancel
                </Button>
              </div>
            </FormControl>
          </form>
        </Paper>
        </Fade>
      </Modal>
    )
}

export default AdminModal
