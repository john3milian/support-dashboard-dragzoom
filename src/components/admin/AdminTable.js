import React, {useState} from 'react'
import {deleteUser} from '../../helpers/adminHelper'

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
    table: {
      maxWidth: 1080,
    },
  }));

const AdminTable = ({ users, sortOrder, getAllUsers, setUsers, setAlert, changeAlert, setAlertType, search, setAlertMessage}) => {

  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const removeUser = (user) => {
    deleteUser(user.Cred, setError, setErrorMsg)
    if(error) {
      setAlertType('serverError')
      setAlertMessage(errorMsg)
    } else {
      setAlertType('error')
    }
    setAlert(true)
    changeAlert()
    getAllUsers(setUsers)
   }

  const display = () => {
    if (search.length >= 1){
      return (users.map((user, i) => {
        return user.Cred.slice(0, search.length) === search ?
        <TableRow key={`row-${i}`}>
            <TableCell align="Left">{user.Access}</TableCell>
            <TableCell align="Left">{user.Cred}</TableCell>
            <TableCell align="Left">
            <Button variant="outlined" color="secondary" onClick={() => removeUser(user)}>
                Remove
            </Button>
            </TableCell>
        </TableRow> : null
        }))
    } else if (sortOrder == 'All') {
      return (users.map((user, i) => 
              (<TableRow key={`row-${i}`}> 
                  <TableCell align="Left">{user.Access}</TableCell>
                  <TableCell align="Left">{user.Cred}</TableCell>
                  <TableCell align="Left">
                  <Button variant="outlined" color="secondary" onClick={() => removeUser(user)}>
                      Remove
                  </Button>
                  </TableCell>
              </TableRow>)))
    } else {
      return (users.map((user, i) => {
        return user.Access === sortOrder ?
        <TableRow key={`row-${i}`}>
            <TableCell align="Left">{user.Access}</TableCell>
            <TableCell align="Left">{user.Cred}</TableCell>
            <TableCell align="Left">
            <Button variant="outlined" color="secondary" onClick={() => removeUser(user)}>
                Remove
            </Button>
            </TableCell>
        </TableRow> : null
        }))
    }}

  const classes = useStyles();

  return (
      <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="Left">Access Level</TableCell>
            <TableCell align="Left">Credentials</TableCell>
            <TableCell align="Left"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {display()}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default AdminTable
