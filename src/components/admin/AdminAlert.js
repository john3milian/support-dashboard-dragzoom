import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
  }));

const AlertSelection = (type, message) => {
  if(type === 'success') {
    return <Alert severity="success">The User was Added Successfully</Alert>
  } else if(type === 'error') {
    return <Alert severity="error">The User was Removed Successfully</Alert>
  } else if(type === 'existingUser') {
    return <Alert severity="error">The User Already Exists.</Alert>
  } else if(type === 'serverError') {
    return <Alert severity="error">{message}</Alert>
  }
}

const AdminAlert = ({type, message}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {AlertSelection(type, message)}
    </div>
  )
}

export default AdminAlert
