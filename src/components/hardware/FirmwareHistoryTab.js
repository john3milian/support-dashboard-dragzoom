import React from "react";

function FirmwareHistoryTab() {
  return (
    <div className='d-flex justify-content-center '>
      <table className='table table-striped w-75'>
        <thead>
          <tr>
            <th>Timestamp</th>
            <th>Pushed FW</th>
            <th>Current FW</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>2021-10-05 12:00:00</td>
            <td>3.1.102</td>
            <td>3.2.2</td>
          </tr>
          <tr>
            <td>2021-10-10 12:00:00</td>
            <td>3.1.23</td>
            <td>3.2.12</td>
          </tr>
          <tr>
            <td>2021-09-23 12:00:00</td>
            <td>3.1.11</td>
            <td>3.2.20</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}

export default FirmwareHistoryTab;
