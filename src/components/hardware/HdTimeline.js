import React from "react";
import Timeline from "@material-ui/lab/Timeline";
import TimelineItem from "@material-ui/lab/TimelineItem";
import TimelineSeparator from "@material-ui/lab/TimelineSeparator";
import TimelineConnector from "@material-ui/lab/TimelineConnector";
import TimelineContent from "@material-ui/lab/TimelineContent";
import TimelineDot from "@material-ui/lab/TimelineDot";
import { TimelineOppositeContent } from "@material-ui/lab";
import { Paper, styled, Typography } from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";

function HdTimeline({ logData }) {
  return (
    <Timeline className='mt-3'>
      {logData.map((eachLog, index) => (
        <TimelineItem>
          <TimelineOppositeContent>
            <Typography color='textSecondary'>
              {eachLog["@timestamp"]}
            </Typography>
          </TimelineOppositeContent>
          <TimelineSeparator>
            <TimelineDot />
            {index !== logData.length - 1 && <TimelineConnector />}
          </TimelineSeparator>
          <TimelineContent>
            <Typography color='successMain'>
              {eachLog["DozeeStatus"] || "--"}
            </Typography>
          </TimelineContent>
        </TimelineItem>
      ))}
    </Timeline>
  );
}
export function DiagnosticsTimeline({ diagnosticsData }) {
  return (
    <Timeline className='mt-3'>
      {diagnosticsData.map((eachLog, index) =>
        eachLog.correct ? (
          <TimelineItem>
            <TimelineOppositeContent>
              <CustomPaper elevation={2}>
                <Typography color='textPrimary'>{eachLog.key}</Typography>
                <Typography color='textSecondary'>{eachLog.value}</Typography>
              </CustomPaper>
            </TimelineOppositeContent>
            <TimelineSeparator>
              <TimelineDot className='bg-success'>
                <CheckIcon />
              </TimelineDot>
              {index !== diagnosticsData.length - 1 && <TimelineConnector />}
            </TimelineSeparator>
            <TimelineContent></TimelineContent>
          </TimelineItem>
        ) : (
          <TimelineItem>
            <TimelineOppositeContent></TimelineOppositeContent>
            <TimelineSeparator>
              <TimelineDot color='secondary'>
                <CloseIcon />
              </TimelineDot>
              {index !== diagnosticsData.length - 1 && <TimelineConnector />}
            </TimelineSeparator>
            <TimelineContent>
              <CustomPaper elevation={2}>
                <Typography color='textPrimary'>{eachLog.key}</Typography>
                <Typography color='textSecondary'>{eachLog.value}</Typography>
              </CustomPaper>
            </TimelineContent>
          </TimelineItem>
        )
      )}
    </Timeline>
  );
}

const CustomPaper = styled(Paper)({
  padding: "10px",
});

export default HdTimeline;
