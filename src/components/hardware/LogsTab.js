import {
  AppBar,
  Box,
  Button,
  Divider,
  FormControl,
  FormControlLabel,
  FormLabel,
  LinearProgress,
  Radio,
  RadioGroup,
  Tab,
  Tabs,
  TextField,
  Typography,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import HdTimeline from "./HdTimeline";
import MyLineChart from "./HardwareGraph";
import HardwareTable from "./HardwareTable";
import React, { Suspense } from "react";
import NotAuthorized from "../central/NotAuthorized";

function LogsTab({
  noOfDays,
  handleDaysChange,
  error,
  showLoader,
  showLogsComponent,
  logData,
  errorMsg,
}) {
  const LazyTimeline = React.lazy(() => import("./HdTimeline"));
  return (
    <>
      <div className='day_selector'>
        <FormControl component='fieldset'>
          <RadioGroup
            style={{ flexDirection: "row" }}
            // aria-label='gender'
            name='gender1'
            value={noOfDays}
            onChange={(e) => handleDaysChange(e)}>
            <FormControlLabel
              value='1'
              control={<Radio color='primary' />}
              label='1 Day'
            />
            <FormControlLabel
              value='7'
              control={<Radio color='primary' />}
              label='7 Days'
            />
          </RadioGroup>
        </FormControl>
      </div>
      <Divider className='mb-2 ' />
      {!error ? (
        <>
          <div className='see_which_component d-none'>
            <Button variant='outlined' color='primary'>
              Logs
            </Button>
            <Button variant='outlined' color='primary'>
              Diagnostics
            </Button>
          </div>
          {!showLoader && showLogsComponent && (
            <div
              className='mt-4'
              style={{ display: "flex", justifyContent: "center" }}>
              <MyLineChart logs={logData} />
            </div>
          )}
          {showLoader && showLogsComponent ? (
            <div className='mt-4'>
              <LinearProgress />
            </div>
          ) : (
            <>
              <Suspense fallback={<div>Loading ...</div>}>
                <LazyTimeline logData={logData} />
              </Suspense>
            </>
          )}
        </>
      ) : errorMsg === "Not Authorized to view this Page!!" ? (
        <NotAuthorized />
      ) : (
        <Alert severity='error'>{errorMsg}</Alert>
      )}
    </>
  );
}

export default LogsTab;
