import React from "react";
import { Line } from "react-chartjs-2";
// import zoomPlugin from "chartjs-plugin-zoom";

export default function MyLineChart({ logs }) {
  const data = {
    labels: [""].concat(logs.map((a) => a.time)),
    // labels: allLabels,
    datasets: [
      {
        label: "RSSI",
        data: [null].concat(
          logs.map((a) => (a.WiFiStatus === undefined ? null : a.WiFiStatus))
        ),
        fill: false,
        showLabel: false,
        pointRadius: 2,
        fontColor: "white",
        pointHitRadius: 10,
        lineTension: 0,
        backgroundColor: "rgba(75,192,192,0.4)",
        borderColor: "rgba(75,192,192,1)",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        // pointBorderColor: "rgba(75,192,192,1)",
        // pointBackgroundColor: "#fff",
        pointBorderWidth: 3,
        pointHoverRadius: 3,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,

        // borderColor: "rgba(75,192,192,1)",
      },
      {
        label: "Upload Queue",
        data: [null].concat(
          logs.map((a) => (a.UploadQueue === undefined ? null : a.UploadQueue))
        ),
        fill: false,
        borderColor: "#3399FF",
        // borderColor: "#742774",
        showLabel: false,
        pointRadius: 2,
        fontColor: "white",
        pointHitRadius: 10,
        lineTension: 0.1,
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        // pointBorderColor: "rgba(75,192,192,1)",
        // pointBackgroundColor: "#fff",
        pointBorderWidth: 3,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 2,
      },
      {
        label: "PON",
        data: [null].concat(
          logs.map((a) => (a.PowerStatus === undefined ? null : a.PowerStatus))
        ),
        fill: false,
        showLabel: false,
        pointRadius: 2,
        fontColor: "white",
        pointHitRadius: 1,
        lineTension: 0.05,
        angle: 180,
        steppedLine: "after",
        backgroundColor: "#FF0080",
        borderColor: "#FF0080",
        borderCapStyle: "butt",
        borderDash: [],
        borderDashOffset: 0.0,
        borderJoinStyle: "miter",
        // pointBorderColor: "rgba(75,192,192,1)",
        // pointBackgroundColor: "#fff",
        pointBorderWidth: 3,
        pointHoverRadius: 3,
        pointHoverBackgroundColor: "rgba(75,192,192,1)",
        pointHoverBorderColor: "rgba(220,220,220,1)",
        pointHoverBorderWidth: 2,

        // borderColor: "rgba(75,192,192,1)",
      },
    ],
  };

  const options = {
    showLabel: false,
    // plugins: [ChartDataLabels],
    labels: {
      // fontColor: 'rgb(255, 99, 132)',
      usePointStyle: true,
    },
    plugins: {
      // Change options for ALL labels of THIS CHART
      datalabels: {
        color: "transparent",
      },
      // zoom: {
      //   pan: {
      //     enabled: true,
      //     mode: "xy",
      //   },
      //   zoom: {
      //     enabled: true,
      //     mode: "xy",
      //   },
      // },
    },
    tooltips: {
      enabled: true,
      // callbacks: {
      //   label: function (tooltipItem) {
      //     console.log(tooltipItem);
      //     return tooltipItem.yLabel;
      //   },
      // },
    },
    scales: {
      xAxes: [
        {
          afterTickToLabelConversion: function (data) {
            var xLabels = data.ticks;

            xLabels.forEach(function (labels, i) {
              if (i % 2 === 1) {
                xLabels[i] = "";
              }
            });
          },
        },
      ],
      yAxes: [
        {
          type: "linear",
          display: true,
          position: "left",
          id: "y-axis-1",
          gridLines: {
            display: false,
          },
          labels: {
            show: true,
          },
          ticks: {
            beginAtZero: false,
          },
        },
        // {
        //   type: "linear",
        //   display: true,
        //   position: "right",
        //   gridLines: {
        //     display: false,
        //   },
        //   id: "y-axis-3",
        //   labels: {
        //     show: false,
        //   },
        //   ticks: {
        //     beginAtZero: true,
        //   },
        // },
      ],
    },
  };

  const legend = {
    display: true,
    position: "bottom",
    // labels: {
    //   fontColor: "#323130",
    //   fontSize: 14,
    // },
  };
  return (
    <div
      className='App'
      style={{
        width: "95%",
      }}>
      <Line
        data={data}
        width={"100vw"}
        legend={legend}
        height={"30vh"}
        options={options}
      />
    </div>
  );
}
