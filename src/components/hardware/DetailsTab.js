import { Grid, Paper, TextField, Typography } from "@material-ui/core";

function DetailsTab() {
  return (
    <div className='d-flex flex-column  align-items-center justify-content-center'>
      <div className='w-50'>
        <Grid container spacing={2}>
          <Grid item md={2} />
          <Grid item md={3}>
            <Typography variant='h6'>Device Type</Typography>
          </Grid>
          <Grid item md={6}>
            <TextField
              fullWidth
              variant='outlined'
              size='small'
              style={{ outline: 0 }}
            />
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item md={2} />
          <Grid item md={3}>
            <Typography variant='h6'>Created At</Typography>
          </Grid>
          <Grid item md={6}>
            <TextField
              fullWidth
              variant='outlined'
              size='small'
              style={{ outline: 0 }}
            />
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item md={2} />
          <Grid item md={3}>
            <Typography variant='h6'>Device Tags</Typography>
          </Grid>
          <Grid item md={6}>
            <TextField
              fullWidth
              multiline
              rows={3}
              rowsMax={3}
              variant='outlined'
              size='small'
              style={{ outline: 0 }}
            />
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid item md={2} />
          <Grid item md={5} />
          <Grid item md={2}>
            <button className='btn btn-warning w-100'>Edit</button>
          </Grid>
          <Grid item md={2}>
            <button className='btn btn-success w-100'>Submit</button>
          </Grid>
        </Grid>

        <div className='mt-4' />
        <table class='table table-primary table-bordered'>
          <thead class=''>
            <tr>
              <th scope='col' colSpan='3'>
                {" "}
                Pairing History
              </th>
            </tr>
          </thead>
          <tbody className='bg-white'>
            <tr>
              <th scope='col'>UserId</th>
              <th scope='col'>Paired At</th>
              <th scope='col'>Unpaired At</th>
            </tr>
            <tr>
              <td scope='col'>abcd124</td>
              <td scope='col'>2018-02-28</td>
              <td scope='col'>2021-01-09</td>
            </tr>
            <tr>
              <td scope='col'>def987</td>
              <td scope='col'>2019-08-13</td>
              <td scope='col'>2020-03-19</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default DetailsTab;
