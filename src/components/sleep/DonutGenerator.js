import React from "react";
import { Doughnut } from "react-chartjs-2";
import "chartjs-plugin-datalabels";
import "chartjs-plugin-doughnutlabel";

const DonutGenerator = ({ sleepData }) => {
  const [light, deep, rem, awake] = [
    sleepData.Properties.LightDuration,
    sleepData.Properties.DeepDuration,
    sleepData.Properties.RemDuration,
    sleepData.Properties.AwakeDuration,
  ];

  const sleepScore = sleepData.Properties.Score;
  // console.log("durations: ", durations, "sleepScore: ", sleepScore);

  const scoreData = {
    light,
    deep,
    rem,
    awake,
  };

  const keys = getKeys(scoreData);
  const values = getValues(scoreData);

  const data = {
    labels: [...keys],
    datasets: [
      {
        pointStyle: "circle",
        // label: "Sales for 2020(M)",
        data: [...values],
        borderColor: ["#ffffff", "#ffffff", "#ffffff", "#ffffff"],
        backgroundColor: [
          "rgb(38,196,194)",
          "rgb(37,162,30)",
          "rgb(145,56,165)",
          "rgb(28,106,180)",
        ],
      },
    ],
  };

  const options = {
    aspectRatio: 4,
    responsive: true,
    plugins: {
      datalabels: {
        formatter: (value, k) => {
          return value + "m";
        },
        color: "#ffffff",
      },
      doughnutlabel: {
        labels: [
          {
            text: sleepScore, //sleep score goes here
            font: {
              size: 20,
              weight: "bold",
            },
          },
          {
            text: "Sleep score",
          },
        ],
      },
    },
    legend: {
      display: true,
      position: "bottom",
      labels: {
        fontColor: "#555",
        usePointStyle: true,
        padding: 20,
      },
    },
    title: {
      display: false,
      text: "donut",
      fontSize: 20,
    },
  };

  return Object.keys(sleepData.Properties).length ? (
    <Doughnut height={"20%"} width={"20%"} data={data} options={options} />
  ) : (
    <div className='' style={{ fontWeight: "400" }}>
      DID NOT SLEEP
    </div>
  );
};

export default DonutGenerator;

function getKeys(obj) {
  const keys = [];

  for (let key of Object.keys(obj)) {
    const capitalisedKey = key[0].toUpperCase() + key.slice(1);
    keys.push(capitalisedKey);
  }
  return keys;
}

function getValues(obj) {
  const values = [];

  for (let value of Object.values(obj)) {
    const adjustedVal = Math.round(value / 60);
    values.push(adjustedVal);
  }
  return values;
}
