import { Divider } from "@material-ui/core";
import React from "react";
import ProgressBar from "../ProgressBar";

const SleepAnalysisTable = ({ data }) => {
  // const {awakening, snoring, wakeUpAfterSleepOnset, stress, sleepLatency, bedtimeResistance, breath, circadianRhythm, deep, duration, heart, movement, rem, sleepEfficiency,  } = data;

  function generateTable() {
    return Object.keys(data)
      .filter((key) => validKeys(key))
      .map((item) => {
        return (
          <tr className='sleep-analysis-row' key={Math.random()}>
            <td className='sleep-analysis-title'>{rename(item)}</td>
            <td className='sleep-analysis-progress-bar'>
              <ProgressBar
                width={Math.round((data[item] / getMaxValue(item)) * 100) + "%"}
              />
            </td>
            <td className='sleep-analysis-score'>{data[item]}</td>
            <br></br>
          </tr>
        );
      });
  }

  function validKeys(key) {
    const keys = [
      "HeartPoints",
      "BreathPoints",
      "AwakeningPoints",
      "SnoringPoints",
      "WakeUpAfterSleepOnsetPoints",
      "StressPoints",
      "SleepLatencyPoints",
      "BedTimeResistancePoints",
      "CircadianRhythmPoints",
      "DeepPoints",
      "DurationPoints",
      "MovementPoints",
      "RemPoints",
      "SleepEfficiencyPoints",
    ];
    return keys.includes(key);
  }

  function rename(name) {
    const titles = {
      AwakeningPoints: "Awakening",
      SnoringPoints: "Snoring",
      WakeUpAfterSleepOnsetPoints: "Wakeup after sleep",
      StressPoints: "Stress",
      SleepLatencyPoints: "Sleep latency",
      BedTimeResistancePoints: "Bedtime resistance",
      BreathPoints: "Respiration",
      CircadianRhythmPoints: "Circadian rhythm",
      DeepPoints: "Deep sleep",
      DurationPoints: "Sleep duration",
      HeartPoints: "Heart",
      MovementPoints: "Movement",
      RemPoints: "REM",
      SleepEfficiencyPoints: "Efficiency",
    };

    return titles[name];
  }

  function getMaxValue(name) {
    const titles = {
      AwakeningPoints: 5,
      SnoringPoints: 5,
      WakeUpAfterSleepOnsetPoints: 5,
      StressPoints: 10,
      SleepLatencyPoints: 5,
      BedTimeResistancePoints: 5,
      BreathPoints: 10,
      CircadianRhythmPoints: 5,
      DeepPoints: 10,
      DurationPoints: 10,
      HeartPoints: 10,
      MovementPoints: 5,
      RemPoints: 10,
      SleepEfficiencyPoints: 5,
    };

    return titles[name];
  }
  return (
    <table className='sleep-analysis-tbl'>
      <tbody>{generateTable()}</tbody>
    </table>
  );
};

export default SleepAnalysisTable;
