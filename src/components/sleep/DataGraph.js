import moment from "moment";
import React, { useEffect } from "react";
import { Chart, Bar } from "react-chartjs-2";

function DataGraph({ plotdata, stagesList }) {
  const data = {
    labels: [""].concat(
      plotdata.map((a) => moment.utc(a.Timestamp).local().format("Do HH:mm"))
    ),
    datasets: Array.isArray(stagesList)
      ? [
          getBRDataset(plotdata),
          getHRDataset(plotdata),
          getStressDataset(plotdata),
          getRecoveryDataset(plotdata),
          getApneaDataset(plotdata),
          getArtifactsDataset(plotdata),
          getMovementDataset(plotdata),
          getStageDataset(stagesList),
        ]
      : [
          getBRDataset(plotdata),
          getHRDataset(plotdata),
          getStressDataset(plotdata),
          getRecoveryDataset(plotdata),
          getApneaDataset(plotdata),
          getArtifactsDataset(plotdata),
          getMovementDataset(plotdata),
          getStageDataset([]),
        ],
  };
  const legend = {
    display: true,
    position: "bottom",
  };
  const options = {
    default: {},
    animation: {
      duration: 0,
    },
    elements: {
      line: {
        tension: 0.2,
      },
      point: {
        radius: 0,
        hoverRadius: 6,
      },
    },
    tooltips: {
      // mode: 'x',
      mode: "index",

      intersect: false,
    },
    plugins: {
      crosshair: {
        line: {
          color: "#000000", // crosshair line color
          width: 5, // crosshair line width
        },
      },
      // zoom: {
      //   pan: {
      //     enabled: true,
      //     mode: "x",
      //   },
      //   zoom: {
      //     enabled: true,
      //     mode: "x",
      //   },
      // },
      datalabels: {
        display: false,
      },
    },
    responsive: true,
    // maintainAspectRatio: false,
    aspectRatio: 4,
    // aspectRatio: 2.75,
    title: {
      display: false,
      text: "Chart",
    },
    legend: {
      display: true,
      position: "top",
      labels: {
        // fontColor: 'rgb(255, 99, 132)',
        usePointStyle: true,
      },
    },
    scales: {
      // function getYAxis(id, position='right', show=true, min=undefined, max=undefined){
      // id, position='right', show=true, min=undefined, max=undefined, name

      yAxes: [
        getYAxis("stages", "left"),
        getYAxis("breath", "right", false, 0, 140),
        getYAxis("heart", "right", true, 0, 140),
        // getYAxis("breath"),
        // getYAxis("heart"),
        getYAxis("snores", "right", false),
        getYAxis("stress", "right"),
        getYAxis("recovery", "right", false),
        getYAxis("movement", "right", false),
        getYAxis("apnea", "right", false),
        getYAxis("artifacts", "right", false),
      ],
      //   xAxes: [getXAxis()],
      xAxes: [
        {
          afterTickToLabelConversion: function (data) {
            var xLabels = data.ticks;

            xLabels.forEach(function (labels, i) {
              if (i % 4 === 1) {
                xLabels[i] = "";
              }
            });
          },
        },
      ],
    },
  };

  useEffect(() => {
    Chart.pluginService.register({
      beforeDraw: function (chart, easing) {
        const ctx = chart.ctx;
        ctx.save();
        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.stroke();
        ctx.restore();
      },
      afterDraw: function (chart, easing) {
        if (chart.tooltip._active && chart.tooltip._active.length) {
          try {
            const activePoint = chart.controller.tooltip._active[0];
            const ctx = chart.ctx;
            const x = activePoint.tooltipPosition().x;
            const y = activePoint.tooltipPosition().y;
            const topY = chart.scales["stages"].top;
            const bottomY = chart.scales["stages"].bottom;
            const topX = chart.scales["x-axis-0"].left;
            const bottomX = chart.scales["x-axis-0"].right;
            // const heightOfCross = chart.scales["x-axis-0"].height;
            ctx.save();
            ctx.beginPath();
            ctx.moveTo(x, topY);
            ctx.lineTo(x, bottomY);
            ctx.moveTo(topX, y);
            ctx.lineTo(bottomX, y);
            ctx.strokeStyle = "#000000";
            ctx.strokeStyle = "#e23fa9";
            // ctx.gridLines = { color: "#000000", zeroLineColor: "#000000" };
            ctx.setLineDash([4]);
            ctx.lineWidth = 1;
            ctx.stroke();
            ctx.restore();
          } catch {
            const ctx = chart.ctx;
            ctx.save();
            ctx.beginPath();
            ctx.moveTo(0, 0);
            ctx.stroke();
            ctx.restore();
          }
        }
      },
    });
  }, []);
  return (
    <div>
      <Bar
        data={data}
        width={"100vw"}
        legend={legend}
        height={"30vh"}
        options={options}
      />
    </div>
  );
}

function getStageDataset(stagesData) {
  const flatStages = getData(stagesData);
  const stageValues = getValues(stagesData, "Stage");

  const stageColors = getStageColors(stagesData);
  const stageTimes = getTimes(flatStages);
  return {
    order: 12,
    yAxisID: "stages",
    type: "bar",
    label: "Stages",
    data: mergeData(stageTimes, stageValues),
    backgroundColor: [...stageColors],
    pointStyle: "circle",
    hidden: false,
  };
}

function getMovementDataset(vitals) {
  // const flatMovement = getData(movementData);

  const movementSet = vitals.map((a) => {
    if (a.Events.map((b) => b["Key"] === "ARF").includes(true)) {
      return 1;
    } else return 0;
  });
  const movementTimes = getTimes(vitals);

  return {
    order: 11,
    yAxisID: "movement",
    type: "bar",
    label: "Movement",
    data: mergeData(movementTimes, movementSet),
    // data: mergeData(movementTimes, movementValues),
    backgroundColor: new Array(movementSet.length).fill("rgb(225, 114, 77)"),
    pointStyle: "circle",
    hidden: true,
  };
}

function getApneaDataset(vitals) {
  const apneaSet = vitals.map((a) => {
    if (a.ApneaEpisodes === null) {
      return 0;
    } else {
      return 1;
    }
  });
  // const apneaValues = getValues(apneaSet, "APN");
  const apneaTimes = getTimes(vitals);
  // const replacedData = replaceData(apneaValues);

  return {
    order: 11,
    yAxisID: "apnea",
    type: "bar",
    label: "Apnea",
    data: mergeData(apneaTimes, apneaSet),
    backgroundColor: new Array(apneaSet.length).fill("rgb(255, 88, 244)"),
    pointStyle: "circle",
    hidden: true,
  };
}

function getArtifactsDataset(vitals) {
  // const artifactsSet = getSpecificSet(artifactsData, "ARF", "duration");
  // const artifactsValues = getValues(artifactsSet, "ARF");
  const artifactsSet = vitals.map((a) => {
    if (a.Events.map((b) => b["Key"] === "ARF").includes(true)) {
      return 1;
    } else return 0;
  });
  const artifactsTimes = getTimes(vitals);
  // const replacedData = replaceData(artifactsValues);

  return {
    order: 11,
    yAxisID: "artifacts",
    type: "bar",
    label: "Artifacts",
    data: mergeData(artifactsTimes, artifactsSet),
    backgroundColor: new Array(artifactsSet.length).fill("rgb(157, 188, 64)"),
    pointStyle: "circle",
    hidden: true,
  };
}

function getSnoreDataset(snoreData) {
  const snoreSet = getSpecificSet(snoreData, "SN", "duration");
  const snoreValues = getValues(snoreSet, "SN");
  const snoreTimes = getTimes(snoreSet);
  const replacedData = replaceData(snoreValues);

  return {
    order: 11,
    yAxisID: "snores",
    type: "bar",
    label: "Snores",
    data: mergeData(snoreTimes, replacedData),
    backgroundColor: new Array(snoreValues.length).fill("rgb(67, 67, 67)"),
    pointStyle: "circle",
    hidden: true,
  };
}

function getBRDataset(vitals) {
  //   const bRValues = getSpecificVitalsValues(vitals, "bR");
  const bRValues = vitals.map((a) =>
    a.BreathRate === -1 ? null : a.BreathRate === 0 ? 0 : a.BreathRate
  );
  const bRTimes = getTimes(vitals);
  return {
    showLabel: false,
    type: "line",
    label: "Breath",
    yAxisID: "breath",
    // data: bRValues,
    data: mergeData(bRTimes, bRValues),
    // borderColor: ["steelblue"],
    borderColor: ["rgb(141, 75, 212)"],
    // filter: "brightness(1.5)",
    // hoverBackgroundColor: ["rgba(254,22,49,1)"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 1.5,
    spanGaps: false,
    order: 1,
    hidden: false,
  };
}

function getHRDataset(vitals) {
  const hRValues = vitals.map((a) => (a.HeartRate === -1 ? null : a.HeartRate));
  const hRTimes = getTimes(vitals);

  return {
    type: "line",
    label: "Heart",
    yAxisID: "heart",
    data: mergeData(hRTimes, hRValues),
    // Old V
    // borderColor: ["rgb(208, 11, 11)"],
    // New V
    borderColor: ["rgb(255, 11, 11)"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 1.5,
    spanGaps: false,
    order: 2,
    hidden: true,
  };
}

function getStressDataset(vitals) {
  const stressValues = vitals.map((a) => (a.Stress === -1 ? null : a.Stress));
  const stressTimes = getTimes(vitals);

  return {
    type: "line",
    label: "Stress",
    yAxisID: "stress",
    data: mergeData(stressTimes, stressValues),
    // Old V
    borderColor: ["rgb(95, 194, 244)"],
    backgroundColor: ["rgba(153, 221, 255, 0.531)"],
    // New V
    // borderColor: ["rgb(95, 233, 244)"],
    // backgroundColor: ["rgba(95, 233, 244, 0.4)"],
    // backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 1.5,
    spanGaps: false,
    order: 3,
    hidden: true,
  };
}

function getRecoveryDataset(vitals) {
  const recoveryValues = vitals.map((a) =>
    a.Recovery === -1 ? null : a.Recovery
  );
  const recoveryTimes = getTimes(vitals);

  return {
    type: "line",
    label: "Recovery",
    yAxisID: "recovery",
    data: mergeData(recoveryTimes, recoveryValues),
    // borderColor: ["rgb(45, 135, 38)"],
    // backgroundColor: ["rgba(64, 135, 59, 0.493)"],
    // NEW V
    borderColor: ["rgb(45, 177, 38)"],
    backgroundColor: ["rgba(45, 177, 38, 0.4)"],
    // backgroundColor: ["rgba(255, 211, 227,.2)"],
    borderWidth: 1.5,
    spanGaps: false,
    order: 4,
    hidden: true,
  };
}

function mergeData(time, data) {
  const arr = [];

  for (let i = 0; i < time.length; i++) {
    const obj = {};
    obj.x = time[i];
    obj.y = data[i];
    arr.push(obj);
  }
  return arr;
}

function replaceData(set) {
  return new Array(set.length).fill(1);
}

function getYAxis(
  id,
  position = "right",
  show = true,
  min = undefined,
  max = undefined,
  name
) {
  return {
    scaleLabel: {
      display: show,
      labelString: name || id[0].toUpperCase() + id.slice(1),
    },
    id,
    // type: 'linear',
    position,
    ticks: {
      display: show,
      beginAtZero: true,
      min,
      max,
      step: 1,
      // padding: 10,
    },
    gridLines: {
      display: show,
      drawOnChartArea: false,
      color: "black",
      // display: false,
      // lineWidth: 1,
      // borderDash: [1]
      // borderWidth: 5
    },
  };
}

function getXAxis() {
  return {
    ticks: {
      maxTicksLimit: 20,
      display: true,
    },
    gridLines: {
      display: true,
      color: "black",
      drawOnChartArea: false,
    },
    type: "time",
    time: {
      displayFormats: {
        minute: "h:mm a",
      },
    },
    barThickness: "flex",
    maxBarThickness: 1.5,
  };
}

function getStageColors(data) {
  const arr = [];
  console.log(data);
  for (let item of data) {
    switch (item.Stage) {
      case 1:
        arr.push("#B6D8F2");
        break;
      case 2:
        arr.push("#F4CFDF");
        break;
      case 3:
        arr.push("#5784BA");
        break;
      case 4:
        arr.push("#CCD4BF");
        break;
      default:
        arr.push("#FFD9CF");
        break;
    }
  }
  return arr;
}

function getTimes(data) {
  const arr = [];

  for (let item of data) {
    const adjustedTime = parseInt(item.Timestamp) * 1000;
    arr.push(new Date(adjustedTime));
  }

  return arr;
}

function getData(data) {
  return data.flat();
}

function getValues(data, key) {
  const arr = [];
  for (let item of data) {
    arr.push(item[key]);
  }
  console.log(arr);
  return arr;
}
// function getValues(data, key, setMax=false){
// 	const arr = [];
// 	let max = -Infinity;
// 	for(let item of data){
// 		max = Math.max(max, item[key])
// 		arr.push(item[key])
// 	}
// 	if(setMax) setMax(max);
// 	return arr;
// }

function getSpecificVitalsValues(data, key) {
  const arr = [];

  for (let item of data) {
    if (item[key] === -1) {
      arr.push(NaN);
    } else {
      arr.push(item[key]);
    }
  }

  return arr;
}

function getSpecificSet(data, key, subkey) {
  const arr = [];

  for (let item of data) {
    if (item[key]) {
      const obj = {};
      // obj[key] = item[key];
      if (subkey) {
        obj[key] = item[key][subkey];
      } else {
        obj[key] = item[key];
      }
      obj.ts = item.ts;
      arr.push(obj);
    }
  }
  return arr;
}

export default DataGraph;
