import React, { useState } from "react";
import "../../css/SearchDate.css";
import "react-date-range/dist/styles.css";
import "react-date-range/dist/theme/default.css";
import { DateRangePicker } from "react-date-range";
import { PeopleAltTwoTone } from "@material-ui/icons";
import { Button, makeStyles, Paper } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import Modal from "@material-ui/core/Modal";

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

function SearchDate({
  setShowDates,
  specificDate,
  showDates,
  toDate,
  setToDate,
  fromDate,
  setFromDate,
}) {
  const history = useHistory();
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const selectionRange = {
    startDate: fromDate,
    endDate: toDate,
    key: "selection",
  };

  function handleSelect(ranges) {
    console.log(ranges);
    setFromDate(ranges.selection.startDate);
    setToDate(ranges.selection.endDate);
  }

  function handleSubmit() {
    if (fromDate < toDate) {
      setShowDates(false);
      specificDate();
    }
  }
  const classes = useStyles();
  return (
    <Modal open={showDates} style={{ overflow: "scroll" }}>
      <div className='searchDate d-flex'>
        <Paper className='p-4' variant='outlined'>
          <Paper elevation={5}>
            <DateRangePicker
              ranges={[selectionRange]}
              onChange={handleSelect}
            />
          </Paper>
          <div className='d-flex'>
            <Button
              fullWidth
              color='secondary'
              variant='contained'
              className='mt-2 mr-1'
              onClick={() => setShowDates(false)}>
              Cancel
            </Button>
            <Button
              fullWidth
              color='primary'
              variant='contained'
              className='mt-2 ml-1'
              onClick={handleSubmit}>
              Go
            </Button>
          </div>
        </Paper>
      </div>
    </Modal>
  );
}

export default SearchDate;
