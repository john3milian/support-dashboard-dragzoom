import { Button } from "@material-ui/core";
import React, { useState, useEffect } from "react";
// import Loader from "./Loader";
import "../css/OptionsDialog.css";
import API from "../api";

const OptionsDialog = (props) => {
  const [fromDate, setFromDate] = useState("");
  const [fromDateDisplay, setFromDateDisplay] = useState("");
  const [toDate, setToDate] = useState("");
  const [toDateDisplay, setToDateDisplay] = useState("");
  const [numSleeps, setNumSleeps] = useState(5);
  const [error, setError] = useState("");
  const DAY = 1000 * 60 * 60 * 24;
  const WEEK = DAY * 7;

  // allow to ony change numSleeps

  function withinRange() {
    return toDate.getTime() - fromDate.getTime() <= DAY * 30;
  }

  function removeMsAndFloor(time) {
    return Math.floor(time / 1000);
  }

  const handleSubmit = () => {
    // e.preventDefault();
    if (fromDate && toDate && fromDate < toDate && withinRange()) {
      callData();
    } else {
      errorMsg("Invalid range. Please select a valid range within 30 days.");
    }
  };

  useEffect(() => {
    console.log("fromDate: ", fromDate);
    console.log("toDate: ", toDate);
    console.log("numSleeps: ", numSleeps);
  }, [toDate, fromDate, numSleeps]);

  useEffect(() => {
    // console.log("fromDate: ", fromDate);
    // console.log(new Date(fromDate).to);
    //convert date to time, add numSleeps * DAY
    //store this as in setToDate
    //done
    let newToDate = new Date(fromDate).getTime();
    console.log("newToDate: ", newToDate);
    newToDate += DAY * numSleeps;
    console.log("new toDate to string", new Date(newToDate).toString());
    setToDate(new Date(newToDate));
    setToDateDisplay(new Date(newToDate).toString());
  }, [fromDate]);

  function errorMsg(msg) {
    setError(msg);
    setTimeout(() => {
      setError("");
    }, 1000);
    console.log(msg);
  }

  function callData() {
    // const days = 14;
    // const timeFrame = parseInt(1000*60*60*24*days);

    // const from = Math.floor((new Date(Date.now() - timeFrame).getTime())/1000);
    props.setStatus(false, false);
    props.setAllData([]);
    props.setIndexSelected(0);

    // const from = removeMsAndFloor(fromDate.getTime());
    // const to = removeMsAndFloor(toDate.getTime());
    const from = removeMsAndFloor(fromDate.getTime());
    const to = removeMsAndFloor(toDate.getTime());
    API.post(
      // "https://influx.dozee.me/api/v1/user/" + props.userId + "/",
      `/api/v1/sleep/list?userId=${props.userId}&fromTime=${from}&toTime=${to}`,
      {
        from,
        to,
        fields: ["sleepId", "WakeupTime", "SleepTime", "OnBedTime"],
      }
      // {
      //   method: "POST",
      //   headers: {
      //     "Content-Type": "application/json;charset=utf-8",
      //   },
      //   body: JSON.stringify({
      //     from,
      //     to,
      //     fields: ["sleepId", "WakeupTime", "SleepTime", "OnBedTime"],
      //   }),
      // }
    )
      .then((res) => res.data)
      .then((res) => {
        // console.log(res);
        const anotherRes = res;
        console.log(
          "This is another res",
          anotherRes.reverse().slice(0, { numSleeps })
        );
        if (!Array.isArray(res)) {
          throw new Error("No data");
        }
        if (res.length === 0) {
          throw new Error(`No data between ${from} and ${to}`);
        }
        const temp_res = [];
        anotherRes.map(({ SleepId, WakeupTime, SleepTime, Properties }) =>
          temp_res.push({
            sleepId: SleepId,
            WakeupTime: WakeupTime,
            SleepTime: SleepTime,
            OnBedTime: Properties["OnBedTime"],
          })
        );
        // res.reverse()
        // temp_res [ 7, 11 ]
        // temp_res.sort((a, b) => (a.OnBedTime < b.OnBedTime ? 1 : -1));
        const slicedList = temp_res
          .map(({ sleepId }) => sleepId)
          .slice(0, numSleeps);
        props.setSleepIdList(slicedList);
        console.log(slicedList);
        return slicedList.reverse();
      })
      .then((arr) => {
        console.log(arr);
        Promise.all([
          ...arr.map((id) =>
            API.get("/api/data/sleep?sleepid=" + id)
              .then((res) => res.data)
              .catch((err) => err)
          ),
        ]).then((res) => {
          props.setStatus(true, true);
          props.setAllData(res);
          // setFetching(false);
        });
      })
      .catch((err) => {
        console.log(err);
        props.setStatus(true, false);
        // setFetching(false);
      });
    props.toggleDisplay(!props.display);
  }

  return (
    <div
      className='options-container'
      style={{
        display: props.display ? "block" : "none",
      }}
      onClick={() => props.toggleDisplay(!props.display)}>
      <div className='options-form-div'>
        <form onSubmit={handleSubmit} onClick={(e) => e.stopPropagation()}>
          {error ? (
            <div
              style={{
                height: "100%",
                width: "100%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                textAlign: "center",
              }}>
              {error}
            </div>
          ) : (
            <>
              <div className=''>
                From{" "}
                <input
                  type='date'
                  value={fromDateDisplay}
                  onChange={(e) => {
                    const date = new Date(e.target.value.toString());
                    setFromDate(date);
                    setFromDateDisplay(e.target.value);
                  }}
                />
              </div>
              <div className=''>
                To{" "}
                <input
                  type='date'
                  value={toDateDisplay}
                  onChange={(e) => {
                    const date = new Date(e.target.value.toString());
                    setToDate(date);
                    setToDateDisplay(e.target.value);
                  }}
                />
              </div>
              <div id='num-sleeps-span'>
                Sleeps to load{" "}
                <input
                  type='number'
                  min='1'
                  max='14'
                  value={numSleeps}
                  onChange={(e) => {
                    const num = +e.target.value;
                    if (num > 0 && num < 10) {
                      setNumSleeps(num);
                    } else {
                      errorMsg("Please specifiy a range between 1 and 9");
                    }
                  }}
                />
              </div>
              <Button
                color='primary'
                onClick={handleSubmit}
                variant='contained'
                className='mt-2 mb-2'>
                Go
              </Button>
              <Button
                type='button'
                color='secondary'
                variant='contained'
                className='options-cancel-btn'
                onClick={() => props.toggleDisplay(!props.display)}>
                Cancel
              </Button>
            </>
          )}
        </form>
      </div>
    </div>
  );
};

export default OptionsDialog;
