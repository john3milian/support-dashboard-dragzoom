import React from "react";
import "../css/ProgressBar.css";

const ProgressBar = ({ width }) => {
  return (
    <div className='progress-bar-border'>
      <div
        className='progress-bar-inner'
        style={{ height: "15px", width }}></div>
    </div>
  );
};

export default ProgressBar;
