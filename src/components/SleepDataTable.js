import React from "react";

const SleepDataTable = ({ data }) => {
  function getSleepDuration(duration) {
    if (!duration) return "-";
    const totalMins = Math.round(duration / 60);
    const hours = Math.floor(totalMins / 60);
    const mins = totalMins % 60;

    if (mins < 10) {
      return hours + "h 0" + mins + "m";
    }
    return hours + "h " + mins + "m";
  }

  function getTime(bedTime) {
    if (!bedTime) return "-";
    const time = new Date(parseInt(bedTime) * 1000);
    const hours = time.getHours();
    const timeString = time.toString().split(" ")[4].slice(0, -3);
    if (hours < 12) {
      return timeString + " am";
    }
    return timeString + " pm";
  }

  return (
    <table className='sleep-data-tbl'>
      <tbody>
        <tr className='sleep-data-row'>
          <td className='sleep-data-key'>Sleep Duration</td>
          <td className='sleep-data-value'>
            <i className='fa fa-clock-o'></i>{" "}
            {getSleepDuration(data.durations.total)}
          </td>
        </tr>
        <tr className='sleep-data-row'>
          <td className='sleep-data-key'>On bed time</td>
          <td className='sleep-data-value'>
            <i className='fa fa-moon-o' /> {getTime(data.bedTime)}
          </td>
        </tr>

        <tr className='sleep-data-row'>
          <td className='sleep-data-key'>Sleep time</td>
          <td className='sleep-data-value'>
            <i className='fa fa-bed' /> {getTime(data.sleepTime)}
          </td>
        </tr>

        <tr className='sleep-data-row'>
          <td className='sleep-data-key'>Wake up time</td>
          <td className='sleep-data-value'>
            <i className='fa fa-sun-o' /> {getTime(data.wakeupTime)}
          </td>
        </tr>

        <tr className='sleep-data-row'>
          <td className='sleep-data-key'>Avg. heart rate</td>
          <td className='sleep-data-value'>
            <i className='fa fa-heart' /> {data.heartRate}
          </td>
        </tr>

        <tr className='sleep-data-row'>
          <td className='sleep-data-key'>Avg. resp rate</td>
          <td className='sleep-data-value'>
            <i className='fa fa-sun-o' /> {data.breathRate}
          </td>
        </tr>

        <tr className='sleep-data-row'>
          <td className='sleep-data-key'>User tags</td>
          <td className='sleep-data-value'>-</td>
        </tr>

        <tr className='sleep-data-row'>
          <td className='sleep-data-key'>Sleep score</td>
          <td className='sleep-data-value'>{data.sleepScore}</td>
        </tr>

        <tr className='sleep-data-row'>
          <td className='sleep-data-key'>Recovery</td>
          <td className='sleep-data-value'>
            {data.rcy}/{data.rcyRecommended}
          </td>
        </tr>
      </tbody>
    </table>
  );
};

export default SleepDataTable;
