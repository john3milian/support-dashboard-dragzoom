import React, { useMemo, useEffect, useState } from "react";
// import { COLUMNS } from './columns';
import {
  useTable,
  useSortBy,
  useGlobalFilter,
  usePagination,
} from "react-table";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSort,
  faSortDown,
  faSortUp,
} from "@fortawesome/free-solid-svg-icons";
import GlobalFilter from "./GlobalFilter";

const TableGenerator = ({
  users,
  COLUMNS,
  defaultPageSize = 20,
  search = true,
  cbState,
  setCbState,
  setProcessingTimeAvgfunc,
  processingTimeAvg,
  setProcessingTimeAvg,
}) => {
  const columns = useMemo(() => COLUMNS, []);
  const data = useMemo(() => users, [users]);
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    previousPage,
    nextPage,
    canPreviousPage,
    canNextPage,
    pageOptions,
    gotoPage,
    pageCount,
    setPageSize,
    state,
    setGlobalFilter,
  } = useTable(
    {
      columns,
      data,
      initialState: {
        pageSize: defaultPageSize,
      },
    },
    useGlobalFilter,
    useSortBy,
    usePagination
  );

  const returnRowColour = (row) => {
    try {
      const data = row.values;
      const started = new Date(data.startTime).getTime();
      const ended = new Date(data.endTime).getTime();
      const secsToEnd = Math.round((ended - started) / 1000);
      let tempAvg = processingTimeAvg;
      if (!isNaN(ended))
        if (secsToEnd > 240) {
          tempAvg.push(secsToEnd);
          return "secondsAreLessThan4";
        } else {
          tempAvg.push(secsToEnd);
          return "secondsAreMoreThan4";
        }
      setProcessingTimeAvg(tempAvg);
      return "";
    } catch {
      return "";
    }
  };
  // const getTrProps = (row, rowInfo) => {
  //   console.log(row);
  //   if (rowInfo) {
  //     return {
  //       style: {
  //         background: rowInfo.row.age > 20 ? "red" : "green",
  //         color: "white",
  //       },
  //     };
  //   }
  //   return {};
  // };
  useEffect(() => {
    // console.log("Called");
    // // console.log(page);
    // console.log("pAGE CHANGED");
    // setTimeout(() => {
    //   setProcessingTimeAvgfunc(page.length);
    // }, 1000);
  }, [page]);
  // const element = <FontAwesomeIcon icon={faCoffee} />
  const sort = <FontAwesomeIcon icon={faSort} />;
  const sortDown = <FontAwesomeIcon icon={faSortDown} />;
  const sortUp = <FontAwesomeIcon icon={faSortUp} />;

  const { globalFilter, pageIndex, pageSize } = state;

  return (
    <div className='tableContainer'>
      <div className='table-search-div'>
        {setCbState && (
          <span className='cb-outer-span'>
            <span className='cb-inner-span'>
              <input
                type='checkbox'
                onChange={() => setCbState(!cbState)}
                value={cbState}
                id='users-hide-autogen-cb'
                name='users-hide-autogen-cb'
              />
            </span>
            <label htmlFor='users-autogen-cb'> hide @autogen</label>
          </span>
        )}
        {search && (
          <GlobalFilter filter={globalFilter} setFilter={setGlobalFilter} />
        )}
      </div>

      <div className='table-body-container'>
        <table {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                    {column.render("Header")}
                    <span>
                      {column.isSorted
                        ? column.isSortedDesc
                          ? sortDown
                          : sortUp
                        : sort}
                    </span>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()} className={returnRowColour(row)}>
                  {/* console.log(cell.row); */}
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>
                        {cell.render("Cell")}
                        {/* {cell} */}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      <div className='table-btn-container'>
        <span className='table-page-size-span'>
          <select
            value={pageSize}
            onChange={(e) => setPageSize(Number(e.target.value))}>
            {[10, 15, 20, 25, 50].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize}
              </option>
            ))}
          </select>
        </span>

        <span className='table-page-number-span'>
          Page{" "}
          <strong>
            {pageIndex + 1} of {pageOptions.length}
          </strong>{" "}
        </span>

        <span className='table-go-to-page-span'>
          <span>Go to page</span>
          <input
            type='number'
            defaultValue={pageIndex + 1}
            onChange={(e) => {
              const pageNumber = e.target.value
                ? Number(e.target.value) - 1
                : 0;
              gotoPage(pageNumber);
            }}
            style={{ width: "40px" }}
          />{" "}
        </span>

        <span className='table-btn-span'>
          <button
            className='pageBtn'
            onClick={() => gotoPage(0)}
            disabled={!canPreviousPage}>
            {" << "}
          </button>
          <button
            className='pageBtn'
            onClick={() => previousPage()}
            disabled={!canPreviousPage}>
            {" < "}
          </button>
          <button
            className='pageBtn'
            onClick={() => nextPage()}
            disabled={!canNextPage}>
            {" > "}
          </button>
          <button
            className='pageBtn'
            onClick={() => gotoPage(pageCount - 1)}
            disabled={!canNextPage}>
            {" >> "}
          </button>
        </span>
      </div>
    </div>
  );
};

export default TableGenerator;
