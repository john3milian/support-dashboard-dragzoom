import React from "react";
import { Bar } from "react-chartjs-2";
import zoomPlugin from "chartjs-plugin-zoom";

const QualityParamsGraphGenerator = ({ graphData }) => {
  // console.log(graphData);

  const noMovementData = getNoMovmentData(graphData);

  // console.log(noMovementData);

  const data = {
    datasets: [
      getGain(graphData),
      getBaseValue(graphData),
      getProcessTime(graphData),
      getSpikes(graphData),
      getSat(graphData),
      getLooseSheet(graphData),
      getSatBreathing(graphData),
      getNOCC(graphData),
      get18hzNoise(graphData),
      getExtNoise(graphData),
      getWhiteNoise(graphData),
      // get50hzNoise(graphData),

      getACNoise(noMovementData),
      getHeartCorrel(noMovementData),
      getRange(noMovementData),
    ],
  };

  const options = {
    elements: {
      line: {
        tension: 0,
      },
      point: {
        radius: 0,
        hoverRadius: 6,
      },
    },
    animation: {
      duration: 0,
    },
    tooltips: {
      // mode: 'dataset',
      mode: "index",
      // mode: 'x',
      // mode: 'y',
      // mode: 'point',
      // mode: 'single',
      // mode: 'nearest',
      intersect: false,
      // intersect: true
    },
    plugins: {
      // zoom: {
      //   pan: {
      //     enabled: true,
      //     mode: "x",
      //   },
      //   zoom: {
      //     enabled: true,
      //     mode: "x",
      //   },
      // },
      datalabels: {
        display: false,
      },
    },
    responsive: true,
    aspectRatio: 3.5,
    title: {
      display: false,
      text: "Chart",
    },
    legend: {
      display: true,
      position: "top",
      labels: {
        usePointStyle: true,
        // padding: 10
      },
    },
    scales: {
      yAxes: [
        getYAxis("Range", "left", true),
        getYAxis("AC noise", "left", true, true),
        getYAxis("Heart Correl.", "left", true),
        getYAxis("Gain", "right", true),
        getYAxis("Base Value", "right", true),
        getYAxis("Processing Time", "right", false),
        getYAxis("Spikes", "right", false),
        getYAxis("Saturation", "right", false),
        getYAxis("Loose Sheet", "right", false, true),
        getYAxis("Sat. Breathing", "right", false, true),
        getYAxis("NOCC", "right", false, true),
        getYAxis("18Hz noise", "right", false, true),

        // getYAxis('50Hz Noise', 'right', false),
        getYAxis("Ext. noise", "right", false, true),
        getYAxis("White noise", "right", false, true),
      ],
      xAxes: [getXAxis()],
    },
  };
  return <Bar data={data} options={options} height={null} width={null} />;
};

export default QualityParamsGraphGenerator;

function fixTime(time) {
  return new Date(parseInt(time) * 1000);
}

function getQualityValues(data, key) {
  const arr = [];

  for (let item of data) {
    const obj = {};
    obj.x = fixTime(item.quality.headerTimeStamp);
    obj.y = item.quality[key];

    arr.push(obj);
  }
  return arr;
}

function getNoMovementQualityValues(data, key) {
  const arr = [];
  // console.log('data: ', key, data);
  for (let item of data) {
    const obj = {};
    obj.x = new Date(item.ts * 1000);
    obj.y = item[key];
    arr.push(obj);
  }
  // console.log(arr);
  return arr;
}

function getACNoise(noMovementData) {
  return {
    type: "line",
    label: "AC noise",
    yAxisID: "AC noise",
    data: getNoMovementQualityValues(noMovementData, "acNoise"),
    borderColor: ["gold"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    hidden: true,
  };
}
function getHeartCorrel(noMovementData) {
  return {
    type: "line",
    label: "Heart Correl.",
    yAxisID: "Heart Correl.",
    data: getNoMovementQualityValues(noMovementData, "heartCorrelation"),
    borderColor: ["red"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    // hidden: true,
  };
}

function getRange(noMovementData) {
  return {
    type: "line",
    label: "Range",
    yAxisID: "Range",
    data: getNoMovementQualityValues(noMovementData, "dataRange"),
    borderColor: ["#FFD9CF"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    // steppedLine: 'middle',
    hidden: true,
  };
}

function getWhiteNoise(data) {
  return {
    type: "line",
    label: "White noise",
    yAxisID: "White noise",
    data: getQualityValues(data, "whitenoise"),
    borderColor: ["black"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    steppedLine: "middle",
    hidden: true,
  };
}

function getExtNoise(data) {
  return {
    type: "line",
    label: "Ext. noise",
    yAxisID: "Ext. noise",
    data: getQualityValues(data, "extnoise"),
    borderColor: ["#8EA4C8"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    steppedLine: "middle",
    hidden: true,
  };
}

function get18hzNoise(data) {
  return {
    type: "line",
    label: "18Hz noise",
    yAxisID: "18Hz noise",
    data: getQualityValues(data, "noise18hz"),
    borderColor: ["#E6D1D2"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    // steppedLine: 'middle',
    hidden: true,
  };
}

function getNOCC(data) {
  return {
    type: "line",
    label: "NOCC",
    yAxisID: "NOCC",
    data: getQualityValues(data, "nocc"),
    borderColor: ["#B2B5B9"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    steppedLine: "middle",
    hidden: true,
  };
}

function getSatBreathing(data) {
  return {
    type: "line",
    label: "Sat. Breathing",
    yAxisID: "Sat. Breathing",
    data: getQualityValues(data, "saturatingBreathing"),
    borderColor: ["#938F43"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    hidden: true,
  };
}

function getLooseSheet(data) {
  return {
    type: "line",
    label: "Loose Sheet",
    yAxisID: "Loose Sheet",
    data: getQualityValues(data, "loseeSheet"),
    borderColor: ["#8FA2A6"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    hidden: true,
  };
}

function getSat(data) {
  return {
    type: "line",
    label: "Saturation",
    yAxisID: "Saturation",
    data: getQualityValues(data, "saturation"),
    borderColor: ["#DB93A5"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    hidden: true,
  };
}

function getSpikes(data) {
  return {
    type: "line",
    label: "Spikes",
    yAxisID: "Spikes",
    data: getQualityValues(data, "spikes"),
    borderColor: ["#CA9C95"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    hidden: true,
  };
}

function getProcessTime(data) {
  return {
    type: "line",
    label: "Processing Time",
    yAxisID: "Processing Time",
    data: getQualityValues(data, "processTime"),
    borderColor: ["#82B2B8"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    hidden: true,
  };
}

function getBaseValue(data) {
  return {
    type: "line",
    label: "Base Value",
    yAxisID: "Base Value",
    data: getQualityValues(data, "baseValue"),
    borderColor: ["#D9C2BD"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    order: 1,
    // steppedLine: 'middle',
    // spanGaps: false,
    hidden: true,
  };
}

function getGain(data) {
  return {
    type: "line",
    label: "Gain",
    yAxisID: "Gain",
    data: getQualityValues(data, "gain"),
    borderColor: ["steelblue"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 2.5,
    steppedLine: "middle",
    order: 1,
    // spanGaps: false,
    hidden: true,
  };
}

function getYAxis(id, position = "right", show = true, beginAtZero = false) {
  return {
    scaleLabel: {
      display: show,
      labelString: id,
    },
    id,
    position,
    ticks: {
      display: show,
      beginAtZero,
      step: 1,
    },
    gridLines: {
      display: show,
      drawOnChartArea: false,
      color: "black",
      // drawBorder: false,
    },
  };
}

function getXAxis() {
  return {
    ticks: {
      maxTicksLimit: 20,
      display: true,
    },
    gridLines: {
      display: true,
      color: "black",
      drawOnChartArea: false,
    },
    type: "time",
    time: {
      displayFormats: {
        minute: "h:mm a",
      },
    },
    barThickness: "flex",
    maxBarThickness: 3,
  };
}

function extract(data) {
  const arr = [];
  for (let key of Object.keys(data)) {
    const obj = {
      ts: parseInt(key),
      ...data[key],
    };
    arr.push(obj);
  }
  arr.sort((a, b) => (a.ts > b.ts ? 1 : -1));
  return arr;
}

function getNoMovmentData(data) {
  const arr = [];

  for (let item of data) {
    arr.push(extract(item.quality.noMovementQualityParameters));
  }
  return arr.flat();
}
