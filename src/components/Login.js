import { Button, CircularProgress } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import "../css/Login.css";
import PersonIcon from "@material-ui/icons/Person";
// import AlternateEmailIcon from "@material-ui/icons/AlternateEmail";

import DialpadIcon from "@material-ui/icons/Dialpad";
import { Zoom } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import { Alert } from "@material-ui/lab";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [otpSent, setOtpSent] = useState(false);
  const [otpWrong, setOtpWrong] = useState(false);
  const [showLoader, setShowLoader] = useState(false);
  const [otpError, setOtpError] = useState("Invalid OTP");
  // ! Below value is for temporary session id storage
  const [gibberish123, setGibberish123] = useState("");

  const history = useHistory();

  const sendOtp = (e) => {
    e.preventDefault();

    const requestOtp = async () => {
      var myHeaders = new Headers();

      myHeaders.append("Content-Type", "application/json");

      var raw = JSON.stringify({ Medium: "Email", MediumValue: email });
      var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };
      const results = fetch(
        "http://139.59.20.84:2385/api/v1/request",
        requestOptions
      )
        .then((response) => {
          localStorage.removeItem("authToken");
          localStorage.removeItem("authTeam");
          localStorage.removeItem("userEmail");
          response.json();
        })
        .then((result) => {
          setOtpSent(true);
          setGibberish123(result["SessionId"]);
        })
        .catch((error) => {
          setOtpSent(false);
          setOtpWrong(true);
          setOtpError(
            "Please check if you have permission to acces the dashboard!"
          );
          console.log("error", error);
        });
      const body = JSON.stringify(results);
      console.log(body);
    };
    requestOtp();
  };

  const verifyOTP = () => {
    const validateUser = async () => {
      console.log(email);
      console.log(password);
      var myHeaders = new Headers();

      myHeaders.append("Content-Type", "application/json");
      var raw = JSON.stringify({
        Medium: "email",
        MediumValue: email,
        Otp: password,
        SessionId: gibberish123,
      });
      var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: raw,
        redirect: "follow",
      };
      fetch("http://139.59.20.84:2385/api/v1/verify", requestOptions)
        .then(async (result) => {
          const otp_body = await result.json();
          console.log(otp_body);
          console.log(otp_body.access_token);
          setGibberish123("");
          localStorage.setItem("authToken", otp_body.access_token);
          localStorage.setItem("authTeam", otp_body.team);
          localStorage.setItem("userEmail", email);
          setOtpWrong(false);
          setShowLoader(false);
          // window.location.href = "/#/sleep";
          const win = window.open(`#/`);
          window.top.close();
          // win.focus();
        })
        .catch((error) => {
          setOtpWrong(true);
          setShowLoader(false);
          console.log("error", error);
        });
    };
    setShowLoader(true);
    validateUser();
  };

  return (
    <div className='loginPage'>
      <div className='loginBox'>
        <h2>Dashboard Login</h2>
        <form
          className='loginBox_Bottom'
          onSubmit={(e) => {
            sendOtp(e);
          }}>
          {!otpSent && (
            <div className='InputBox'>
              <PersonIcon />
              <input
                autoComplete={false}
                type='email'
                id='Email'
                placeholder='Email'
                value={email}
                onChange={(e) => {
                  setEmail(e.target.value);
                }}
              />
            </div>
          )}

          {otpSent && (
            <Zoom in={otpSent} timeout={350}>
              <div className='InputBox'>
                <DialpadIcon />
                <input
                  type='password'
                  id='Email'
                  placeholder='OTP'
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
            </Zoom>
          )}
          {!otpSent && (
            <div style={{ width: "100%", maxWidth: "250px" }}>
              <Button
                variant='contained'
                color='primary'
                fullWidth
                type='submit'>
                Send OTP
              </Button>
              {otpWrong && <div className='mt-4' />}
            </div>
          )}
          {otpWrong && (
            <div style={{ marginBottom: 10 }}>
              <Alert severity='error'>{otpError}</Alert>
            </div>
          )}
          {otpSent && (
            <div style={{ width: "100%", maxWidth: "250px" }}>
              {showLoader ? (
                <CircularProgress />
              ) : (
                <>
                  <Button
                    variant='contained'
                    color='primary'
                    fullWidth
                    onClick={verifyOTP}>
                    Login
                  </Button>
                  <div className='rest_button'>
                    <Button
                      variant='contained'
                      color='secondary'
                      fullWidth
                      onClick={() => setOtpSent(false)}>
                      Go back
                    </Button>
                  </div>
                </>
              )}
            </div>
          )}
        </form>
      </div>
    </div>
  );
}

export default Login;
