import { Typography } from "@material-ui/core";
import forbiddenImage from "../../images/403.png";

function NotAuthorized() {
  return (
    <div
      className='d-flex w-100 align-items-center justify-content-center flex-column bg-white'
      style={{ height: "100%" }}>
      {/* <Typography variant='h1'> 403</Typography>
      <Typography variant='h4'>
        <u>Forbidden</u>
      </Typography> */}
      <img src={forbiddenImage} alt='Forbidden' />
      <Typography variant='h5'>
        {" "}
        You do not have access to this page!!{" "}
      </Typography>
    </div>
  );
}

export default NotAuthorized;
