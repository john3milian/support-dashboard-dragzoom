import React, { useContext, useEffect } from "react";
import { Select, TextField } from "@material-ui/core";
import API from "../../../api";
import InputLabel from "@material-ui/core/InputLabel";
import { TaskContext } from "../../../context/TaskContext";
import moment from "moment";

export function TaskInput({ eachField }) {
  const [final, setFinal] = React.useState();
  const { handleEdit } = useContext(TaskContext);

  useEffect(() => {
    handleEdit(eachField.id, final);
  }, [final, eachField.id]);

  const handleChange = (e) => {
    const tempFinal = e.target.value;
    setFinal(tempFinal);
    // handleEdit(eachField.id, tempFinal);
  };
  return (
    <div>
      <InputLabel htmlFor='age-native-simple'>{eachField.label}</InputLabel>
      <TextField
        InputLabelProps={{
          shrink: true,
        }}
        onChange={(event) => handleChange(event)}
        type='text'
        fullWidth
        variant='outlined'
      />
    </div>
  );
}

export function TaskDate({ eachField }) {
  const [final, setFinal] = React.useState();
  const { handleEdit } = useContext(TaskContext);
  useEffect(() => {
    handleEdit(eachField.id, final);
  }, [final, eachField.id]);

  const handleChange = (e) => {
    let tempFinal = moment(e.target.value).format();
    // let tempFinal = new Date(e.target.value);
    setFinal(tempFinal);
    // handleEdit(eachField.id, tempFinal);
  };
  return (
    <div>
      <InputLabel htmlFor='age-native-simple'>{eachField.label}</InputLabel>
      <TextField
        InputLabelProps={{
          shrink: true,
        }}
        type='date'
        fullWidth
        variant='outlined'
        onChange={(e) => handleChange(e)}
      />
    </div>
  );
}

export function TaskTimestamp({ eachField }) {
  const [final, setFinal] = React.useState();
  const { handleEdit } = useContext(TaskContext);
  useEffect(() => {
    handleEdit(eachField.id, final);
  }, [final, eachField.id]);

  const handleChange = (e) => {
    let theDate = new Date(e.target.value);
    if (eachField.id === "nextrun") {
      console.log("THIS IS NEXTRUN!!!!!!!!!");
      theDate = theDate.getTime() / 1000;
      setFinal(theDate);
    } else {
      setFinal(theDate.getTime());
    }
  };
  return (
    <div>
      <InputLabel htmlFor='age-native-simple'>{eachField.label}</InputLabel>
      <TextField
        InputLabelProps={{
          shrink: true,
        }}
        type='datetime-local'
        fullWidth
        variant='outlined'
        onChange={(e) => handleChange(e)}
      />
    </div>
  );
}

export function TaskTime({ eachField }) {
  const [final, setFinal] = React.useState();
  const { handleEdit } = useContext(TaskContext);
  useEffect(() => {
    handleEdit(eachField.id, final);
  }, [final, eachField.id]);

  return (
    <div>
      <TextField
        InputLabelProps={{
          shrink: true,
        }}
        type='time'
        fullWidth
        variant='outlined'
        label={eachField.label}
      />
    </div>
  );
}

export function TaskDropdown({ eachField }) {
  const [dropdownValues, setDropdownValues] = React.useState([]);
  const [final, setFinal] = React.useState();
  const { handleEdit } = useContext(TaskContext);
  useEffect(() => {
    handleEdit(eachField.id, final);
  }, [final, eachField.id]);

  React.useEffect(() => {
    if (eachField.data_origin.toLowerCase() === "url") {
      API.get(eachField.source)
        .then((response) => {
          if (response.status === 200) {
            const apiValues = response.data;
            setDropdownValues(apiValues);
          } else if (response.status) {
            alert("Error occured! Status code:" + response.status);
          }
        })
        .catch((error) => {
          alert(error);
        });
    } else {
      setDropdownValues(eachField.source);
    }
  }, [eachField]);

  return (
    <div>
      <InputLabel htmlFor='age-native-simple'>{eachField.label}</InputLabel>
      <Select
        style={{ outline: 0 }}
        native
        defaultValue=''
        InputLabelProps={{
          shrink: true,
        }}
        fullWidth
        variant='outlined'
        onChange={(e) => setFinal(e.target.value)}>
        {dropdownValues.map((value) => (
          <option
            value={value.OrganizationId || value}
            key={value.OrganizationId || value}>
            {value.OrganizationName || value}
          </option>
        ))}
      </Select>
    </div>
  );
}

export function TaskSecondsDropdown({ eachField }) {
  const [timer, setTimer] = React.useState(0);
  const [noOfDays, setNoOfDays] = React.useState(0);
  const [final, setFinal] = React.useState();
  const { handleEdit } = useContext(TaskContext);

  const days = [0, 1, 2, 3, 4, 5, 6, 7];
  const dropdownValues = [
    "00:30",
    "01:00",
    "01:30",
    "02:00",
    "02:30",
    "03:00",
    "03:30",
    "04:00",
    "04:30",
    "05:00",
    "05:30",
    "06:00",
    "06:30",
    "07:00",
    "07:30",
    "08:00",
    "08:30",
    "09:00",
    "09:30",
    "10:00",
    "10:30",
    "11:00",
    "11:30",
    "12:00",
    "12:30",
    "13:00",
    "13:30",
    "14:00",
    "14:30",
    "15:00",
    "15:30",
    "16:00",
    "16:30",
    "17:00",
    "17:30",
    "18:00",
    "18:30",
    "19:00",
    "19:30",
    "20:00",
    "20:30",
    "21:00",
    "21:30",
    "22:00",
    "22:30",
    "23:00",
    "23:30",
  ];

  React.useEffect(() => {}, [eachField]);

  const onChangeTimer = (e) => {
    let tempValue;
    let [hours, minutes] = e.target.value.split(":");
    console.log(hours, minutes);
    let temptimerValue = parseInt(hours) * 60 * 60 + parseInt(minutes) * 60;

    tempValue = temptimerValue + noOfDays;
    setTimer(temptimerValue);
    setFinal(tempValue);
  };

  const onChangeDays = (e) => {
    let tempValue;
    // Converts no of days to seconds
    let tempDateValue = e.target.value * 24 * 60 * 60;
    console.log(tempDateValue);

    // We already have hours and minutes converted to seconds in var called timer, we add it to set final value
    tempValue = tempDateValue + timer;
    setNoOfDays(tempDateValue);
    setFinal(tempValue);
  };

  useEffect(() => {
    handleEdit(eachField.id, final);
  }, [final, eachField.id]);
  return (
    <div className='d-flex'>
      <div className='w-50 mr-1'>
        <InputLabel htmlFor='age-native-simple'>
          {eachField.label} (Days)
        </InputLabel>
        <Select
          style={{ outline: 0 }}
          native
          defaultValue=''
          InputLabelProps={{
            shrink: true,
          }}
          fullWidth
          variant='outlined'
          onChange={(e) => onChangeDays(e)}>
          {days.map((value) => (
            <option value={value} key={value}>
              {value}
            </option>
          ))}
        </Select>
      </div>
      <div className='w-50 ml-1'>
        <InputLabel htmlFor='age-native-simple'>
          {eachField.label} (Hours: Minutes)
        </InputLabel>
        <Select
          style={{ outline: 0 }}
          native
          defaultValue=''
          InputLabelProps={{
            shrink: true,
          }}
          fullWidth
          variant='outlined'
          onChange={(e) => onChangeTimer(e)}>
          {dropdownValues.map((value) => (
            <option value={value} key={value}>
              {value}
            </option>
          ))}
        </Select>
      </div>
    </div>
  );
}
