import React from "react";
import {
  TaskInput,
  TaskDate,
  TaskTimestamp,
  TaskTime,
  TaskDropdown,
  TaskSecondsDropdown,
} from "./CustomInputs/CustomInputs";

function Element({ eachInput }) {
  switch (eachInput.type) {
    case "text":
      return <TaskInput eachField={eachInput} />;
    case "date":
      return <TaskDate eachField={eachInput} />;
    case "timestep":
      return <TaskTimestamp eachField={eachInput} />;
    case "time":
      return <TaskTime eachField={eachInput} />;
    case "timer":
      return <TaskSecondsDropdown eachField={eachInput} />;
    case "dropdown":
      return <TaskDropdown eachField={eachInput} />;
    default:
      return <p>{eachInput.type}</p>;
  }
}

export default Element;
