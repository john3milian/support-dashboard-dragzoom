import React, { useEffect, useRef } from "react";
import { Chart, Bar } from "react-chartjs-2";
// import zoomPlugin from "chartjs-plugin-zoom";
import { Button } from "@material-ui/core";
import ZoomOutIcon from "@material-ui/icons/ZoomOut";
// import "chartjs-plugin-crosshair";

const MixedGraphGenerator = ({ graphData }) => {
  // const [max, setMax] = useState(false);
  console.log(graphData);
  const chartRef = useRef();
  const resetZoom = () => {
    chartRef.current.chartInstance.resetZoom();
  };
  const { stages, events, stress, recovery, vitals } = graphData;

  const flatEvents = getData(events);
  const flatVitals = getData(vitals);
  const flatStress = getData(stress);
  const flatRecovery = getData(recovery);

  useEffect(() => {
    resetZoom();
  }, [graphData]);
  const data = {
    datasets: [
      getStageDataset(stages),
      getBRDataset(flatVitals),
      getHRDataset(flatVitals),
      getStressDataset(flatStress),
      getRecoveryDataset(flatRecovery),
      getMovementDataset(flatEvents),
      getApneaDataset(flatEvents),
      getArtifactsDataset(flatEvents),
      getSnoreDataset(flatEvents),
    ],
  };

  useEffect(() => {
    Chart.pluginService.register({
      beforeDraw: function (chart, easing) {
        const ctx = chart.ctx;
        ctx.save();
        ctx.beginPath();
        ctx.moveTo(0, 0);
        ctx.stroke();
        ctx.restore();
      },
      afterDraw: function (chart, easing) {
        if (chart.tooltip._active && chart.tooltip._active.length) {
          try {
            const activePoint = chart.controller.tooltip._active[0];
            const ctx = chart.ctx;
            const x = activePoint.tooltipPosition().x;
            const y = activePoint.tooltipPosition().y;
            const topY = chart.scales["stages"].top;
            const bottomY = chart.scales["stages"].bottom;
            const topX = chart.scales["x-axis-0"].left;
            const bottomX = chart.scales["x-axis-0"].right;
            // const heightOfCross = chart.scales["x-axis-0"].height;
            ctx.save();
            ctx.beginPath();
            ctx.moveTo(x, topY);
            ctx.lineTo(x, bottomY);
            ctx.moveTo(topX, y);
            ctx.lineTo(bottomX, y);
            ctx.strokeStyle = "#000000";
            ctx.strokeStyle = "#e23fa9";
            // ctx.gridLines = { color: "#000000", zeroLineColor: "#000000" };
            ctx.setLineDash([4]);
            ctx.lineWidth = 1;
            ctx.stroke();
            ctx.restore();
          } catch {
            const ctx = chart.ctx;
            ctx.save();
            ctx.beginPath();
            ctx.moveTo(0, 0);
            ctx.stroke();
            ctx.restore();
          }
        }
      },
    });
  }, []);

  const options = {
    default: {},
    animation: {
      duration: 0,
    },
    elements: {
      line: {
        tension: 0.2,
      },
      point: {
        radius: 0,
        hoverRadius: 6,
      },
    },
    tooltips: {
      // mode: 'x',
      mode: "index",

      intersect: false,
    },
    plugins: {
      crosshair: {
        line: {
          color: "#000000", // crosshair line color
          width: 5, // crosshair line width
        },
      },
      // zoom: {
      //   pan: {
      //     enabled: true,
      //     mode: "x",
      //   },
      //   zoom: {
      //     enabled: true,
      //     mode: "x",
      //   },
      // },
      datalabels: {
        display: false,
      },
    },
    responsive: true,
    // maintainAspectRatio: false,
    aspectRatio: 4,
    // aspectRatio: 2.75,
    title: {
      display: false,
      text: "Chart",
    },
    legend: {
      display: true,
      position: "top",
      labels: {
        // fontColor: 'rgb(255, 99, 132)',
        usePointStyle: true,
      },
    },
    scales: {
      // function getYAxis(id, position='right', show=true, min=undefined, max=undefined){
      // id, position='right', show=true, min=undefined, max=undefined, name

      yAxes: [
        getYAxis("stages", "left"),
        getYAxis("breath", "right", false, 0, 140),
        getYAxis("heart", "right", true, 0, 140),
        // getYAxis('breath'),
        // getYAxis('heart'),
        getYAxis("snores", "right", false),
        getYAxis("stress", "right"),
        getYAxis("recovery", "right", false),
        getYAxis("movement", "right", false),
        getYAxis("apnea", "right", false),
        getYAxis("artifacts", "right", false),
      ],
      xAxes: [getXAxis()],
    },
  };
  return (
    <>
      {" "}
      <div className='w-100'>
        <Button
          startIcon={<ZoomOutIcon />}
          className='float-right'
          onClick={resetZoom}>
          Reset Zoom
        </Button>
      </div>
      <Bar
        ref={chartRef}
        data={data}
        options={options}
        height={null}
        width={null}
      />
    </>
  );
};

export default MixedGraphGenerator;

function getStageDataset(stagesData) {
  const flatStages = getData(stagesData);
  const stageValues = getValues(stagesData, "stage");
  const stageColors = getStageColors(stagesData);
  const stageTimes = getTimes(flatStages);
  return {
    order: 12,
    yAxisID: "stages",
    type: "bar",
    label: "Stages",
    data: mergeData(stageTimes, stageValues),
    backgroundColor: [...stageColors],
    pointStyle: "circle",
    hidden: false,
  };
}

function getMovementDataset(movementData) {
  // const flatMovement = getData(movementData);
  const movementSet = getSpecificSet(movementData, "MV", "duration");
  const movementValues = getValues(movementSet, "MV");
  const movementTimes = getTimes(movementSet);
  const replacedData = replaceData(movementValues);

  return {
    order: 11,
    yAxisID: "movement",
    type: "bar",
    label: "Movement",
    data: mergeData(movementTimes, replacedData),
    // data: mergeData(movementTimes, movementValues),
    backgroundColor: new Array(movementValues.length).fill("rgb(225, 114, 77)"),
    pointStyle: "circle",
    hidden: true,
  };
}

function getApneaDataset(apneaData) {
  const apneaSet = getSpecificSet(apneaData, "APN", "duration");
  const apneaValues = getValues(apneaSet, "APN");
  const apneaTimes = getTimes(apneaSet);
  const replacedData = replaceData(apneaValues);

  return {
    order: 11,
    yAxisID: "apnea",
    type: "bar",
    label: "Apnea",
    data: mergeData(apneaTimes, replacedData),
    backgroundColor: new Array(apneaValues.length).fill("rgb(255, 88, 244)"),
    pointStyle: "circle",
    hidden: true,
  };
}

function getArtifactsDataset(artifactsData) {
  const artifactsSet = getSpecificSet(artifactsData, "ARF", "duration");
  const artifactsValues = getValues(artifactsSet, "ARF");
  const artifactsTimes = getTimes(artifactsSet);
  const replacedData = replaceData(artifactsValues);

  return {
    order: 11,
    yAxisID: "artifacts",
    type: "bar",
    label: "Artifacts",
    data: mergeData(artifactsTimes, replacedData),
    backgroundColor: new Array(artifactsValues.length).fill(
      "rgb(157, 188, 64)"
    ),
    pointStyle: "circle",
    hidden: true,
  };
}

function getSnoreDataset(snoreData) {
  const snoreSet = getSpecificSet(snoreData, "SN", "duration");
  const snoreValues = getValues(snoreSet, "SN");
  const snoreTimes = getTimes(snoreSet);
  const replacedData = replaceData(snoreValues);

  return {
    order: 11,
    yAxisID: "snores",
    type: "bar",
    label: "Snores",
    data: mergeData(snoreTimes, replacedData),
    backgroundColor: new Array(snoreValues.length).fill("rgb(67, 67, 67)"),
    pointStyle: "circle",
    hidden: true,
  };
}

function getBRDataset(vitals) {
  const bRValues = getSpecificVitalsValues(vitals, "bR");
  const bRTimes = getTimes(vitals);

  return {
    type: "line",
    label: "Breath",
    yAxisID: "breath",
    data: mergeData(bRTimes, bRValues),
    // borderColor: ["steelblue"],
    borderColor: ["rgb(141, 75, 212)"],
    // filter: "brightness(1.5)",
    // hoverBackgroundColor: ["rgba(254,22,49,1)"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 1.5,
    spanGaps: false,
    order: 1,
    hidden: true,
  };
}

function getHRDataset(vitals) {
  const hRValues = getSpecificVitalsValues(vitals, "hR");
  const hRTimes = getTimes(vitals);

  return {
    type: "line",
    label: "Heart",
    yAxisID: "heart",
    data: mergeData(hRTimes, hRValues),
    // Old V
    // borderColor: ["rgb(208, 11, 11)"],
    // New V
    borderColor: ["rgb(255, 11, 11)"],
    backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 1.5,
    spanGaps: false,
    order: 2,
    hidden: true,
  };
}

function getStressDataset(stressData) {
  const stressValues = getValues(stressData, "stress");
  const stressTimes = getTimes(stressData);

  return {
    type: "line",
    label: "Stress",
    yAxisID: "stress",
    data: mergeData(stressTimes, stressValues),
    // Old V
    borderColor: ["rgb(95, 194, 244)"],
    backgroundColor: ["rgba(153, 221, 255, 0.531)"],
    // New V
    // borderColor: ["rgb(95, 233, 244)"],
    // backgroundColor: ["rgba(95, 233, 244, 0.4)"],
    // backgroundColor: ["rgba(0,0,0,0)"],
    borderWidth: 1.5,
    spanGaps: false,
    order: 3,
    hidden: true,
  };
}

function getRecoveryDataset(recoveryData) {
  const recoveryValues = getValues(recoveryData, "rcy");
  const recoveryTimes = getTimes(recoveryData);

  return {
    type: "line",
    label: "Recovery",
    yAxisID: "recovery",
    data: mergeData(recoveryTimes, recoveryValues),
    // borderColor: ["rgb(45, 135, 38)"],
    // backgroundColor: ["rgba(64, 135, 59, 0.493)"],
    // NEW V
    borderColor: ["rgb(45, 177, 38)"],
    backgroundColor: ["rgba(45, 177, 38, 0.4)"],
    // backgroundColor: ["rgba(255, 211, 227,.2)"],
    borderWidth: 1.5,
    spanGaps: false,
    order: 4,
    hidden: true,
  };
}

function mergeData(time, data) {
  const arr = [];

  for (let i = 0; i < time.length; i++) {
    const obj = {};
    obj.x = time[i];
    obj.y = data[i];
    arr.push(obj);
  }
  return arr;
}

function replaceData(set) {
  return new Array(set.length).fill(1);
}

function getYAxis(
  id,
  position = "right",
  show = true,
  min = undefined,
  max = undefined,
  name
) {
  return {
    scaleLabel: {
      display: show,
      labelString: name || id[0].toUpperCase() + id.slice(1),
    },
    id,
    // type: 'linear',
    position,
    ticks: {
      display: show,
      beginAtZero: true,
      min,
      max,
      step: 1,
      // padding: 10,
    },
    gridLines: {
      display: show,
      drawOnChartArea: false,
      color: "black",
      // display: false,
      // lineWidth: 1,
      // borderDash: [1]
      // borderWidth: 5
    },
  };
}

function getXAxis() {
  return {
    ticks: {
      maxTicksLimit: 20,
      display: true,
    },
    gridLines: {
      display: true,
      color: "black",
      drawOnChartArea: false,
    },
    type: "time",
    time: {
      displayFormats: {
        minute: "h:mm a",
      },
    },
    barThickness: "flex",
    maxBarThickness: 1.5,
  };
}

function getStageColors(data) {
  const arr = [];
  for (let item of data) {
    switch (item.stage) {
      case 1:
        arr.push("#B6D8F2");
        break;
      case 2:
        arr.push("#F4CFDF");
        break;
      case 3:
        arr.push("#5784BA");
        break;
      case 4:
        arr.push("#CCD4BF");
        break;
      default:
        arr.push("#FFD9CF");
        break;
    }
  }
  return arr;
}

function getTimes(data) {
  const arr = [];

  for (let item of data) {
    const adjustedTime = parseInt(item.ts) * 1000;
    arr.push(new Date(adjustedTime));
  }

  return arr;
}

function getData(data) {
  return data.flat();
}

function getValues(data, key) {
  const arr = [];
  for (let item of data) {
    arr.push(item[key]);
  }
  return arr;
}
// function getValues(data, key, setMax=false){
// 	const arr = [];
// 	let max = -Infinity;
// 	for(let item of data){
// 		max = Math.max(max, item[key])
// 		arr.push(item[key])
// 	}
// 	if(setMax) setMax(max);
// 	return arr;
// }

function getSpecificVitalsValues(data, key) {
  const arr = [];

  for (let item of data) {
    if (item[key] === -1) {
      arr.push(NaN);
    } else {
      arr.push(item[key]);
    }
  }

  return arr;
}

function getSpecificSet(data, key, subkey) {
  const arr = [];

  for (let item of data) {
    if (item[key]) {
      const obj = {};
      // obj[key] = item[key];
      if (subkey) {
        obj[key] = item[key][subkey];
      } else {
        obj[key] = item[key];
      }
      obj.ts = item.ts;
      arr.push(obj);
    }
  }
  return arr;
}
