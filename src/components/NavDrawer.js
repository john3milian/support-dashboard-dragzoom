import React, { useEffect } from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import HomeIcon from "@material-ui/icons/Home";
import PermIdentityTwoToneIcon from "@material-ui/icons/PermIdentityTwoTone";
import ListAltIcon from "@material-ui/icons/ListAlt";
import TrendingUpIcon from "@material-ui/icons/TrendingUp";
import NotificationsIcon from "@material-ui/icons/Notifications";
import SettingsInputComponentIcon from "@material-ui/icons/SettingsInputComponent";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";

import LocalHotelIcon from "@material-ui/icons/LocalHotel";
import { useHistory } from "react-router";
import { Button } from "@material-ui/core";
import allLevels from "../helpers/navigationHelper";
const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    background: "-webkit-linear-gradient(left, #2b6ca3, #2b6ca3)",
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    background: "-webkit-linear-gradient(left, #004960, #2f5f7e)",
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
    color: "white",
  },
  drawerOpen: {
    // background: "-webkit-linear-gradient(left, #004960, #2f5f7e)",
    color: "#2f5f7e",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    // background: "-webkit-linear-gradient(left, #004960, #2f5f7e)",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9.5) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

export default function NavDrawer({ isAuth }) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const history = useHistory();

  const [sleepAccess, setSleepAccess] = React.useState(false);
  const [alertAccess, setAlertAccess] = React.useState(false);

  const [opsAccess, setOpsAccess] = React.useState(false);
  const [hardwareAccess, setHardwareAccess] = React.useState(false);
  const [statsAccess, setStatsAccess] = React.useState(false);
  const [adminAccess, setAdminAccess] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    checkAccess();
  }, []);
  const checkAccess = (level) => {
    const theLevel = localStorage.getItem("authTeam");

    // Alerts
    setAlertAccess(
      allLevels
        .map((eachLevel) => {
          if (eachLevel["level"] === theLevel) {
            return eachLevel.values.includes("Alerts");
          } else return false;
        })
        .includes(true)
    );
    // Ops
    setOpsAccess(
      allLevels
        .map((eachLevel) => {
          if (eachLevel["level"] === theLevel) {
            return eachLevel.values.includes("Ops");
          } else return false;
        })
        .includes(true)
    );
    // Hardware
    setHardwareAccess(
      allLevels
        .map((eachLevel) => {
          if (eachLevel["level"] === theLevel) {
            return eachLevel.values.includes("HW");
          } else return false;
        })
        .includes(true)
    );

    // Stats
    setStatsAccess(
      allLevels
        .map((eachLevel) => {
          if (eachLevel["level"] === theLevel) {
            return eachLevel.values.includes("Stats");
          } else return false;
        })
        .includes(true)
    );

    // Admin
    setAdminAccess(
      allLevels
        .map((eachLevel) => {
          if (eachLevel["level"] === theLevel) {
            return eachLevel.values.includes("Admin");
          } else return false;
        })
        .includes(true)
    );
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position='fixed'
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}>
        <Toolbar style={{ paddingLeft: "15px" }}>
          <IconButton
            color='inherit'
            aria-label='open drawer'
            // onClick={handleDrawerOpen}
            edge='start'
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}>
            {/* <MenuIcon /> */}
            <img src='./dozee_logo.png' style={{ width: "45px" }} alt='dozee' />
          </IconButton>
          <div style={{ flex: 1 }}>
            <Button
              variant='contained'
              style={{
                float: "right",
                backgroundColor: "white !important",
                outline: 0,
              }}
              endIcon={<ExitToAppIcon />}
              onClick={() => {
                localStorage.removeItem("authToken");
                localStorage.removeItem("userEmail");
                history.push("/login");
              }}>
              Logout
            </Button>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        variant='permanent'
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}>
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon color='inherit' />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
          {/* <ListItem
            button
            onClick={() => history.push("/login")}
            style={{ paddingLeft: "0.5vw" }}>
            <ListItemIcon style={{ color: "white !important" }} color='primary'>
              <div className='d-flex flex-column align-items-center  justify-content-start w-100'>
                <PermIdentityTwoToneIcon color='primary' fontSize='large' />
                {!open && (
                  <Typography color='primary' variant='subtitle2'>
                    Login
                  </Typography>
                )}
              </div>
            </ListItemIcon>
          </ListItem> */}
          <ListItem
            button
            onClick={() => history.push("/")}
            style={{ paddingLeft: "0.5vw" }}>
            {/* <Link to='/matured-users'> */}
            <ListItemIcon style={{ color: "white !important" }} color='primary'>
              <div className='d-flex flex-column align-items-center  justify-content-start w-100'>
                <HomeIcon color='primary' fontSize='large' />
                {!open && (
                  <Typography color='primary' variant='subtitle2'>
                    Home
                  </Typography>
                )}
              </div>
            </ListItemIcon>
            {/* </Link> */}
          </ListItem>
          <ListItem
            button
            onClick={() => history.push("/sleep")}
            style={{ paddingLeft: "0.5vw" }}>
            <ListItemIcon style={{ color: "white !important" }} color='primary'>
              <div className='d-flex flex-column align-items-center  justify-content-start w-100'>
                <LocalHotelIcon color='primary' fontSize='large' />
                {!open && (
                  <Typography color='primary' variant='subtitle2'>
                    Sleep
                  </Typography>
                )}
              </div>
            </ListItemIcon>
          </ListItem>
          <ListItem
            button
            disabled={!opsAccess}
            style={{ paddingLeft: "0.5vw" }}>
            <ListItemIcon
              style={{ color: "white !important" }}
              color='primary'
              onClick={() => {
                const win = window.open(`#/ops-0`);
                win.focus();
              }}>
              <div className='d-flex flex-column align-items-center  justify-content-start w-100'>
                <ListAltIcon color='primary' fontSize='large' />
                {!open && (
                  <Typography color='primary' variant='subtitle2'>
                    Ops
                  </Typography>
                )}
              </div>
            </ListItemIcon>
          </ListItem>
          <ListItem
            button
            disabled={!alertAccess}
            onClick={() => history.push("/alerts")}
            style={{ paddingLeft: "0.5vw" }}>
            <ListItemIcon style={{ color: "white !important" }} color='primary'>
              <div className='d-flex flex-column align-items-center  justify-content-start w-100'>
                <NotificationsIcon color='primary' fontSize='large' />
                {!open && (
                  <Typography color='primary' variant='subtitle2'>
                    Alerts
                  </Typography>
                )}
              </div>
            </ListItemIcon>
          </ListItem>
          <ListItem
            button
            disabled={!hardwareAccess}
            onClick={() => history.push("/hardware")}
            style={{ paddingLeft: "0.5vw" }}>
            <ListItemIcon style={{ color: "white !important" }} color='primary'>
              <div
                className='d-flex flex-column align-items-center  justify-content-start w-100'
                style={{ wordBreak: "break-all" }}>
                <SettingsInputComponentIcon color='primary' fontSize='large' />
                {!open && (
                  <Typography color='primary' variant='subtitle2'>
                    Hardware
                  </Typography>
                )}
              </div>
            </ListItemIcon>
          </ListItem>
          <ListItem
            button
            disabled={!statsAccess}
            onClick={() => history.push("/stats")}
            style={{ paddingLeft: "0.5vw" }}>
            <ListItemIcon style={{ color: "white !important" }} color='primary'>
              <div
                className='d-flex flex-column align-items-center  justify-content-start w-100'
                style={{ wordBreak: "break-all" }}>
                <TrendingUpIcon color='primary' fontSize='large' />
                {!open && (
                  <Typography color='primary' variant='subtitle2'>
                    Stats
                  </Typography>
                )}
              </div>
            </ListItemIcon>
          </ListItem>
          <ListItem
            button
            disabled={!adminAccess}
            onClick={() => history.push("/admin")}
            style={{ paddingLeft: "0.5vw" }}>
            <ListItemIcon style={{ color: "white !important" }} color='primary'>
              <div className='d-flex flex-column align-items-center  justify-content-start w-100'>
                <SupervisorAccountIcon color='primary' fontSize='large' />
                {!open && (
                  <Typography color='primary' variant='subtitle2'>
                    Admin
                  </Typography>
                )}
              </div>
            </ListItemIcon>
          </ListItem>
        </List>
      </Drawer>
    </div>
  );
}
