import React from "react";
import ProgressBar from "./ProgressBar";

const SleepAnalysisTable = ({ data }) => {
  // const {awakening, snoring, wakeUpAfterSleepOnset, stress, sleepLatency, bedtimeResistance, breath, circadianRhythm, deep, duration, heart, movement, rem, sleepEfficiency,  } = data;

  function generateTable() {
    return Object.keys(data)
      .filter((key) => validKeys(key))
      .map((item) => {
        return (
          <tr className='sleep-analysis-row' key={Math.random()}>
            <td className='sleep-analysis-title'>{rename(item)}</td>
            <td className='sleep-analysis-progress-bar'>
              <ProgressBar
                width={Math.round((data[item] / getMaxValue(item)) * 100) + "%"}
              />
            </td>
            <td className='sleep-analysis-score'>{data[item]}</td>
          </tr>
        );
      });
  }

  function validKeys(key) {
    const keys = [
      "heart",
      "breath",
      "awakening",
      "snoring",
      "wakeUpAfterSleepOnset",
      "stress",
      "sleepLatency",
      "bedtimeResistance",
      "circadianRhythm",
      "deep",
      "duration",
      "movement",
      "rem",
      "sleepEfficiency",
    ];
    return keys.includes(key);
  }

  function rename(name) {
    const titles = {
      awakening: "Awakening",
      snoring: "Snoring",
      wakeUpAfterSleepOnset: "Wakeup after sleep",
      stress: "Stress",
      sleepLatency: "Sleep latency",
      bedtimeResistance: "Bedtime resistance",
      breath: "Respiration",
      circadianRhythm: "Circadian rhythm",
      deep: "Deep sleep",
      duration: "Sleep duration",
      heart: "Heart",
      movement: "Movement",
      rem: "REM",
      sleepEfficiency: "Efficiency",
    };

    return titles[name];
  }

  function getMaxValue(name) {
    const titles = {
      awakening: 5,
      snoring: 5,
      wakeUpAfterSleepOnset: 5,
      stress: 10,
      sleepLatency: 5,
      bedtimeResistance: 5,
      breath: 10,
      circadianRhythm: 5,
      deep: 10,
      duration: 10,
      heart: 10,
      movement: 5,
      rem: 10,
      sleepEfficiency: 5,
    };

    return titles[name];
  }
  return (
    <table className='sleep-analysis-tbl'>
      <tbody>{generateTable()}</tbody>
    </table>
  );
};

export default SleepAnalysisTable;
