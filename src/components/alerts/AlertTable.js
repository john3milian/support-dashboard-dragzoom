import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";

import { TableHead } from "@material-ui/core";
import Moment from "react-moment";
import AddIcon from "@material-ui/icons/Add";
import SimpleModal from "./Modal";
import AlertRow from "./AlertRow";

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label='first page'>
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label='previous page'>
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label='next page'>
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label='last page'>
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

const useStyles2 = makeStyles({
  table: {
    minWidth: 500,
  },
});

export default function AlertTable({
  alertsData,
  loading,
  selectedFilter,
  fetchData,
}) {
  const classes = useStyles2();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const rows = alertsData;

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  const [render, setRender] = useState(false);
  const [open, setOpen] = React.useState(false);
  const [isNurseInput, setIsNurseInput] = useState(true);
  const [selectedRow, setSelectedRow] = useState({});

  // const temp_rows = rows.filter((eachItem) => {
  //   return Object.keys(eachItem["Attender"]).length === 0;
  // });

  useEffect(() => {
    localStorage.setItem("modalOpen", false);
  }, []);
  useEffect(() => {
    setTimeout(() => {
      // const checkOpen = open;
      const checkOpen = localStorage.getItem("modalOpen");
      console.log("Open's status=", checkOpen);

      if (checkOpen === "false") {
        console.log("Its not!!!!!!!!!!!!!");
        // fetchData([]);
      } else {
        console.log("It is ");
      }
      setRender(!render);
    }, 30000);
  }, [render]);

  // setInterval(() => {
  //   console.log("Checking if open of not!!!!!!!!!!!!!");
  //   if (!open) {
  //     console.log("Its not!!!!!!!!!!!!!");
  //     fetchData([]);
  //   } else {
  //     console.log("It is ");
  //   }
  //   setRender(!render);
  // }, 30000);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleOpen = (isNurse) => {
    setIsNurseInput(isNurse);
    setOpen(true);
    localStorage.setItem("modalOpen", true);
  };

  const handleClose = () => {
    localStorage.setItem("modalOpen", false);
    setOpen(false);
  };

  return (
    <>
      <SimpleModal
        selectedRow={selectedRow}
        open={open}
        isNurseInput={isNurseInput}
        handleClose={handleClose}
        setOpen={setOpen}
        handleOpen={handleOpen}
      />

      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label='custom pagination table'>
          <TableHead style={{ backgroundColor: "#f2f2f2" }}>
            <TableRow>
              <TableCell align='center'>Device Name</TableCell>
              <TableCell align='center'>Hospital</TableCell>
              <TableCell align='center'>Patient</TableCell>
              <TableCell align='center'>Ward-Bed No</TableCell>
              <TableCell align='center'>Alert Type</TableCell>
              <TableCell align='center'>Vital Value</TableCell>
              <TableCell align='center'>Alert Time</TableCell>
              <TableCell align='center'>Group ID</TableCell>
              <TableCell align='center'>Escalate</TableCell>
              <TableCell align='center'>Nurse Input</TableCell>
              <TableCell align='center'>
                Attended{" "}
                <b>
                  {(selectedFilter === "unattended" ||
                    selectedFilter === "attended") &&
                    "(" + rows.length + ")"}
                </b>
              </TableCell>
              <TableCell align='center'>Alert Success</TableCell>
              <TableCell align='center'>Remarks</TableCell>
              {/* <TableCell align='center'>Alert Result</TableCell> */}
            </TableRow>
          </TableHead>

          <TableBody>
            {(rowsPerPage > 0
              ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              : rows
            ).map((row, key) => (
              <AlertRow
                isMainRow={true}
                row={row}
                key={key}
                setSelectedRow={setSelectedRow}
                handleOpen={handleOpen}
                setIsNurseInput={setIsNurseInput}
                selectedRow={selectedRow}
                open={open}
                isNurseInput={isNurseInput}
                handleClose={handleClose}
                setOpen={setOpen}
              />
            ))}

            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[5, 10, 50, { label: "All", value: -1 }]}
                colSpan={0}
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: { "aria-label": "rows per page" },
                  native: true,
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
                // rowsPerPageOptions={[10, 25, 50, 100]}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    </>
  );
}
