import React, { useEffect, useState } from "react";

import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Moment from "react-moment";
import {
  Button,
  Checkbox,
  Divider,
  LinearProgress,
  Select,
  Typography,
} from "@material-ui/core";
import API from "../../api";

function AlertRow({
  row,
  setSelectedRow,
  handleOpen,
  setIsNurseInput,
  open,
  selectedRow,
  isNurseInput,
  handleClose,
  setOpen,
  isMainRow,
}) {
  const [isAttended, setIsAttended] = useState();
  const [opRemark, setOpRemark] = useState([]);
  const [stateValue, setStateValue] = useState("");
  const [showDetailedRows, setShowDetailedRows] = useState(false);
  const [groupDetails, setGroupDetails] = useState([]);
  const [groupFetchLoader, setGroupFetchLoader] = useState(false);
  const numOfCells = [1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3];
  useEffect(() => {
    // console.log(Object.keys(row.Attender).length ? true : false);
    setIsAttended(Object.keys(row.Attender).length ? true : false);
    setOpRemark(row.Remarks);
    setStateValue(row.State);
  }, [row]);

  const changeAttender = async () => {
    const timeElapsed = Date.now();
    const today = new Date(timeElapsed);
    const emailId = localStorage.getItem("userEmail");
    const results = await API.post(
      `/api/smartalerts/update?smartAlertId=${row.SmartAlertId}`,
      {
        Attender: {
          name: emailId,
          timestamp: today.toISOString(),
        },
      }
      // {
      //   method: "POST",
      //   body: JSON.stringify(),
      // }
    );

    const body = await results.data;
  };

  const changeState = async (newState) => {
    const results = await API.post(
      `/api/smartalerts/update?smartAlertId=${row.SmartAlertId}`,
      {
        State: newState,
      }
    );

    const body = await results.data;
  };

  const fetchByGroupId = async () => {
    if (!showDetailedRows) {
      setShowDetailedRows(!showDetailedRows);
      const groupResults = await fetch(
        `https://alerts.senslabs.io/api/smartalerts/groups/list?groupId=${row.GroupId}`
      );

      let groupBody = await groupResults.json();

      groupBody = groupBody.filter((eachItem) => {
        return Object.values(eachItem.SmartAlertId) !== row.SmartAlertId;
      });
      setGroupDetails(groupBody);
      setGroupFetchLoader(false);
    } else {
      setGroupDetails([]);
      setShowDetailedRows(false);
    }
  };
  return (
    <>
      <TableRow
        className={
          isMainRow
            ? showDetailedRows
              ? "parentRow row_" + row?.ColorCode?.toLowerCase()
              : "row_" + row?.ColorCode?.toLowerCase()
            : "childRow row_" + row?.ColorCode?.toLowerCase()
        }>
        <TableCell
          className='Link-text'
          align='center'
          component='th'
          scope='row'
          style={{ cursor: "pointer" }}
          onClick={() => {
            const win = window.open(
              `https://health.senslabs.io/#/pages/operator/redirect?organizationId=${row.OrganizationId}&userId=${row.UserId}&deviceId=${row.DeviceId}`
            );
            win.focus();
          }}>
          <Typography variant='caption'>
            <span style={{ textDecoration: "underline", fontWeight: 600 }}>
              {row.DevicePrefix &&
                row.DeviceSequence &&
                row.DevicePrefix + "-" + row.DeviceSequence}
              {!row.DevicePrefix && !row.DeviceSequence && " -- "}
            </span>
          </Typography>
        </TableCell>
        <TableCell
          align='center'
          style={{
            cursor: "pointer",
          }}
          onClick={() => {
            const win = window.open(
              `https://health.senslabs.io/#/pages/operator/redirect?organizationId=${row.OrganizationId}`
            );
            win.focus();
          }}
          className='Link-text'>
          <Typography variant='caption'>
            <span
              style={{
                textDecoration: "underline",
                fontWeight: 600,
                maxWidth: " 100px !important",
              }}>
              {row.OrganizationNames.map((eachName, index) =>
                row.OrganizationNames.length !== index + 1
                  ? eachName + ", "
                  : eachName
              )}
            </span>
          </Typography>
        </TableCell>
        <TableCell align='center' style={{ maxWidth: "100px" }}>
          <Typography variant='caption'>{row.UserName}</Typography>
        </TableCell>
        <TableCell align='center'>
          <Typography variant='caption'>
            {row.Ward ? (row.Bed ? row.Ward + "_" + row.Bed : "_ _") : "_ _"}
          </Typography>
        </TableCell>
        <TableCell align='center'>
          <Typography variant='caption'>{row.AlertCode}</Typography>
        </TableCell>
        <TableCell align='center'>
          <Typography variant='caption'>{row.ParameterValue}</Typography>
        </TableCell>
        <TableCell align='center' style={{ maxWidth: "8vw" }}>
          <Typography variant='caption'>
            <Moment format='Do MMM YYYY HH:mm:ss' utc local>
              {row.CreatedAt}
            </Moment>
          </Typography>
        </TableCell>
        <TableCell
          align='center'
          onClick={() => {
            isMainRow && setGroupFetchLoader(true);
            isMainRow && fetchByGroupId();
          }}
          className='Link-text'
          style={{ maxWidth: "12vw", cursor: "pointer" }}>
          <Typography
            variant='caption'
            style={{ textDecoration: "underline", fontWeight: 600 }}>
            {row.GroupId}{" "}
            <b>
              {groupDetails.length > 0 ? "(" + groupDetails.length + ")" : " "}
            </b>
          </Typography>
        </TableCell>
        <TableCell align='center' style={{ minWidth: "1vw" }}>
          <Typography variant='caption'>
            {/* {row?.Escalations?.Type && "Type : "} */}
            {(row?.Escalations?.Type &&
              Object.values(row.Escalations)[1].toLowerCase() ===
                "valid alert") ||
            Object.values(row.Escalations)[1].toLowerCase() === "valid"
              ? "Yes"
              : Object.values(row.Escalations)}
          </Typography>
        </TableCell>
        <TableCell align='center'>
          <Typography variant='caption'>
            <div>
              {/* {row.NurseInputs[0] && Object.keys(row?.NurseInputs[0]) + " :"} */}
              {row.NurseInputs[0] &&
                Object.values(row.NurseInputs[0])[0] +
                  " : " +
                  Object.values(row.NurseInputs[0])[1]}
            </div>
            {row.NurseInputs[0] && <Divider className='mt-1 mb-1' />}
            {row.NurseInputs[0] ? (
              <Button
                style={{ outline: 0 }}
                onClick={() => {
                  setSelectedRow(row);
                  handleOpen(true);
                }}>
                See more...
              </Button>
            ) : (
              <Button
                color='primary'
                variant='contained'
                style={{ outline: 0 }}
                onClick={() => {
                  setSelectedRow(row);
                  handleOpen(true);
                }}>
                {/* <AddIcon /> */}
                Add Nurse Input
              </Button>
            )}
          </Typography>
        </TableCell>
        <TableCell align='center'>
          <Typography variant='caption'>
            <Checkbox
              checked={isAttended ? true : false}
              // checked={isAttended}
              onChange={() => {
                setIsAttended(true);
                changeAttender();
              }}
              // disabled={Object.keys(row.Attender).length ? true : false}
              color='primary'
              inputProps={{ "aria-label": "secondary checkbox" }}
            />
            {/* {row.Attender === {} ? (
                    <Checkbox
                      checked
                      color='primary'
                      inputProps={{ "aria-label": "secondary checkbox" }}
                    />
                  ) : (
                    
                  )} */}
          </Typography>
        </TableCell>
        <TableCell align='center'>
          <Typography variant='caption'>
            <Select
              native
              // value={state.age}
              // onChange={handleChange}
              fullWidth
              value={stateValue}
              onChange={(e) => {
                setStateValue(e.target.value);
                changeState(e.target.value);
              }}
              inputProps={{
                name: "age",
                id: "age-native-simple",
              }}>
              <option value={"Null"}>--</option>
              <option value={"yes"}>Yes</option>
              <option value={"no"}>No</option>
            </Select>
          </Typography>
        </TableCell>
        <TableCell align='center'>
          <Typography variant='caption'>
            <div style={{ maxWidth: "8vw" }}>
              <div>
                <span>
                  <b>
                    {row.Remarks[0] && Object.values(row.Remarks[0])[0] + " :"}
                  </b>
                  {row.Remarks[0] && Object.values(row.Remarks[0])[1]}
                </span>
              </div>
              {row.Remarks[0] && <Divider className='mt-1 mb-1' />}
              {row.Remarks[0] ? (
                <Button
                  style={{ outline: 0 }}
                  onClick={() => {
                    setSelectedRow(row);
                    handleOpen(false);
                  }}>
                  See more...
                </Button>
              ) : (
                <Button
                  color='primary'
                  variant='contained'
                  style={{ outline: 0 }}
                  onClick={() => {
                    setSelectedRow(row);
                    handleOpen(false);
                  }}>
                  {/* <AddIcon /> */}
                  Add Remark
                </Button>
              )}
            </div>
          </Typography>
        </TableCell>
      </TableRow>
      {showDetailedRows && groupFetchLoader ? (
        <TableRow>
          {numOfCells.map(() => (
            <TableCell>
              <LinearProgress />
            </TableCell>
          ))}
        </TableRow>
      ) : (
        groupDetails.length > 1 &&
        groupDetails.map((groupDetRow, key) => (
          <AlertRow
            isMainRow={false}
            row={groupDetRow}
            key={key}
            setSelectedRow={setSelectedRow}
            handleOpen={handleOpen}
            setIsNurseInput={setIsNurseInput}
            selectedRow={selectedRow}
            open={open}
            isNurseInput={isNurseInput}
            handleClose={handleClose}
            setOpen={setOpen}
          />
        ))
      )}
    </>
  );
}

export default AlertRow;
