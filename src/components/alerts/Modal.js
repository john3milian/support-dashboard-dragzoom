import React, { useEffect, useRef, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import CloseIcon from "@material-ui/icons/Close";
import "../../css/Modal.css";
import {
  Button,
  CircularProgress,
  Fade,
  FormControl,
  InputAdornment,
  MenuItem,
  Select,
  TextField,
  Typography,
} from "@material-ui/core";
import API from "../../api";
import { Alert } from "@material-ui/lab";

function getModalStyle() {
  return {
    top: `40%`,
    left: `40%`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function SimpleModal({
  open,
  handleClose,
  handleOpen,
  setOpen,
  selectedRow,
  isNurseInput,
}) {
  const classes = useStyles();
  // getModalStyle is not a pure function, we roll the style only on the first render
  const [modalStyle] = React.useState(getModalStyle);
  const [nurseName, setNurseName] = useState("");
  const [nurseRemark, setNurseRemark] = useState("");
  const [thisRow, setThisRow] = useState({ remarks: [] });
  const [loading, setloading] = useState(false);
  const [modalTop, setModalTop] = useState("40%");
  const [customRemark, setCustomRemark] = useState(true);
  const [isFeedback, setIsFeedback] = useState(false);

  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const fixedOperatorRemarks = [
    "Informed",
    "No response",
    "Transferred to command centre",
    "No contact details",
    "Insufficient data",
  ];

  useEffect(() => {
    setThisRow(selectedRow);
    setCustomRemark(false);
  }, [open, selectedRow]);
  const userEmail = localStorage.getItem("userEmail");
  const elementRef = useRef();

  useEffect(() => {
    // const height = document.getElementById("myModal").clientHeight;
    setTimeout(() => {
      if (elementRef.current) {
        if (
          elementRef.current.clientHeight > 400 &&
          elementRef.current.clientHeight < 500
        ) {
          setModalTop("29%");
        } else {
          if (elementRef.current.clientHeight > 500) {
            setModalTop("20%");
          }
        }
      }
    }, 200);
  }, [open, loading, elementRef]);

  const handleAddNewRemark = async () => {
    setloading(true);
    const bodyObj = {
      name: userEmail,
      remark: isFeedback ? "Feedback: " + nurseRemark : nurseRemark,
    };
    API.post(
      `/api/smartalerts/remarks/add?smartAlertId=${thisRow.SmartAlertId}`,
      bodyObj
      // {
      //   method: "POST",
      //   body: JSON.stringify(bodyObj),
      // }
    )
      .then(() => {
        setloading(false);
        handleClose();
      })
      .catch((error) => {
        setloading(false);
        setError(true);
        setErrorMsg(error?.response?.data?.Action);
      });
  };

  const handleAddNewInput = async () => {
    setloading(true);
    const bodyObj = {
      name: nurseName,
      remark: nurseRemark,
    };
    API.post(
      `/api/smartalerts/nurseinputs/add?smartAlertId=${thisRow.SmartAlertId}`,
      bodyObj
    )
      .then(() => {
        setloading(false);
        handleClose();
      })
      .catch((error) => {
        setloading(false);
        setError(true);
        setErrorMsg(error?.response?.data?.Action);
      });
  };

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='simple-modal-title'
        aria-describedby='simple-modal-description'>
        <div
          // style={modalStyle}
          className={classes.paper}
          ref={elementRef}
          style={{ top: `${modalTop}`, left: `40%` }}
          id='myModal'>
          <Button
            onClick={handleClose}
            style={{ position: "absolute", top: 10, right: 10 }}>
            <CloseIcon />
          </Button>
          {isNurseInput && !loading && (
            <>
              {error ? (
                <Alert severity='error'>{errorMsg}</Alert>
              ) : (
                <>
                  <h3 id='simple-modal-title'>Nurse Input</h3>
                  <p id='simple-modal-description'>
                    {thisRow?.NurseInputs?.length === 0 && "No Remarks"}
                    {thisRow?.NurseInputs?.length !== 0 &&
                      thisRow?.NurseInputs?.map((remark) => (
                        <div>
                          {remark !== null && (
                            <p className='mb-0'>
                              <span>
                                <b>
                                  {Object.values(remark).length !== 0 &&
                                    Object.values(remark)[0] &&
                                    Object.values(remark)[0].split("@")[0]}
                                </b>
                              </span>
                              {": "}
                              <span>
                                {Object.values(remark).length !== 0 &&
                                  Object.values(remark)[1] &&
                                  Object.values(remark)[1]}
                              </span>
                            </p>
                          )}
                        </div>
                      ))}
                  </p>

                  <div className='d-flex flex-column remarks-textboxes align-items-start w-100'>
                    {/* <input
                  placeholder='Nurse Name'
                  value={nurseName}
                  onChange={(e) => setNurseName(e.target.value)}
                /> */}
                    <Typography>{userEmail}</Typography>
                    <TextField
                      multiline
                      rows={3}
                      onKeyPress={(e) => {
                        if (e.key === "Enter" && !e.shiftKey) {
                          handleAddNewInput();
                        }
                      }}
                      variant='outlined'
                      className='mt-2 w-100'
                      placeholder='Remarks'
                      value={nurseRemark}
                      onChange={(e) => setNurseRemark(e.target.value)}
                    />
                  </div>
                  <div className='w-100 d-flex justify-content-between mt-4'>
                    <Button
                      style={{ outline: 0 }}
                      onClick={handleClose}
                      color='secondary'
                      variant='contained'>
                      Cancel
                    </Button>
                    <Button
                      style={{ outline: 0 }}
                      color='primary'
                      variant='contained'
                      onClick={handleAddNewInput}>
                      Add New Remark
                    </Button>
                  </div>
                </>
              )}
            </>
          )}
          {loading && (
            <div className='d-flex align-items-center justify-content-center'>
              <CircularProgress />
            </div>
          )}
          {!isNurseInput && !loading && (
            <>
              {error ? (
                <Alert severity='error'>{errorMsg}</Alert>
              ) : (
                <>
                  <h3 id='simple-modal-title'>Remarks</h3>
                  <p id='simple-modal-description'>
                    {thisRow?.Remarks?.length === 0 && "No Remarks"}
                    {thisRow?.Remarks &&
                      thisRow?.Remarks?.map((remark) => (
                        <div>
                          {remark !== null && (
                            <p className='mb-0'>
                              <span>
                                <b>
                                  {Object.values(remark).length !== 0 &&
                                    Object.values(remark)[0] &&
                                    Object.values(remark)[0]}
                                </b>
                              </span>
                              {": "}
                              <span>
                                {Object.values(remark).length !== 0 &&
                                  Object.values(remark)[1] &&
                                  Object.values(remark)[1]}
                              </span>
                            </p>
                          )}
                        </div>
                      ))}
                  </p>

                  <form className='d-flex flex-column remarks-textboxes align-items-start w-100'>
                    <Typography>{userEmail}</Typography>
                    <FormControl
                      fullWidth
                      variant='outlined'
                      className='mt-2'
                      onKeyPress={(e) => {
                        e.preventDefault();
                        if (e.key === "Enter") {
                          console.log("SUBMITTED");
                          // handleAddNewRemark();
                        }
                      }}>
                      <Select
                        defaultValue={"--- Please select a remark ---"}
                        labelId='demo-simple-select-outlined-label'
                        id='demo-simple-select-outlined'>
                        <MenuItem
                          aria-label='None'
                          value='--- Please select a remark ---'>
                          --- Please select a remark ---
                        </MenuItem>

                        {fixedOperatorRemarks.map((eachRemak) => (
                          <MenuItem
                            onClose={(e) => {
                              e.preventDefault();
                              if (e.key === "Enter") {
                                console.log("SUBMITTED");
                                // handleAddNewRemark();
                              }
                            }}
                            value={eachRemak}
                            onClick={() => {
                              setCustomRemark(false);
                              setNurseRemark(eachRemak);
                              setIsFeedback(false);
                            }}>
                            {eachRemak}
                          </MenuItem>
                        ))}
                        <MenuItem
                          value={9}
                          onClick={() => {
                            setNurseRemark("");
                            setCustomRemark(true);
                            setIsFeedback(true);
                          }}>
                          Feedback
                        </MenuItem>
                        <MenuItem
                          value={1}
                          onClick={() => {
                            setNurseRemark("");
                            setCustomRemark(true);
                            setIsFeedback(false);
                          }}>
                          Other
                        </MenuItem>
                      </Select>
                    </FormControl>
                    <Fade in={customRemark}>
                      <TextField
                        multiline
                        rows={3}
                        onKeyPress={(e) => {
                          if (e.key === "Enter" && !e.shiftKey) {
                            // console.log("SUBMITTED");
                            handleAddNewRemark();
                          }
                        }}
                        InputProps={
                          isFeedback && {
                            startAdornment: (
                              <InputAdornment position='start'>
                                Feedback :
                              </InputAdornment>
                            ),
                          }
                        }
                        variant='outlined'
                        className='mt-2 w-100'
                        placeholder={!isFeedback && "Remarks"}
                        value={!customRemark ? "" : nurseRemark}
                        onChange={(e) => setNurseRemark(e.target.value)}
                      />
                    </Fade>
                  </form>
                  <div className='w-100 d-flex justify-content-between mt-4'>
                    <Button
                      style={{ outline: 0 }}
                      onClick={handleClose}
                      color='secondary'
                      variant='contained'>
                      Cancel
                    </Button>

                    <Button
                      disabled={nurseRemark.length === 0}
                      style={{ outline: 0 }}
                      color='primary'
                      variant='contained'
                      onClick={handleAddNewRemark}>
                      Submit
                    </Button>
                  </div>
                </>
              )}
            </>
          )}
        </div>
      </Modal>
    </div>
  );
}
