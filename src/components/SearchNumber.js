import { FormControl, InputLabel, Select, TextField } from "@material-ui/core";
import React, { useState } from "react";

function SearchNumber({
  prefix,
  setPrefix,
  deviceIdentifier,
  setDeviceIdentifier,
  setShowError,
  fromHardware,
}) {
  return (
    <>
      <FormControl
        variant='outlined'
        size='small'
        style={{ outline: 0 }}
        className={fromHardware && "w-25"}>
        <Select
          style={{ outline: 0 }}
          native
          value={prefix}
          onChange={(e) => setPrefix(e.target.value)}>
          <option value={"DOZ"}>DOZ</option>
          <option value={"DP"}>DP</option>
          <option value={"ID"}>Device Id</option>
        </Select>
      </FormControl>
      <div className={fromHardware ? "w-100" : "w-50"}>
        <TextField
          // onFocus={() => setShowError(false)}
          required
          variant='outlined'
          type={prefix === "ID" ? "text" : "number"}
          placeholder={
            prefix === "DOZ" ? "1234" : prefix === "DP" ? "12345" : "Device Id "
          }
          size='small'
          style={{ outline: 0 }}
          fullWidth
          value={deviceIdentifier}
          onChange={(e) => setDeviceIdentifier(e.target.value)}
        />
      </div>
    </>
  );
}

export default SearchNumber;
