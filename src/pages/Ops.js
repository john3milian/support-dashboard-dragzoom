import React, { useState, useEffect } from "react";
import { MD5 } from "crypto-js";
import TableGenerator from "../components/TableGenerator";
import { Redirect, useHistory, useLocation, useParams } from "react-router-dom";
import { COLUMNS } from "../helpers/OpsCols";
// import Loader from "./Loader";
// import Logo from "../dozee-logo.png";
import "../css/Ops.css";
import "../css/table.css";
import { getDate } from "date-fns";
import { Button, CircularProgress, Paper } from "@material-ui/core";
import SearchNumber from "../components/SearchNumber";
import API from "../api";
import { Alert } from "@material-ui/lab";
import NotAuthorized from "../components/central/NotAuthorized";

const HOUR = 1000 * 60 * 60;
const HOURS8 = HOUR * 8;
const DAY = HOUR * 24;
const WEEK = DAY * 7;
const FIFTEEN = DAY * 30;
// const MONTH = DAY * 30;

const Ops = (props) => {
  const [dataRangeInput, setDataRangeInput] = useState(HOUR * 8);
  const [simData, setSimData] = useState("");
  const [filterData, setFilterData] = useState("");

  const [leanSimData, setLeanSimData] = useState("");
  const [leanFilterData, setLeanFilterData] = useState("");

  const [rows, setRows] = useState("");
  const [ready, setReady] = useState(false);

  const [processingTimeAvg, setProcessingTimeAvg] = useState([]);
  const [finalProcessingAvg, setFinalProcessingTimeAvg] = useState();
  const [searchDeviceId, setSearchDeviceId] = useState("");
  const [prefix, setPrefix] = useState("DOZ");

  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const history = useHistory();
  const setProcessingTimeAvgfunc = (value) => {
    setProcessingTimeAvg(value);
  };
  const [fromDate, setFromDate] = useState(
    new Date(new Date() - HOURS8).toISOString()
  );
  // const [fromDate, setFromDate] = useState(new Date(new Date() - MONTH).toISOString());
  // const [fromDate, setFromDate] = useState(new Date().toISOString());
  const [toDate] = useState(new Date().toISOString());
  const { deviceId } = useParams();
  // console.log(COLUMNS);
  //simulator
  const location = useLocation();
  useEffect(() => {
    // getSimData(deviceId, fromDate, toDate, setSimData);
    // getFilterData(deviceId, fromDate, toDate, setFilterData);
    // const checkDeviceId = localStorage.getItem("deviceIdentifier");
    // console.log(checkDeviceId);
    // if (
    //   checkDeviceId !== null &&
    //   checkDeviceId.length > 0 &&
    //   checkDeviceId !== undefined
    // ) {
    //   handleNewDeviceSearch();
    // }
    const tempVal = {
      deviceId: deviceId,
      fromDate: fromDate,
      toDate: toDate,
      setSimData: setSimData,
      setFilterData: setFilterData,
      setError: setError,
      setErrorMsg: setErrorMsg,
    };
    getData(tempVal);
  }, [deviceId, fromDate, toDate, setSimData, setFilterData, location]);

  useEffect(() => {
    if (simData && filterData) {
      // console.log('data loaded!')
      setLeanSimData(cleanup(simData));
      setLeanFilterData(cleanup(filterData));
    } else {
      console.log("loading ... ");
    }
  }, [simData, filterData]);

  useEffect(() => {
    if (rows) {
      setReady(true);
    }
  }, [rows]);

  useEffect(() => {
    setFinalProcessingTimeAvg(processingTimeAvg);
  }, [processingTimeAvg]);
  useEffect(() => {
    if (leanSimData && leanFilterData) {
      console.log("leanSimData: ", leanSimData.length, leanSimData[0]);
      console.log("leanFilterData: ", leanFilterData.length, leanFilterData[0]);

      // mapper(leanFilterData);
      generateRows();
    }
  }, [leanSimData, leanFilterData]);

  function generateRows() {
    const rows = [];

    for (let data of leanSimData) {
      const row = {
        identifier: "",
        headerTimeStamp: "",
        uploadedAt: "",
        startTime: "",
        endTime: "",
        dbInsert: "",
        status: "",
        sleepId: "",
      };
      // console.log(data.key);
      const key = MD5(data.key).toString();
      const key2 = MD5(data.key.substring(1, data.key.length)).toString();
      // console.log(key, key2);
      const keyData = collectData(key, key2);

      row.identifier = idGenerator(data.key);
      row.uploadedAt = data.uploaded_at;

      if (keyData.startTime) row.startTime = keyData.startTime;
      if (keyData.endTime) row.endTime = keyData.endTime;
      if (keyData.dbInsert) row.dbInsert = keyData.dbInsert;
      if (keyData.status) row.status = keyData.status;
      if (keyData.sleepId) row.sleepId = keyData.sleepId;
      if (keyData.headerTimeStamp)
        row.headerTimeStamp = keyData.headerTimeStamp;

      rows.push(row);
    }
    try {
      // console.log("first: ", rows[0]);
      // console.log("mid: ", rows[~~(rows.length / 2)]);
      const temp_data = rows[rows.length - 1];
      // const started = new Date(temp_data["endTime"]).getTime();
      const ended = new Date(temp_data["endTime"]).getTime();
      // const secsToEnd = Math.round((ended - started) / 1000);
      console.log(new Date(ended).toTimeString().split(" ")[0]);
      rows.sort((a, b) => {
        if (a.headerTimeStamp > b.headerTimeStamp) {
          return -1;
        }
        if (a.headerTimeStamp < b.headerTimeStamp) {
          return 1;
        }
        return 0;
      });
    } catch {}
    setRows(rows);
  }

  function idGenerator(str) {
    return str.split("/").reverse()[1] + "/" + str.split("/").reverse()[0];
  }
  useEffect(() => {
    setProcessingTimeAvg([]);
  }, []);

  function collectData(id, id2) {
    const data = {
      rows: [],
      startTime: "",
      endTime: "",
      dbInsert: "",
      status: "",
      sleepId: "",
      headerTimeStamp: "",
    };

    for (let row of leanFilterData) {
      if (row.identifier === id || row.identifier === id2) {
        data.rows.push(row);

        // if(row.header_time){
        // 	data.headerTimeStamp = row.header_time;
        // }

        if (row.log) {
          if (row.log === "Starting Filter") {
            data.startTime = row["@timestamp"];
          } else if (row.log === "Finished Filter") {
            data.endTime = row["@timestamp"];
            data.status = "Processed";

            data.headerTimeStamp = row.header_time;
          } else if (row.log === "Finished push_data") {
            data.dbInsert = "Successful";
          } else if (row.log.includes("Sleep Id: ")) {
            data.sleepId = row.log.split("Sleep Id: ")[1];
          }
        } else {
          console.log("FAILED ROW!!!!");
        }
      }
    }

    if (!data.startTime && !data.endTime) {
      data.status = "Uploaded";
    } else if (data.startTime && !data.endTime) {
      data.status = "Under processing";
    }
    return data;
  }

  useEffect(() => {
    console.log("dataRangeInput: ", dataRangeInput);
    setFromDate(new Date(new Date() - dataRangeInput).toISOString());
  }, [dataRangeInput]);

  const handleRangeSubmit = (e) => {
    e.preventDefault();
    setReady(false);
    const tempVal = {
      deviceId: deviceId,
      fromDate: fromDate,
      toDate: toDate,
      setSimData: setSimData,
      setFilterData: setFilterData,
      setError: setError,
      setErrorMsg: setErrorMsg,
    };
    getData(tempVal);
  };

  const autoCompleteDOZNumber = (deviceIdentifier) => {
    let auto_append_zeros = "0".repeat(4 - deviceIdentifier.length);
    if (auto_append_zeros.length > 0) {
      return `${prefix}-${auto_append_zeros}${deviceIdentifier}`;
    } else {
      return `${prefix}-${deviceIdentifier}`;
    }
  };
  const autoCompleteDPNumber = (deviceIdentifier) => {
    let auto_append_zeros = "0".repeat(5 - deviceIdentifier.length);
    if (auto_append_zeros.length > 0) {
      return `${prefix}-${auto_append_zeros}${deviceIdentifier}`;
    } else {
      return `${prefix}-${deviceIdentifier}`;
    }
  };

  const handleNewDeviceSearch = () => {
    const fetchData = async () => {
      let temp_var;
      if (prefix === "ID") {
        temp_var = `deviceInfoByID?deviceid=${searchDeviceId}`;
      } else if (prefix === "DOZ") {
        temp_var = autoCompleteDOZNumber(searchDeviceId);
      } else {
        temp_var = autoCompleteDPNumber(searchDeviceId);
      }
      const results = await API.get(
        // `http://139.59.20.84:2380/api/v1/${api_postfix}`
        // `https://senslabs.io/api/v1/deviceInfoByName?devicename=${searchDeviceId}`
        `/api/v1/deviceInfoByName?devicename=${temp_var}`
      )
        .then((results) => {
          const body = results.data;
          // console.log(body);
          const deviceId = body["Device_Info"]["Device_ID"];
          setReady(true);
          localStorage.setItem("deviceIdentifier", deviceId);
          history.push(`ops-${deviceId}`);
          const tempVal = {
            deviceId: deviceId,
            fromDate: fromDate,
            toDate: toDate,
            setSimData: setSimData,
            setFilterData: setFilterData,
            setError: setError,
            setErrorMsg: setErrorMsg,
          };
          getData(tempVal);
        })
        .catch((error) => {
          setError(true);
          setErrorMsg(error?.response?.data?.Action);
        });
    };

    // console.log(searchDeviceId.length);

    if (searchDeviceId.length < 10) {
      fetchData();
    } else {
      localStorage.setItem("deviceIdentifier", searchDeviceId);
      setReady(true);
      history.push(`ops-${searchDeviceId}`);
      // const win = window.open(`#/ops-${searchDeviceId}`);
      // win.focus();
    }
  };

  return (
    <div>
      <div>
        <span className='users-table-logo-title-span'>
          {/* <img src={Logo} alt='dozee-logo' /> */}
          <span className='users-table-heading'>Ops Dashboard</span>
        </span>
        <div className='users-page-header-div'>
          <span className='ops-deviceId-outer-span'>
            Device Id
            <span className='ops-deviceId-inner-span'>{deviceId}</span>
          </span>
          <form className='ops-new-device-id' style={{ color: "white" }}>
            <SearchNumber
              setPrefix={setPrefix}
              prefix={prefix}
              deviceIdentifier={searchDeviceId}
              setDeviceIdentifier={setSearchDeviceId}
              fromHardware={false}
            />
            {/* <input
              value={searchDeviceId}
              onChange={(e) => setSearchDeviceId(e.target.value)}
              id='search-device'
              type='text'
              placeholder='DEVICE ID / DEVICE NUMBER'
            /> */}
            <Button
              color='primary'
              variant='contained'
              type='submit'
              className=''
              onClick={(e) => {
                e.preventDefault();
                setReady(false);
                handleNewDeviceSearch();
              }}>
              Go
            </Button>
          </form>
        </div>
      </div>
      {!error ? (
        <div className='table-div'>
          <div className='sleeps-range-div'>
            <span className='range-options-span'>
              <form
                className='ops-date-range-input-form'
                onSubmit={(e) => handleRangeSubmit(e)}>
                <span className='inner-range-span'>
                  <span>
                    <label htmlFor='8-hours'>8 hours</label>
                  </span>{" "}
                  <input
                    type='radio'
                    value='8 hours'
                    name='range-range-input'
                    onChange={() => setDataRangeInput(HOURS8)}
                    id='8-hours'
                    checked={dataRangeInput === 28800000 ? true : false}
                  />
                </span>
                <span className='inner-range-span'>
                  <span>
                    <label htmlFor='1-day'>1 day</label>
                  </span>
                  <input
                    type='radio'
                    value='1 day'
                    name='range-range-input'
                    onChange={() => setDataRangeInput(DAY)}
                    id='1-day'
                    checked={dataRangeInput === 86400000 ? true : false}
                  />
                </span>
                <span className='inner-range-span'>
                  <span>
                    <label htmlFor='7-days'>7 days</label>
                  </span>
                  <input
                    type='radio'
                    value='7 days'
                    name='range-range-input'
                    onChange={() => setDataRangeInput(WEEK)}
                    id='7-days'
                    checked={dataRangeInput === 604800000 ? true : false}
                  />
                </span>
                {/* <span className='inner-range-span'>
                <span>
                  <label htmlFor='30-days'>15 days</label>
                </span>
                <input
                  type='radio'
                  value='30 days'
                  name='range-range-input'
                  onChange={() => setDataRangeInput(FIFTEEN)}
                  id='30-days'
                  checked={dataRangeInput === 2592000000 ? true : false}
                />
              </span> */}
                <Button
                  type='submit'
                  color='primary'
                  variant='contained'
                  className='data-range-btn'>
                  Go
                </Button>
              </form>
            </span>
            <span className='ops-date-range-input-form d-flex align-items-center'>
              {/* {setTimeout(() => {
              "processingTimeAvg.length";
            }, 10000)}{" "} */}{" "}
              -- total elements processed. Average Processing End Time
              <Paper
                className='data-range-btn'
                style={{
                  margin: ".1em",
                  marginLeft: "10px",
                  padding: ".7em 1em",
                  color: "white",
                  backgroundColor: "#2f5f7e",
                  cursor: "text",
                }}>
                --
                {/* 10 */}
              </Paper>
            </span>
          </div>
          {ready ? (
            <TableGenerator
              users={rows}
              COLUMNS={COLUMNS}
              search={false}
              setProcessingTimeAvgfunc={setProcessingTimeAvgfunc}
              setProcessingTimeAvg={setProcessingTimeAvg}
              processingTimeAvg={processingTimeAvg}
            />
          ) : (
            <div
              className=''
              style={{
                display: "flex",
                height: "100vh",
                width: "100vw",
                justifyContent: "center",
                alignItems: "center",
              }}>
              <CircularProgress />
            </div>
          )}
        </div>
      ) : errorMsg === "Not Authorized to view this Page!!" ? (
        <NotAuthorized />
      ) : (
        <Alert severity='error'>{errorMsg}</Alert>
      )}
    </div>
  );
};

export default Ops;

function cleanup(arr) {
  const leanArr = [];

  for (let row of arr) {
    if (row._source) {
      leanArr.push(row._source);
    }
  }
  return leanArr;
}

function mapper(leanFilterData) {
  const logMap = {};
  const typeMap = {};

  for (const row of leanFilterData) {
    if (row.log) {
      if (logMap[row.log]) {
        logMap[row.log] += 1;
      } else {
        logMap[row.log] = 1;
      }
    }
    if (row.type) {
      if (typeMap[row.type]) {
        typeMap[row.type] += 1;
      } else {
        typeMap[row.type] = 1;
      }
    }
  }

  // console.log("logMap: ", logMap);
  // console.log("typeMap: ", typeMap);
}

function getSimData(
  deviceId,
  fromDate,
  toDate,
  setSimData,
  setError,
  setErrorMsg
) {
  const body = {
    device: deviceId,
    start: Math.floor(new Date(fromDate).getTime() / 1000),
  };

  const requestOptions = {
    method: "POST",
    headers: {
      // 'Authorization': 'Basic ' + btoa('abhirup' + ':' + 'abhirup'),
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(body),
    redirect: "follow",
  };
  console.log("request options device simulator, ", requestOptions);

  API.post(
    "/api/v1/devicesimulator/deviceid/find",
    body
    // requestOptions
  )
    .then((response) => response.data)
    .then((res) => {
      // console.log("simulator, res: ", res);
      setSimData(res.hits.hits);
    })
    .catch((error) => {
      try {
        setError(true);
        setErrorMsg(error?.response?.data?.Action);
      } catch {
        setError(true);
        setErrorMsg("Unknown error occured");
      }
    });
}

function getFilterData(
  deviceId,
  fromDate,
  toDate,
  setFilterData,
  setError,
  setErrorMsg
) {
  const body = {
    device: deviceId,
    start: Math.floor(new Date(fromDate).getTime() / 1000),
  };

  const requestOptions = {
    method: "POST",
    headers: {
      // 'Authorization': 'Basic ' + btoa('abhirup' + ':' + 'abhirup'),
      "Content-Type": "application/json;charset=utf-8",
    },
    body: JSON.stringify(body),
    redirect: "follow",
  };
  console.log("request options fluent, ", requestOptions);
  API.post("/api/v1/fluentd/deviceid/find", body)
    .then((response) => response.data)
    .then((res) => {
      console.log("fluent res: ", res);
      setFilterData(res.hits.hits);
    })
    .catch((error) => {
      try {
        setError(true);
        setErrorMsg(error?.response?.data?.Action);
      } catch {
        setError(true);
        setErrorMsg("Unknown error occured");
      }
    });
}

function getData(tempValue) {
  let {
    deviceId,
    fromDate,
    toDate,
    setSimData,
    setFilterData,
    setError,
    setErrorMsg,
  } = tempValue;
  getSimData(deviceId, fromDate, toDate, setSimData, setError, setErrorMsg);
  getFilterData(
    deviceId,
    fromDate,
    toDate,
    setFilterData,
    setError,
    setErrorMsg
  );
}
