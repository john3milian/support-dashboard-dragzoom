import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import moment from "moment";
import DateFnsUtils from "@date-io/date-fns";
// import MomentUtils from "@date-io/moment";
import "../css/Licensing.css";

import { Button, Select, TextField, Typography } from "@material-ui/core";

const columns = [
  { id: "id", label: "License ID", minWidth: 100, align: "center" },
  { id: "created_by", label: "Created By", minWidth: 100, align: "center" },
  {
    id: "created_at",
    label: "Created At",
    minWidth: 120,
    align: "center",
    // format: (value) => value.toLocaleString("en-US"),
    format: (value) => moment(value).local().format("Do-MMM h:mm a"),
  },
  {
    id: "number",
    label: "No Of Devices",
    minWidth: 120,
    align: "center",
    // format: (value) => value.toLocaleString("en-US"),
  },
  {
    id: "duration",
    label: "Duration",
    minWidth: 130,
    align: "center",
    // format: (value) => value.toFixed(2),
  },
  {
    id: "type",
    label: "Type",
    minWidth: 120,
    align: "center",
    // format: (value) => value.toFixed(2),
  },
  {
    id: "status",
    label: "Status",
    minWidth: 140,
    align: "center",
    // format: (value) => value.toFixed(2),
  },
  {
    id: "redeemed_at",
    label: "Redeemed At",
    minWidth: 130,
    align: "center",
    // format: (value) => value.toFixed(2),
  },
  {
    id: "redeemed_org",
    label: "Redeemed Org",
    minWidth: 130,
    align: "center",
    // format: (value) => value.toFixed(2),
  },
  {
    id: "remarks",
    label: "Remarks",
    minWidth: 150,
    align: "center",
    // format: (value) => value.toFixed(2),
  },
];

function createData(
  id,
  created_by,
  created_at,
  number,
  duration,
  type,
  status,
  redeemed_at,
  redeemed_org,
  remarks
) {
  // const density = population / size;
  return {
    id,
    created_by,
    created_at,
    number,
    duration,
    type,
    status,
    redeemed_at,
    redeemed_org,
    remarks,
  };
}

const rows = [
  createData(
    "abcd123456",
    "Kriti@dozee.io",
    Date.now(),
    34,
    "12 months",
    "Single User",
    "Not Redeemed",
    "---",
    "---",
    "n/a"
  ),
  createData(
    "abcd123456",
    "Kriti@dozee.io",
    Date.now(),
    42,
    "1 year",
    "Single User",
    "Not Redeemed",
    "---",
    "---",
    "n/a"
  ),
];

const useStyles = makeStyles({
  root: {
    width: "100%",
  },
  container: {
    maxHeight: 440,
  },
});

export default function Licensing() {
  const classes = useStyles();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [searchText, setSearchText] = useState("");
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <>
      <Typography color='textPrimary' variant='h4'>
        Licensing
      </Typography>
      <div className='w-100 h-25 mb-3 d-flex align-items-end justify-content-between'>
        <div className='w-25'>
          <Typography>Filter by Organization / Redeemed by</Typography>
          <TextField
            placeholder='Search'
            variant='outlined'
            size='small'
            margin='normal'
            fullWidth
          />
        </div>
        <div className='ml-3' style={{ minWidth: "40%" }}>
          <div className='filter-box'>
            <div className='filter-box'>
              <Typography color='primary' variant='body1' className='mr-1'>
                From Date{" "}
              </Typography>
              <TextField
                id='date'
                variant='outlined'
                margin='normal'
                size='small'
                type='date'
                // defaultValue='2017-05-24'
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
            <div className='filter-box'>
              <Typography color='primary' variant='body1' className='mr-1'>
                To Date{" "}
              </Typography>
              <TextField
                id='date'
                variant='outlined'
                margin='normal'
                size='small'
                type='date'
                // defaultValue='2017-05-24'
                className={classes.textField}
                InputLabelProps={{
                  shrink: true,
                }}
              />
            </div>
          </div>
          <div className='filter-box mt-2' style={{ width: "100%" }}>
            <div className='filter-box box'>
              <Typography
                color='primary'
                variant='body1'
                style={{ minWidth: "31%" }}>
                Search by{" "}
              </Typography>
              <Select
                native
                fullWidth
                margin='none'
                inputProps={{
                  name: "age",
                  id: "age-native-simple",
                }}>
                <option value='created'>Created At</option>
                <option value='redeemed'>Redeemed At</option>
              </Select>
            </div>
            <Button
              fullWidth
              className='ml-3'
              color='primary'
              variant='contained'
              style={{ outline: 0 }}>
              Search
            </Button>
          </div>
        </div>
      </div>
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label='sticky table'>
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}>
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow
                      hover
                      role='checkbox'
                      tabIndex={-1}
                      key={row.code}>
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.format && typeof value === "number"
                              ? column.format(value)
                              : value}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component='div'
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
