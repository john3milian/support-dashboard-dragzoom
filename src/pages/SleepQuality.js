import React, { useState, useEffect } from "react";
import TableGenerator from "../components/TableGenerator";
import QualityParamsGraphGenerator from "../components/QualityParamsGraphGenerator";
import { COLUMNS } from "../helpers/sleepQualityLogColumns";
import { Redirect, useHistory, useParams } from "react-router-dom";
// import Loader from "./Loader";
import "../css/SleepQuality.css";
import { Button, CircularProgress, TextField } from "@material-ui/core";
import API from "../api";

const SleepQuality = () => {
  const [data, setData] = useState({});
  const [arrangedData, setArrangedData] = useState([]);
  const [loaded, setLoaded] = useState(false);
  const [inputVal, setInputVal] = useState("");
  const [currentSleepId, setCurrentSleepId] = useState("");

  const [responded, setResponded] = useState(false);
  const [dataAvailable, setDataAvailable] = useState(false);

  const history = useHistory();
  function clearData() {
    setData({});
    setArrangedData([]);
    setCurrentSleepId("");
  }

  function setStatus(responseStatus, dataAvailableStatus) {
    setResponded(responseStatus);
    setDataAvailable(dataAvailableStatus);
  }

  function validState() {
    return responded && dataAvailable;
  }

  let { sleepId } = useParams();

  useEffect(() => {
    if (!sleepId) {
      return setStatus(true, false);
    }

    setCurrentSleepId(sleepId);
    return API.get(`/api/data/sleep/quality?sleepid=${sleepId}`)
      .then((res) => res.data)
      .then((res) => {
        console.log("res (transactions): ", res);
        if (!Object.keys(res.uploads).length) {
          console.log("No data for submitted sleep ID");
          throw new Error("No data for submitted sleep ID");
        } else {
          setData(res);
        }
      })
      .catch((err) => {
        // alert(err);
        console.log("error received: ", err);
        setStatus(true, false);
      });
  }, []);

  useEffect(() => {
    if (data.uploads) {
      console.log("data length: ", Object.keys(data.uploads).length);
      console.log(Object.keys(data.uploads)[0]);

      const arr = [];

      for (let key of Object.keys(data.uploads)) {
        const obj = {
          transactionId: key,
          ...data.uploads[key],
        };
        arr.push(obj);
      }

      arr.sort((a, b) => (a.uploadedAt > b.uploadedAt ? 1 : -1));
      console.log("sorted arr: ", arr);
      setArrangedData(arr);
    }
  }, [data]);

  useEffect(() => {
    if (arrangedData.length) {
      setLoaded(true);
    }
  }, [arrangedData]);

  const handleSubmit = (e) => {
    // history.push(`/#/sleep-quality/${inputVal}`);
    e.preventDefault();

    if (inputVal) {
      console.log("Making a call ... ");
      clearData();
      setLoaded(false);
      setCurrentSleepId(inputVal);
      getSleepData(inputVal, setData, setStatus, setLoaded);
    }
    setInputVal("");
  };

  return (
    <>
      {localStorage.getItem("authToken") ? (
        <div>
          <div className='search-input-div'>
            <span className='sleep-quality-header-outer-span'>
              Sleep{" "}
              <span className='sleep-quality-header-span'>
                {currentSleepId ? currentSleepId : "n/a"}
              </span>
            </span>

            <form
              onSubmit={handleSubmit}
              className='d-flex'
              style={{ width: "35%" }}>
              <TextField
                variant='outlined'
                size='small'
                type='text'
                className='outline-0 mr-2'
                placeholder='ENTER SLEEP ID'
                value={inputVal}
                fullWidth
                onChange={(e) => setInputVal(e.target.value)}
              />
              {/* <input
                className='sleep-quality-search-input'
                type='text'
                placeholder='ENTER SLEEP ID'
                value={inputVal}
                onChange={(e) => setInputVal(e.target.value)}
              /> */}
              <Button color='primary' variant='contained' type='submit'>
                Go
              </Button>
              {/* <button className='sleep-quality-go-btn'>Go</button> */}
            </form>
          </div>

          <div className='quality-params-div'>
            <div className='quality-params-header'>Quality parameters</div>
            <div className='sleep-quality-chart'>
              {loaded ? (
                <QualityParamsGraphGenerator graphData={arrangedData} />
              ) : responded && !dataAvailable ? (
                "DATA UNAVAILABLE"
              ) : (
                <CircularProgress />
              )}
            </div>
          </div>

          <div
            className='sleep-quality-log-div'
            style={{ alignItems: loaded ? "flex-start" : "center" }}>
            {loaded ? (
              <TableGenerator
                users={arrangedData}
                COLUMNS={COLUMNS}
                defaultPageSize={10}
                search={false}
              />
            ) : responded && !dataAvailable ? (
              "DATA UNAVAILABLE"
            ) : (
              <CircularProgress />
            )}
          </div>
        </div>
      ) : (
        <Redirect to='/' />
      )}
    </>
  );
};

export default SleepQuality;

function getSleepData(sleepId, setData, setStatus, setLoaded) {
  API.get(
    `/api/data/sleep/quality?sleepid=${sleepId}`
    // "https://datastore.dozee.me/api/data/log/" + sleepId + "-transactions/"
  )
    .then((res) => {
      if (res.status >= 500) {
        throw new Error("Invalid ID");
      }
      return res.data;
    })
    .then((res) => {
      console.log("Got data ... ", res);
      setData(res);
      setLoaded(true);
    })
    .catch((err) => {
      console.log(err);
      setStatus(true, false);
      setLoaded(false);
    });
}
