import {
  LinearProgress,
  CircularProgress,
  Divider,
  Typography,
  Button,
  Tooltip,
  ClickAwayListener,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import DonutGenerator from "../components/DonutGenerator";
import MixedGraphGenerator from "../components/MixedGraphGenerator";
import OptionsDialog from "../components/OptionsDialog";
import SleepAnalysisTable from "../components/SleepAnalysisTable";
import SleepDataTable from "../components/SleepDataTable";
import "../css/SleepDataPage.css";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import API from "../api";
import { Alert } from "@material-ui/lab";
import NotAuthorized from "../components/central/NotAuthorized";
import moment from "moment";

function SleepDataPage() {
  const [user, setUser] = useState("");
  const [data, setData] = useState({});
  const [responded, setResponded] = useState(false);
  const [dataAvailable, setDataAvailable] = useState(false);
  const [sleepIdList, setSleepIdList] = useState([]);
  const [indexSelected, setIndexSelected] = useState(0);
  const [allData, setAllData] = useState([]);
  const [showOptions, setShowOptions] = useState(false);

  const [showToolTip1, setShowToolTip1] = useState(false);
  const [showToolTip2, setShowToolTip2] = useState(false);
  const [showToolTip3, setShowToolTip3] = useState(false);
  const [loaded, setLoaded] = useState(false);

  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  let { userId } = useParams();
  // const userId = "0583b27a-158a-4150-8798-f73d78a65b6b";
  console.log("userId: ", userId);

  const copyToClipboard = (text) => {
    if (!navigator.clipboard) {
      // fallbackCopyTextToClipboard(text);
      return;
    }

    navigator.clipboard.writeText(text).then(
      function () {
        console.log("Async: Copying to clipboard was successful!");
      },
      function (err) {
        console.error("Async: Could not copy text: ", err);
      }
    );
  };

  function setStatus(responseStatus, dataAvailableStatus) {
    setResponded(responseStatus);
    setDataAvailable(dataAvailableStatus);
  }

  function validState() {
    return responded && dataAvailable;
  }

  useEffect(() => {
    console.log("responded: ", responded);
    console.log("data available: ", dataAvailable);
    console.log("validState check: ", validState());
  }, [responded, dataAvailable]);

  useEffect(() => {
    const days = 14;
    const timeFrame = parseInt(1000 * 60 * 60 * 24 * days);
    const from = Math.round(new Date(Date.now() - timeFrame).getTime() / 1000);
    const to = Math.round(new Date(Date.now()).getTime() / 1000);

    API.post(
      `/api/v1/sleep/list?userId=${userId}&fromTime=${from}&toTime=${to}`,
      {
        from,
        fields: ["sleepId", "WakeupTime", "SleepTime", "OnBedTime"],
      }
    )
      .catch((error) => {
        setError(true);
        setErrorMsg(error?.response?.data?.Action);
      })
      .then((res) => res.data)
      .then((res) => {
        if (!Array.isArray(res)) {
          throw new Error("No data");
        }
        if (res.length === 0) {
          throw new Error(`No data between ${moment(from)} and ${moment(to)}`);
        }
        const temp_res = [];
        res.map(({ SleepId, WakeupTime, SleepTime, Properties }) =>
          temp_res.push({
            sleepId: SleepId,
            WakeupTime: WakeupTime,
            SleepTime: SleepTime,
            OnBedTime: Properties["OnBedTime"],
          })
        );

        console.log(res);
        res = temp_res;
        res.sort((a, b) => (a.OnBedTime < b.OnBedTime ? 1 : -1));
        const slicedList = res.map(({ sleepId }) => sleepId).slice(0, 5);
        console.log(slicedList);
        setSleepIdList(slicedList);

        return slicedList;
      })
      .then((arr) =>
        Promise.all([
          ...arr.map((id) =>
            API.get("/api/data/sleep?sleepid=" + id)
              .then((res) => res.data)
              .catch((err) => err)
          ),
        ])
          .catch((error) => {
            setError(true);
            setErrorMsg(error?.response?.data?.Action);
          })
          .then(async (res) => {
            const myres = await API.get(
              `/api/v1/sleeprecords?userId=${userId}&from=${from}&to=${to}`
            ).catch((error) => {
              setError(true);
              setErrorMsg(error?.response?.data?.Action);
            });
            const resBody = await myres.data;
            console.log(resBody);
            setStatus(true, true);
            setAllData(res);
          })
      )
      .catch((err) => {
        console.log(err);
        setStatus(true, false);
      });
  }, []);

  useEffect(() => {
    if (allData.length) {
      console.log("allData: ", allData);
    }
  }, [allData]);

  useEffect(() => {
    if (Object.keys(data).length) {
      saveToDisk(data);
      setLoaded(true);
      console.log(data);
      console.log("durations: ", data.durations);
    } else {
      console.log("Nothing as yet ... ");
    }
  }, [data]);

  const notMySleep = () => {
    console.log("not my sleep clicked");
  };

  function createButton(arr) {
    console.log("THIS ARE THE DATESSSSSSSS", arr);
    return arr.map((item, index) => {
      // console.log(item.bedTime);
      // const time = parseInt(item.bedTime) * 1000;
      const time = parseInt(item.wakeupTime) * 1000;
      // const key=time;
      const label = new Date(time)
        .toString()
        .split(" ")
        .slice(1, 3)
        .reverse()
        .join("-");
      return (
        <Button
          key={time}
          onClick={() => {
            console.log(index);
            setIndexSelected(index);
          }}
          color='primary'
          style={{ outline: 0 }}
          variant={index === indexSelected ? "contained" : "outlined"}
          className='ml-1'>
          {label}
        </Button>
      );
    });
  }
  return (
    <div className='sleep-data-container'>
      {error ? (
        errorMsg === "Not Authorized to view this Page!!" ? (
          <NotAuthorized />
        ) : (
          <Alert severity='error'>{errorMsg}</Alert>
        )
      ) : (
        <>
          <div className='user-details'>
            <div className='user-details__top'>
              <Typography color='primary' variant='subtitle2'>
                User : {userId}
                <ClickAwayListener onClickAway={() => setShowToolTip1(false)}>
                  <Tooltip
                    arrow
                    title='Copied'
                    placement='top'
                    onClose={() => setShowToolTip1(false)}
                    open={showToolTip1}>
                    <FileCopyIcon
                      fontSize='small'
                      style={{
                        fontSize: "1rem",
                        marginLeft: "5px",
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        setShowToolTip1(true);
                        copyToClipboard(userId ? userId : "n/a");
                      }}
                    />
                  </Tooltip>
                </ClickAwayListener>
              </Typography>
              <Typography
                color='primary'
                variant='subtitle2'
                className='det_box'>
                Sleep :{" "}
                {sleepIdList.length ? (
                  <Link
                    to={`/sleep-quality/${sleepIdList[indexSelected]}`}
                    target='_blank'
                    className='sleep-link'>
                    {sleepIdList[indexSelected]}
                  </Link>
                ) : (
                  "n/a"
                )}
                <ClickAwayListener onClickAway={() => setShowToolTip2(false)}>
                  <Tooltip
                    arrow
                    title='Copied'
                    placement='top'
                    onClose={() => setShowToolTip2(false)}
                    open={showToolTip2}>
                    <FileCopyIcon
                      fontSize='small'
                      style={{
                        fontSize: "1rem",
                        marginLeft: "5px",
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        setShowToolTip2(true);
                        copyToClipboard(
                          sleepIdList.length
                            ? sleepIdList[indexSelected]
                            : "n/a"
                        );
                      }}
                    />
                  </Tooltip>
                </ClickAwayListener>
              </Typography>
              <Typography color='primary' variant='subtitle2'>
                Device Id :{" "}
                {allData.length ? allData[indexSelected].deviceId : "n/a"}
                <ClickAwayListener onClickAway={() => setShowToolTip3(false)}>
                  <Tooltip
                    arrow
                    title='Copied'
                    placement='top'
                    onClose={() => setShowToolTip3(false)}
                    open={showToolTip3}>
                    <FileCopyIcon
                      fontSize='small'
                      style={{
                        fontSize: "1rem",
                        marginLeft: "5px",
                        cursor: "pointer",
                      }}
                      onClick={() => {
                        setShowToolTip3(true);
                        copyToClipboard(
                          allData.length
                            ? allData[indexSelected].deviceId
                            : "n/a"
                        );
                      }}
                    />
                  </Tooltip>
                </ClickAwayListener>
              </Typography>
              <Typography color='primary'>Not my sleep?</Typography>
            </div>
            <div className='user-details__bottom mt-4'>
              <div className='user-details__bottom--buttons'>
                {allData.length ? createButton(allData) : ""}
              </div>
              <div style={{ width: "15%" }}>
                <Button
                  onClick={() => setShowOptions(!showOptions)}
                  color='primary'
                  variant='contained'
                  className='mr-3'
                  fullWidth>
                  options
                </Button>
              </div>
              <OptionsDialog
                display={showOptions}
                toggleDisplay={setShowOptions}
                userId={userId}
                setAllData={setAllData}
                setStatus={setStatus}
                setSleepIdList={setSleepIdList}
                setIndexSelected={setIndexSelected}
              />
            </div>
          </div>

          <div className='sleep-data-donut'>
            <div className='sleep-data-box'>
              <Typography color='primary' variant='h6'>
                Sleep Data
              </Typography>
              <Divider />
              <div className='sleep-data-content'>
                {allData.length ? (
                  <SleepDataTable data={allData[indexSelected]} />
                ) : responded && !dataAvailable ? (
                  "DATA UNAVAILABLE"
                ) : (
                  <LinearProgress />
                )}
              </div>
            </div>
            <div className='sleep-score-box'>
              <Typography color='primary' variant='h6'>
                Sleep Score
              </Typography>
              <Divider />
              <div className='sleep-score-content'>
                {allData.length ? (
                  <DonutGenerator
                    durations={allData[indexSelected].durations}
                    sleepScore={allData[indexSelected].sleepScore}
                  />
                ) : responded && !dataAvailable ? (
                  "DATA UNAVAILABLE"
                ) : (
                  <CircularProgress />
                )}
              </div>
            </div>
            <div className='sleep-analysis-box'>
              <Typography color='primary' variant='h6'>
                Sleep Analysis
              </Typography>
              <Divider />
              <div className='sleep-score-content'>
                {allData.length ? (
                  <SleepAnalysisTable data={allData[indexSelected].points} />
                ) : responded && !dataAvailable ? (
                  "DATA UNAVAILABLE"
                ) : (
                  <LinearProgress />
                )}
              </div>
            </div>
          </div>

          <div className='sleep-mixed-graph'>
            {allData.length ? (
              <>
                <p>AHI </p>
                <MixedGraphGenerator graphData={allData[indexSelected]} />
              </>
            ) : responded && !dataAvailable ? (
              "DATA UNAVAILABLE"
            ) : (
              <LinearProgress />
            )}
          </div>
        </>
      )}
    </div>
  );
}

export default SleepDataPage;

export function saveToDisk(data, key = "graphData") {
  localStorage.setItem(key, JSON.stringify(data));
}
