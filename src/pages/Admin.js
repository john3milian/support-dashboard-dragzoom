import React, { useState, useEffect } from 'react'
import {getAllUsers} from "../helpers/adminHelper"
import AdminModal from '../components/admin/AdminModal' 
import AdminTable from '../components/admin/AdminTable'
import AdminAlert from '../components/admin/AdminAlert'

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';

const useStyles = makeStyles((theme) => ({
  table: {
    maxWidth: 1080,
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 240,
  },
}));
  

const Admin = () => {

  const classes = useStyles();

  const [users, setUsers] = useState([])
  const [open, setOpen] = useState(false);
  const [Sort, setSort] = useState('All');
  const [Alert, setAlert] = useState(false)
  const [AlertMessage, setAlertMessage] = useState('')
  const [type, setAlertType] = useState('error')
  const [search, setSearch] = useState('')
  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event) => {
    setSort(event.target.value);
  };

  function hideAlert() {
    setAlert(false)
  }

  const searchUser = (event) => {
    setSearch(event.target.value)
  }

  const changeAlert = () => {
    setTimeout(hideAlert, 6000)
  }

  useEffect(() => {
      getAllUsers(setUsers, setError, setErrorMsg)
    }, []);

  return (
    <div className = {classes.root}>
      {Alert ? (<AdminAlert type = {type} message = {AlertMessage} /> ) : null}
      {error ? (<AdminAlert type = 'serverError' message  = {errorMsg} />) : 
      <div>
        <AdminModal 
        open = {open} 
        handleClose = {handleClose} 
        getAllUsers = {getAllUsers} 
        setUsers = {setUsers} 
        setAlert = {setAlert}
        setAlertMessage = {setAlertMessage}
        changeAlert = {changeAlert}
        setAlertType = {setAlertType}
        users = {users}/>
      <Grid container spacing = {3}>
        <Grid item xs={12} md = {3}>
          <FormControl className={classes.formControl}>
            <InputLabel shrink id="demo-simple-select-placeholder-label-label">
              Sort By
            </InputLabel>
            <Select
              value={Sort}
              onChange={handleChange}
              displayEmpty
              className={classes.selectEmpty}
              inputProps={{ 'aria-label': 'Without label' }}
            >
              <MenuItem value="All">None</MenuItem>
              <MenuItem value="validation">validation</MenuItem>
              <MenuItem value="Support Team">Support Team</MenuItem>
              <MenuItem value="Data Science">Data Science</MenuItem>
              <MenuItem value="Operations - RCSE">Operations-RCSE</MenuItem>
              <MenuItem value="Operations - Field">Operations-Field</MenuItem>
              <MenuItem value="Operations - Alerts">Operations-Alerts</MenuItem>
              <MenuItem value="Hardware Team">Hardware Team</MenuItem>
              <MenuItem value="Software Development">Software Development</MenuItem>
              <MenuItem value="Software Admin">Software Admin</MenuItem>
              <MenuItem value="Management">Management</MenuItem>
              <MenuItem value="Product">Product</MenuItem>
            </Select>
            <FormHelperText>View Users by Access Level</FormHelperText>
          </FormControl>
        </Grid>
        <Grid item xs={12} md = {3}>
          <SearchIcon style = {{marginTop: 31}}/>
          <TextField style = {{marginTop: 9}}  id="standard-basic" label="Search" 
            onChange = {(event) => searchUser(event)}/> 
        </Grid>
        <Grid item xs={12} md = {3}>
          <Button style = {{marginTop: 21}} variant="contained" color="Primary" onClick={handleOpen}>
            Add User
          </Button>
        </Grid>
        <Grid item xs={10}>
          <AdminTable users = {users} sortOrder = {Sort} 
          getAllUsers = {getAllUsers} 
          setUsers = {setUsers} 
          setAlert = {setAlert}
          changeAlert = {changeAlert}
          setAlertType = {setAlertType}
          setAlertMessage = {setAlertMessage}
          search = {search}/>
        </Grid>
      </Grid>
      </div>
      }
    </div>
  )
}

export default Admin
