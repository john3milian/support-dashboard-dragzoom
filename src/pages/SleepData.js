import React, { useState, useEffect } from "react";
import { Link, Redirect, useParams } from "react-router-dom";
import "../css/SleepsData.css";
import DonutGenerator from "../components/DonutGenerator";
import MixedGraphGenerator from "../components/MixedGraphGenerator";
// import Loader from "./Loader";
// import SleepDataTable from "../components/SleepDataTable";
import SleepDataTable from "../components/SleepDataTable";
import SleepAnalysisTable from "../components/SleepAnalysisTable";
import OptionsDialog from "../components/OptionsDialog";
import { CircularProgress } from "@material-ui/core";

const SleepsData = (props) => {
  const [user, setUser] = useState("");
  const [data, setData] = useState({});
  const [responded, setResponded] = useState(false);
  const [dataAvailable, setDataAvailable] = useState(false);
  const [sleepIdList, setSleepIdList] = useState([]);
  const [indexSelected, setIndexSelected] = useState(0);
  const [allData, setAllData] = useState([]);
  const [showOptions, setShowOptions] = useState(false);

  const [loaded, setLoaded] = useState(false);
  //   let { userId } = useParams();
  const userId = "0583b27a-158a-4150-8798-f73d78a65b6b";
  console.log("userId: ", userId);

  function setStatus(responseStatus, dataAvailableStatus) {
    setResponded(responseStatus);
    setDataAvailable(dataAvailableStatus);
  }

  function validState() {
    return responded && dataAvailable;
  }

  useEffect(() => {
    console.log("responded: ", responded);
    console.log("data available: ", dataAvailable);
    console.log("validState check: ", validState());
  }, [responded, dataAvailable]);

  useEffect(() => {
    if (!userId) {
      return setStatus(true, false);
    }

    fetch("http://139.59.20.84:2380/api/data/sleep?sleepid=" + userId)
      .then((res) => {
        if (res.status >= 500) {
          throw new Error("Invalid ID");
        }
        return res.json();
      })
      .then((res) => {
        // console.log('user res: ', res);
        setUser(res.email);
      })
      .catch((err) => {
        console.log(err);
        setStatus(true, false);
      });
  }, []);

  useEffect(() => {
    const days = 14;
    const timeFrame = parseInt(1000 * 60 * 60 * 24 * days);
    const from = Math.round(new Date(Date.now() - timeFrame).getTime() / 1000);

    fetch(
      // "https://influx.dozee.me/api/v1/user/" + userId + "/",
      `http://139.59.20.84:2380/api/v1/sleep/list?userId=${userId}&fromTime=${from}`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json;charset=utf-8",
        },
        body: JSON.stringify({
          from,
          fields: ["sleepId", "WakeupTime", "SleepTime", "OnBedTime"],
        }),
      }
    )
      .then((res) => res.json())
      .then((res) => {
        if (!Array.isArray(res)) {
          throw new Error("No data");
        }
        const temp_res = [];
        res.map(({ SleepId, WakeupTime, SleepTime, Properties }) =>
          temp_res.push({
            sleepId: SleepId,
            WakeupTime: WakeupTime,
            SleepTime: SleepTime,
            OnBedTime: Properties["OnBedTime"],
          })
        );
        console.log(res);
        res = temp_res;
        res.sort((a, b) => (a.OnBedTime < b.OnBedTime ? 1 : -1));
        const slicedList = res.map(({ sleepId }) => sleepId).slice(0, 5);
        setSleepIdList(slicedList);

        return slicedList;
      })
      .then((arr) =>
        Promise.all([
          ...arr.map((id) =>
            fetch("http://139.59.20.84:2380/api/data/sleep?sleepid=" + id)
              .then((res) => res.json())
              .catch((err) => err)
          ),
        ]).then((res) => {
          setStatus(true, true);
          setAllData(res);
        })
      )
      .catch((err) => {
        console.log(err);
        setStatus(true, false);
      });
  }, []);

  useEffect(() => {
    if (allData.length) {
      console.log("allData: ", allData);
    }
  }, [allData]);

  useEffect(() => {
    if (Object.keys(data).length) {
      saveToDisk(data);
      setLoaded(true);
      console.log(data);
      console.log("durations: ", data.durations);
    } else {
      console.log("Nothing as yet ... ");
    }
  }, [data]);

  const notMySleep = () => {
    console.log("not my sleep clicked");
  };

  function createButton(arr) {
    return arr.map((item, index) => {
      // console.log(item.bedTime);
      // const time = parseInt(item.bedTime) * 1000;
      const time = parseInt(item.wakeupTime) * 1000;
      // const key=time;
      const label = new Date(time)
        .toString()
        .split(" ")
        .slice(1, 3)
        .reverse()
        .join("-");
      return (
        <button
          key={time}
          onClick={() => setIndexSelected(index)}
          className={
            index === indexSelected ? "date-btn-selected" : "date-btn"
          }>
          {label}
        </button>
      );
    });
  }

  return (
    <>
      {localStorage.getItem("adminPage-AccessToken") ? (
        <div className='main-sleep-container'>
          <div className='user-info-container'>
            <span className='sleep-data-header-outer-span'>
              User{" "}
              <span className='sleep-data-header-span'>
                {user ? user : "n/a"}
              </span>
            </span>
            <span className='sleep-data-header-outer-span'>
              Sleep{" "}
              <span className='sleep-data-header-span'>
                {sleepIdList.length ? (
                  <Link
                    to={`/sleep-quality/${sleepIdList[indexSelected]}`}
                    target='_blank'>
                    {sleepIdList[indexSelected]}
                  </Link>
                ) : (
                  "n/a"
                )}
              </span>
            </span>
            <span className='sleep-data-header-outer-span'>
              Device{" "}
              <span className='sleep-data-header-span'>
                {allData.length ? allData[indexSelected].deviceId : "n/a"}
              </span>
            </span>

            <span className='not-my-sleep-btn-container'>
              <button onClick={notMySleep} className='not-my-sleep-btn'>
                Not my sleep
              </button>
            </span>
          </div>

          <div className='sleep-main-container'>
            <div className='sleeps-range-div'>
              <span>{allData.length ? createButton(allData) : ""}</span>

              <span className='options-span'>
                <button
                  onClick={() => setShowOptions(!showOptions)}
                  className='options-btn'>
                  options
                </button>
              </span>
            </div>

            <OptionsDialog
              display={showOptions}
              toggleDisplay={setShowOptions}
              userId={userId}
              setAllData={setAllData}
              setStatus={setStatus}
              setSleepIdList={setSleepIdList}
              setIndexSelected={setIndexSelected}
            />
            <div className='sleep-charts-container'>
              <div className='sleep-data'>
                <div className='sleep-data-header'>Data</div>
                <div className='sleep-data-table-container'>
                  {allData.length ? (
                    <SleepDataTable data={allData[indexSelected]} />
                  ) : responded && !dataAvailable ? (
                    "DATA UNAVAILABLE"
                  ) : (
                    <CircularProgress />
                  )}
                </div>
              </div>
              <div className='sleep-score'>
                <div className='sleep-score-header'>Score</div>
                <div className='sleep-donut'>
                  {allData.length ? (
                    <DonutGenerator
                      durations={allData[indexSelected].durations}
                      sleepScore={allData[indexSelected].sleepScore}
                    />
                  ) : responded && !dataAvailable ? (
                    "DATA UNAVAILABLE"
                  ) : (
                    <CircularProgress />
                  )}
                </div>
              </div>
              <div className='sleep-analysis'>
                <div className='sleep-analysis-header'>Analysis</div>
                <div className='sleep-analysis-table-container'>
                  {allData.length ? (
                    <SleepAnalysisTable data={allData[indexSelected].points} />
                  ) : responded && !dataAvailable ? (
                    "DATA UNAVAILABLE"
                  ) : (
                    <CircularProgress />
                  )}
                </div>
              </div>
            </div>
          </div>

          <div className='ahi-graph-container'>
            <div className='chart'>
              {allData.length ? (
                <>
                  <p>AHI </p>
                  <MixedGraphGenerator graphData={allData[indexSelected]} />
                </>
              ) : responded && !dataAvailable ? (
                "DATA UNAVAILABLE"
              ) : (
                <CircularProgress />
              )}
            </div>
          </div>
        </div>
      ) : (
        <Redirect to='/' />
      )}
    </>
  );
};

export default SleepsData;

export function saveToDisk(data, key = "graphData") {
  localStorage.setItem(key, JSON.stringify(data));
}
