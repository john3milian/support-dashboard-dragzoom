import { Alert } from "@material-ui/lab";
import React from "react";

function Home() {
  return (
    <div className='d-flex  align-items-center flex-column '>
      <h3>Welcome to Support Dashboard</h3>
      <Alert severity='info' className='w-100'>
        For issues, please contact <u>+91 8800125518</u>
      </Alert>
    </div>
  );
}

export default Home;
