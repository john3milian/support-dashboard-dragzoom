import React, { useEffect, useState } from "react";
import { Typography } from "@material-ui/core";
import moment from "moment";
import Moment from "react-moment";
import API from "../api";
import { Alert } from "@material-ui/lab";
import NotAuthorized from "../components/central/NotAuthorized";
function Stats() {
  const [errorMsg, setErrorMsg] = useState("");
  const [data, setData] = useState({
    _shards: {},
    hits: {
      hits: [
        {
          _source: {
            B2BDAD: 0,
            B2BDADC: 0,
            B2BDAU: 0,
            B2BDAUC: 0,
            B2BMAD: 0,
            B2BMADC: 0,
            B2BMAU: 0,
            B2BMAUC: 0,
            B2BWAD: 0,
            B2BWADC: 0,
            B2BWAU: 0,
            B2BWAUC: 0,
            B2CDAD: 0,
            B2CDADC: 0,
            B2CDAU: 0,
            B2CDAUC: 0,
            B2CMAD: 0,
            B2CMADC: 0,
            B2CMAU: 0,
            B2CMAUC: 0,
            B2CWAD: 0,
            B2CWADC: 0,
            B2CWAU: 0,
            B2CWAUC: 0,
            TotalDAD: 0,
            TotalDADC: 0,
            TotalDAU: 0,
            TotalDAUC: 0,
            TotalMAD: 0,
            TotalMADC: 0,
            TotalMAU: 0,
            TotalMAUC: 0,
            TotalWAD: 0,
            TotalWADC: 0,
            TotalWAU: 0,
            TotalWAUC: 0,
          },
          fields: {
            time: ["2021-05-06T13:41:27.000Z"],
          },
        },
      ],
    },
  });

  const percentChange = (val) => {
    if (val < 0) return " (" + val + "%)";
    else return " (+" + val + "%)";
  };

  const convertTime = (dateTime) => {
    var updatedDateTime = moment(dateTime, "YYYY-M-DDTHH:mm:SS");
    return updatedDateTime;
  };

  const fetchData = async () => {
    API.get(`/api/stats/get`)
      .then((results) => {
        const body = results.data;
        try {
          if (body["hits"]["hits"].length > 0) {
            setData(body);
          }
        } catch {
          console.log("did not workkkkkkkkkkkkkkkkkkk");
        }
        return body;
      })
      .catch((error) => {
        setErrorMsg(error.response.data.Action);
        return;
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className='container-fluid'>
      {errorMsg?.length < 1 ? (
        <>
          {/* heading */}
          <div className='row'>
            <div className='col-8'>
              <Typography color='textPrimary' variant='h4'>
                Stats
              </Typography>
            </div>
            <div className='col-4 d-flex justify-content-end align-items-center'>
              <Moment format='Do MMM YYYY HH:mm:ss'>
                {moment
                  .utc(data.hits.hits[0].fields.time, "YYYY-M-DDTHH:mm:SS")
                  .local()}
              </Moment>
            </div>
          </div>

          {/* first table */}
          <div className='row mt-4'>
            <table class='table table-bordered'>
              <thead>
                <tr>
                  <th scope='col'> </th>
                  <th scope='col'>DAU</th>
                  <th scope='col'>WAU</th>
                  <th scope='col'>MAU</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope='row'>B2C</th>
                  <td>
                    {data.hits.hits[0]._source.B2CDAU}
                    {percentChange(data.hits.hits[0]._source.B2CDAUC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.B2CWAU}
                    {percentChange(data.hits.hits[0]._source.B2CWAUC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.B2CMAU}
                    {percentChange(data.hits.hits[0]._source.B2CMAUC)}
                  </td>
                </tr>
                <tr>
                  <th scope='row'>B2B</th>
                  <td>
                    {data.hits.hits[0]._source.B2BDAU}
                    {percentChange(data.hits.hits[0]._source.B2BDAUC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.B2BWAU}
                    {percentChange(data.hits.hits[0]._source.B2BWAUC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.B2BMAU}
                    {percentChange(data.hits.hits[0]._source.B2BMAUC)}
                  </td>
                </tr>
                <tr>
                  <th scope='row'>Total</th>
                  <td>
                    {data.hits.hits[0]._source.TotalDAU}
                    {percentChange(data.hits.hits[0]._source.TotalDAUC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.TotalWAU}
                    {percentChange(data.hits.hits[0]._source.TotalWAUC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.TotalMAU}
                    {percentChange(data.hits.hits[0]._source.TotalWAUC)}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>

          {/* second table */}
          <div className='row mt-2'>
            <table class='table table-bordered'>
              <thead>
                <tr>
                  <th scope='col'> </th>
                  <th scope='col'>DAD</th>
                  <th scope='col'>WAD</th>
                  <th scope='col'>MAD</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope='row'>B2C</th>
                  <td>
                    {data.hits.hits[0]._source.B2CDAD}
                    {percentChange(data.hits.hits[0]._source.B2CDADC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.B2CWAD}
                    {percentChange(data.hits.hits[0]._source.B2CWADC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.B2CMAD}
                    {percentChange(data.hits.hits[0]._source.B2CMADC)}
                  </td>
                </tr>
                <tr>
                  <th scope='row'>B2B</th>
                  <td>
                    {data.hits.hits[0]._source.B2BDAD}
                    {percentChange(data.hits.hits[0]._source.B2BDADC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.B2BWAD}
                    {percentChange(data.hits.hits[0]._source.B2BWADC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.B2BMAD}
                    {percentChange(data.hits.hits[0]._source.B2BMADC)}
                  </td>
                </tr>
                <tr>
                  <th scope='row'>Total</th>
                  <td>
                    {data.hits.hits[0]._source.TotalDAD}
                    {percentChange(data.hits.hits[0]._source.TotalDADC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.TotalWAD}
                    {percentChange(data.hits.hits[0]._source.TotalWADC)}
                  </td>
                  <td>
                    {data.hits.hits[0]._source.TotalMAD}
                    {percentChange(data.hits.hits[0]._source.TotalMADC)}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </>
      ) : errorMsg === "Not Authorized to view this Page!!" ? (
        <NotAuthorized />
      ) : (
        <Alert severity='error'>{errorMsg}</Alert>
      )}
    </div>
  );
}

export default Stats;
