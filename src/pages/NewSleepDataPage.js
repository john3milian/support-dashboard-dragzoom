import {
  LinearProgress,
  CircularProgress,
  Divider,
  Typography,
  Button,
  Tooltip,
  ClickAwayListener,
  TextField,
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import DonutGenerator from "../components/sleep/DonutGenerator";
import MixedGraphGenerator from "../components/MixedGraphGenerator";
import OptionsDialog from "../components/OptionsDialog";
import SleepAnalysisTable from "../components/sleep/SleepAnalysisTable";
import SleepDataTable from "../components/sleep/SleepDataTable";
import "../css/SleepDataPage.css";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import { fetchData, createButton } from "../helpers/sleepDataHelper";
import API from "../api";
import DataGraph from "../components/sleep/DataGraph";
import theme from "../theme";
import moment from "moment";
import SearchDate from "../components/sleep/SearchDate";

function NewSleepDataPage() {
  const [user, setUser] = useState("");
  const [data, setData] = useState({});
  const [responded, setResponded] = useState(false);
  const [dataAvailable, setDataAvailable] = useState(false);
  const [sleepIdList, setSleepIdList] = useState([]);
  const [sleepDataList, setSleepDataList] = useState([]);
  const [indexSelected, setIndexSelected] = useState(0);
  const [allData, setAllData] = useState([]);
  const [stagesData, setStagesData] = useState([]);
  const [showOptions, setShowOptions] = useState(false);
  const [showToolTip1, setShowToolTip1] = useState(false);
  const [showToolTip2, setShowToolTip2] = useState(false);
  const [showToolTip3, setShowToolTip3] = useState(false);
  const [loaded, setLoaded] = useState(false);
  const [fromDate, setFromDate] = useState(new Date());
  const [toDate, setToDate] = useState(new Date());

  const [showDates, setShowDates] = useState(false);
  let { userId } = useParams();

  const copyToClipboard = (text) => {
    if (!navigator.clipboard) {
      // fallbackCopyTextToClipboard(text);
      return;
    }

    navigator.clipboard.writeText(text).then(
      function () {
        console.log("Async: Copying to clipboard was successful!");
      },
      function (err) {
        console.error("Async: Could not copy text: ", err);
      }
    );
  };
  function setStatus(responseStatus, dataAvailableStatus) {
    setResponded(responseStatus);
    setDataAvailable(dataAvailableStatus);
  }

  useEffect(() => {
    const days = 14;
    const timeFrame = parseInt(1000 * 60 * 60 * 24 * days);
    const from = Math.round(new Date(Date.now() - timeFrame).getTime() / 1000);
    const to = Math.round(new Date(Date.now()).getTime() / 1000);
    setSleepDataList([]);
    setAllData([]);
    fetchData(
      API,
      userId,
      setSleepDataList,
      setSleepIdList,
      setStatus,
      setAllData,
      setStagesData,
      to,
      from
    );
  }, []);

  const specificDate = () => {
    let from = new Date(moment.utc(fromDate)) / 1000;
    let to = new Date(moment.utc(toDate)) / 1000;
    setResponded(false);
    setDataAvailable(false);
    setSleepDataList([]);
    setAllData([]);
    fetchData(
      API,
      userId,
      setSleepDataList,
      setSleepIdList,
      setStatus,
      setAllData,
      setStagesData,
      to,
      from
    );
  };

  const toggleShowDates = () => {
    setShowDates(!showDates);
  };

  return (
    <div className='sleep-data-container'>
      <div className='user-details'>
        <div className='user-details__top'>
          <Typography color='primary' variant='subtitle2'>
            User : {userId}
            <ClickAwayListener onClickAway={() => setShowToolTip1(false)}>
              <Tooltip
                arrow
                title='Copied'
                placement='top'
                onClose={() => setShowToolTip1(false)}
                open={showToolTip1}>
                <FileCopyIcon
                  fontSize='small'
                  style={{
                    fontSize: "1rem",
                    marginLeft: "5px",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    setShowToolTip1(true);
                    copyToClipboard(userId ? userId : "n/a");
                  }}
                />
              </Tooltip>
            </ClickAwayListener>
          </Typography>
          <Typography color='primary' variant='subtitle2' className='det_box'>
            Sleep :{" "}
            {sleepIdList.length ? (
              <Link
                to={`/sleep-quality/${sleepIdList[indexSelected]}`}
                target='_blank'
                className='sleep-link'>
                {sleepIdList[indexSelected]}
              </Link>
            ) : (
              "n/a"
            )}
            <ClickAwayListener onClickAway={() => setShowToolTip2(false)}>
              <Tooltip
                arrow
                title='Copied'
                placement='top'
                onClose={() => setShowToolTip2(false)}
                open={showToolTip2}>
                <FileCopyIcon
                  fontSize='small'
                  style={{
                    fontSize: "1rem",
                    marginLeft: "5px",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    setShowToolTip2(true);
                    copyToClipboard(
                      sleepIdList.length ? sleepIdList[indexSelected] : "n/a"
                    );
                  }}
                />
              </Tooltip>
            </ClickAwayListener>
          </Typography>
          <Typography color='primary' variant='subtitle2'>
            Device Id :{" "}
            {allData.length ? allData[indexSelected].deviceId : "n/a"}
            <ClickAwayListener onClickAway={() => setShowToolTip3(false)}>
              <Tooltip
                arrow
                title='Copied'
                placement='top'
                onClose={() => setShowToolTip3(false)}
                open={showToolTip3}>
                <FileCopyIcon
                  fontSize='small'
                  style={{
                    fontSize: "1rem",
                    marginLeft: "5px",
                    cursor: "pointer",
                  }}
                  onClick={() => {
                    setShowToolTip3(true);
                    copyToClipboard(
                      allData.length ? allData[indexSelected].deviceId : "n/a"
                    );
                  }}
                />
              </Tooltip>
            </ClickAwayListener>
          </Typography>
          <Typography color='primary'>Not my sleep?</Typography>
        </div>
        <div className='user-details__bottom mt-2'>
          <div className='d-flex align-items-center mt-2'>
            {sleepDataList.length
              ? createButton(
                  sleepDataList,
                  Button,
                  setIndexSelected,
                  indexSelected
                )
              : ""}
          </div>
          <form className='d-flex align-items-center'>
            <div className='filter-box ml-2'>
              {showDates && (
                <SearchDate
                  setShowDates={setShowDates}
                  specificDate={specificDate}
                  showDates={showDates}
                  toDate={toDate}
                  setToDate={setToDate}
                  fromDate={fromDate}
                  setFromDate={setFromDate}
                />
              )}
              <Button
                color='primary'
                variant='contained'
                onClick={toggleShowDates}>
                Change Dates
              </Button>
            </div>
          </form>
        </div>
      </div>
      <div className='sleep-data-donut'>
        <div className='sleep-data-box'>
          <Typography color='primary' variant='h6'>
            Sleep Data
          </Typography>
          <Divider />
          <div className='sleep-data-content'>
            {sleepDataList.length ? (
              <SleepDataTable data={sleepDataList[indexSelected]} />
            ) : responded && !dataAvailable ? (
              "DATA UNAVAILABLE"
            ) : (
              <LinearProgress />
            )}
          </div>
        </div>
        <div className='sleep-score-box'>
          <Typography color='primary' variant='h6'>
            Sleep Score
          </Typography>
          <Divider />
          <div className='sleep-score-content'>
            {sleepDataList.length ? (
              <DonutGenerator sleepData={sleepDataList[indexSelected]} />
            ) : responded && !dataAvailable ? (
              "DATA UNAVAILABLE"
            ) : (
              <CircularProgress />
            )}
          </div>
        </div>
        <div className='sleep-analysis-box'>
          <Typography color='primary' variant='h6'>
            Sleep Analysis
          </Typography>
          <Divider />
          <div className='sleep-score-content'>
            {sleepDataList.length ? (
              <SleepAnalysisTable
                data={sleepDataList[indexSelected].Properties}
              />
            ) : responded && !dataAvailable ? (
              "DATA UNAVAILABLE"
            ) : (
              <LinearProgress />
            )}
          </div>
        </div>
      </div>
      <div className='sleep-mixed-graph'>
        {allData.length ? (
          <>
            <p>AHI </p>

            <DataGraph
              plotdata={allData[indexSelected]}
              stagesList={sleepDataList[indexSelected].Stages}
            />
            {/* <DataGraph plotdata={allData.reverse} /> */}
          </>
        ) : responded && !dataAvailable ? (
          "DATA UNAVAILABLE"
        ) : (
          <LinearProgress />
        )}
      </div>
    </div>
  );
}

export default NewSleepDataPage;
