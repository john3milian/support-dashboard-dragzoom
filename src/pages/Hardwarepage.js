import {
  AppBar,
  Box,
  Button,
  Divider,
  FormControl,
  FormControlLabel,
  FormLabel,
  LinearProgress,
  Paper,
  Radio,
  RadioGroup,
  Tab,
  Tabs,
  TextField,
  Typography,
} from "@material-ui/core";
import moment from "moment";
import React, { useEffect, useState, Suspense } from "react";
import HdTimeline, {
  DiagnosticsTimeline,
} from "../components/hardware/HdTimeline";
import MyLineChart from "../components/hardware/HardwareGraph";
import HardwareTable from "../components/hardware/HardwareTable";
import SearchNumber from "../components/SearchNumber";
import "../css/Hardware.css";
import API from "../api";
import { Alert } from "@material-ui/lab";
import {
  TabPanel,
  a11yProps,
  autoCompleteDPNumber,
  autoCompleteDOZNumber,
  formatData,
} from "../helpers/hardwareHelper";
import DetailsTab from "../components/hardware/DetailsTab";
import LogsTab from "../components/hardware/LogsTab";
import FirmwareHistoryTab from "../components/hardware/FirmwareHistoryTab";

function Hardwarepage() {
  const [noOfDays, setNoOfDays] = useState("1");
  const [deviceIdentifier, setDeviceIdentifier] = useState("");
  // const [deviceId, setDeviceId] = useState("");
  const [showLogsComponent, setShowLogsComponent] = useState(false);
  const [logData, setLogData] = useState([]);
  const [showLoader, setShowLoader] = useState(false);
  const [prefix, setPrefix] = useState("DOZ");
  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  const LazyTimeline = React.lazy(() =>
    import("../components/hardware/HdTimeline")
  );

  const diagnosticsData = [
    {
      key: "DEVICETIME",
      value: "2021-10-05 12:00:00",
      correct: true,
    },
    {
      key: "SDCARD",
      value: "1",
      correct: true,
    },
    {
      key: "WIFI",
      value: "NOSETUP",
      correct: true,
    },
    {
      key: "SHEET",
      value: "183282372",
      correct: true,
    },
    {
      key: "NUMUPLOADS",
      value: "129812938213",
      correct: false,
    },
  ];
  const fetchDeviceId = async (days, deviceNumber) => {
    let api_postfix = "";
    if (prefix === "ID") {
      api_postfix = `deviceInfoByID?deviceid=${deviceIdentifier}`;
    } else if (prefix === "DOZ") {
      api_postfix = autoCompleteDOZNumber(deviceIdentifier, prefix);
    } else {
      api_postfix = autoCompleteDPNumber(deviceIdentifier, prefix);
    }
    API.get(`/api/v1/${api_postfix}`)
      .then((results) => {
        const body = results.data;
        const temp_deviceId = body?.Device_Info?.Device_ID || "";
        fetchData(days, temp_deviceId);
      })
      .catch((error) => {
        setError(true);
        setErrorMsg(error.response.data.Action);
      });
  };

  const fetchData = async (days, id) => {
    // 0a854294-6417-4945-97b7-cff985c3fd35
    console.log(days, id);
    if (id.length > 0) {
      API.get(`/api/hardware/devicelogs?deviceId=${id}&duration=${days}`)
        .then((results) => {
          const body = results.data;
          formatData(body["hits"]["hits"], moment, setLogData, setShowLoader);
          setShowLogsComponent(true);
        })
        .catch((error) => {
          setError(true);
          setErrorMsg(error.response.data.Action);
        });
    }
  };

  const handleDaysChange = (e) => {
    setShowLoader(true);
    const temp_days = e.target.value;
    setNoOfDays(temp_days);
    console.log(temp_days);
    if (deviceIdentifier.length > 0) {
      if (deviceIdentifier.length > 10) {
        fetchData(temp_days, deviceIdentifier);
      } else {
        fetchDeviceId(temp_days, deviceIdentifier);
      }
    }
  };

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <div>
      <div className='hardware_top'>
        <Typography color='textPrimary' variant='h4'>
          Hardware
        </Typography>
        <form
          className='input_button'
          onSubmit={(e) => {
            console.table("Here here!!!");
            setShowLoader(true);
            e.preventDefault();
            if (deviceIdentifier.length > 10) {
              fetchData(noOfDays, deviceIdentifier);
            } else {
              fetchDeviceId(noOfDays, deviceIdentifier);
            }
          }}>
          <SearchNumber
            setPrefix={setPrefix}
            prefix={prefix}
            deviceIdentifier={deviceIdentifier}
            setDeviceIdentifier={setDeviceIdentifier}
            fromHardware={true}
          />
          {/* <TextField
            variant='outlined'
            placeholder='Device Id/ Device Number'
            size='small'
            style={{ outline: 0 }}
            fullWidth
            value={deviceIdentifier}
            onChange={(e) => setDeviceIdentifier(e.target.value)}
          /> */}
          <Button variant='contained' type='submit' style={{ outline: "0" }}>
            Go
          </Button>
        </form>
      </div>

      <div className='mt-4'></div>
      <LogsTab
        noOfDays={noOfDays}
        handleDaysChange={handleDaysChange}
        error={error}
        showLoader={showLoader}
        showLogsComponent={showLogsComponent}
        logData={logData}
        errorMsg={errorMsg}
      />

      {/* <AppBar position='static'>
        <Tabs
          // indicatorColor='primary'
          // textColor='secondary'
          value={value}
          onChange={handleChange}
          aria-label='simple tabs example'>
          <Tab label='Details' {...a11yProps(0)} />
          <Tab label='Logs' {...a11yProps(1)} />
          <Tab label='Diagnostics' {...a11yProps(2)} />\
          <Tab label='Firmware History' {...a11yProps(3)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <DetailsTab />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <div style={{ overflow: "scroll", maxHeight: "100%" }}>
          <LogsTab
            noOfDays={noOfDays}
            handleDaysChange={handleDaysChange}
            error={error}
            showLoader={showLoader}
            showLogsComponent={showLogsComponent}
            logData={logData}
            errorMsg={errorMsg}
          />
        </div>
      </TabPanel>
      <TabPanel value={value} index={2}>
        <div className='d-flex align-items-center justify-content-center'>
          <div className='w-75'>
            <DiagnosticsTimeline diagnosticsData={diagnosticsData} />
          </div>
        </div>
      </TabPanel>
      <TabPanel value={value} index={3}>
        <FirmwareHistoryTab />
      </TabPanel> */}
    </div>
  );
}

export default Hardwarepage;
