import { Button, Typography } from "@material-ui/core";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import Divider from "@material-ui/core/Divider";
// import reportsData from "../reports-data.json";
// import reportsData from "../UpdatedReportsData.json";
import { useState } from "react";
import "../css/Reports.css";
import Element from "../components/Tasks/Element";
import { TextField } from "@material-ui/core";
import { TaskContext } from "../context/TaskContext";
import { useEffect } from "react";
import API from "../api";

function Tasks() {
  const [reportsData, setReportsData] = useState([]);
  const [loader, setLoader] = useState(true);
  const [selectedReport, setSelectedReport] = useState(0);
  const [selectedVariant, setSelectedVariant] = useState(-1);
  const [elements, setElements] = useState([]);

  useEffect(() => {
    // 134.209.150.44:1620
    API.get(`http://134.209.150.44:1620/executor`).then((results) => {
      setReportsData(results.data);
      // setLoader(false);
    });
  }, []);

  const onSelectTask = (e) => {
    setSelectedReport(parseInt(e.target.value));
    setElements([]);
  };

  const onSelectVariant = (e) => {
    setSelectedVariant(parseInt(e.target.value));
    setElements(
      reportsData[selectedReport].properties[parseInt(e.target.value)].values
    );
  };

  const handleEdit = (eId, value) => {
    const newElements = [...elements];
    newElements.forEach((eachField) => {
      const { id, type } = eachField;
      if (eId === id) {
        eachField["value"] = value !== undefined ? value : "";
      }
      setElements(newElements);
    });
  };

  const handleSubmit = () => {
    console.log("submitted");
    const newElements = [...elements];
    const finalData = {
      lastrun: 0,
      nextrun: 0,
      step: 0,
      state: "success",
      retry: 0,
      cutoff: 0,
      arguments: {},
      executor: reportsData[selectedReport]["name"],
      creator: localStorage.getItem("userEmail"),
    };
    newElements.map((eachField) => {
      var tempVal = eachField.id.split(".");
      console.log(tempVal);
      if (eachField.value !== "") {
        if (eachField.id.includes("arguments.")) {
          finalData["arguments"][tempVal[-1]] = eachField.value;
        } else {
          console.log(tempVal[0]);
          finalData[tempVal[0]] = eachField.value;
        }
      }
    });

    API.post(`http://134.209.150.44:1620/task`, finalData)
      .then((results) => console.log(results))
      .catch((err) => console.error(err));

    console.log(finalData);
  };

  return (
    <div>
      {loader && reportsData.length > 0 && (
        <TaskContext.Provider value={{ handleEdit }}>
          <div className='page_top_selector w-25'>
            <Typography variant='h4'>Tasks</Typography>
            <div className='selector'>
              <InputLabel htmlFor='age-native-simple'>Select a task</InputLabel>
              <Select
                fullWidth
                native
                value={selectedReport}
                onChange={onSelectTask}
                inputProps={{
                  name: "age",
                  id: "age-native-simple",
                }}>
                <option aria-label='None' value={-1} />
                {reportsData.map((eachReport, index) => {
                  return <option value={index}>{eachReport?.name}</option>;
                })}
              </Select>
            </div>
          </div>
          <Divider variant='middle' className='mt-3' />
          {selectedReport !== -1 && (
            <>
              <div className='report_selected'>
                {/* <Typography variant='h5'>
            {reportsData[selectedReport]["name"]}
          </Typography> */}
              </div>
              <div style={{ width: "10vw" }} className='variant_selector'>
                <InputLabel htmlFor='age-native-simple'>
                  Select Variant
                </InputLabel>
                <Select
                  fullWidth
                  native
                  value={selectedVariant}
                  onChange={(e) => onSelectVariant(e)}
                  inputProps={{
                    name: "age",
                    id: "age-native-simple",
                  }}>
                  <option aria-label='None' value={-1}>
                    --Select a variant--
                  </option>
                  {reportsData[selectedReport].properties.map(
                    (eachProperty, index) => (
                      <option value={index}>
                        {eachProperty?.title.toUpperCase()}
                      </option>
                    )
                  )}
                </Select>
              </div>

              {selectedVariant !== -1 && (
                <div className='input_boxes'>
                  {elements.map((eachInput) => (
                    <>
                      <div className='w-50 my-2'>
                        <Element eachInput={eachInput} />
                        {/* <TextField
                      InputLabelProps={{
                        shrink: true,
                      }}
                      type={eachInput.type}
                      fullWidth
                      variant='outlined'
                      label={eachInput.label}
                    /> */}
                      </div>
                    </>
                  ))}

                  <Button
                    className='w-25'
                    style={{ float: "right !important" }}
                    variant='contained'
                    color='primary'
                    onClick={handleSubmit}>
                    Run
                  </Button>
                </div>
              )}
            </>
          )}
        </TaskContext.Provider>
      )}
    </div>
  );
}

export default Tasks;
