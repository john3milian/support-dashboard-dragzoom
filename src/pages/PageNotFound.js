import { Typography } from "@material-ui/core";
import React from "react";

function PageNotFound() {
  return (
    <div className='d-flex align-items-center justify-content-center flex-column'>
      <Typography variant='h4'>404</Typography>
      <Typography variant='h4'>PAGE NOT FOUND</Typography>
    </div>
  );
}

export default PageNotFound;
