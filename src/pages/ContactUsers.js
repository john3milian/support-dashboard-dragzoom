import { makeStyles, Paper, TextField, Typography } from "@material-ui/core";
import React from "react";
import MaturedUsersWhatsapp from "../components/MaturedUsers";
import MaturedUsersCall from "../components/MaturedUsersCall";

const useStyles = makeStyles({
  container: {
    display: "flex",
    margin: "auto",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
    marginTop: "1%",
    marginBottom: "2%",
    marginLeft: "5vw",
  },
  searchBox: {
    minWidth: "900px",
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
    padding: "20px",
  },
  searchFieldBox: {
    marginTop: "3%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    width: "100%",
  },
});
function ContactUsers() {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <Paper className={classes.searchBox}>
        <Typography
          variant='h6'
          color='primary'
          style={{ textAlign: "left", marginBottom: "5px", width: "100%" }}>
          Find User
        </Typography>
        <div className={classes.searchFieldBox}>
          <TextField id='dozee-number' variant='outlined' label='Dozee No' />
          <TextField
            id='dozee-number'
            variant='outlined'
            label='Name'
            disabled
            InputLabelProps={{
              shrink: true,
            }}
            value={"kaustubh"}
          />
          <TextField
            id='dozee-number'
            variant='outlined'
            label='Phone No'
            InputLabelProps={{
              shrink: true,
            }}
            disabled
            value={"9619809793"}
          />
          <TextField
            id='dozee-number'
            variant='outlined'
            label='Medical Condition'
            InputLabelProps={{
              shrink: true,
            }}
            value={"None"}
            disabled
          />
        </div>
      </Paper>
      <MaturedUsersWhatsapp />
      <MaturedUsersCall />
    </div>
  );
}

export default ContactUsers;
