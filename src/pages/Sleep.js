import { Button, CircularProgress, Typography } from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import moment from "moment";
import React, { useEffect, useState } from "react";
import Moment from "react-moment";
import { useHistory } from "react-router";
import SearchNumber from "../components/SearchNumber";
import "../css/SleepPage.css";
import API from "../api";
import NotAuthorized from "../components/central/NotAuthorized";

function Sleep() {
  const [deviceIdentifier, setDeviceIdentifier] = useState("");
  const [showContent, setShowContent] = useState();
  const [showLoader, setShowLoader] = useState();
  const [showError, setShowError] = useState();
  const [errorMsg, setErrorMsg] = useState("");
  const [userSleeps, setUserSleeps] = useState([]);
  const [userId, setUserId] = useState("");
  const [info, setInfo] = useState({});
  const [orgInfo, setOrgInfo] = useState({ orgName: "", orgId: "" });
  const [prefix, setPrefix] = useState("DOZ");

  const history = useHistory();

  const convertTime = (dateTime) => {
    var updatedDateTime = moment(dateTime, "YYYY-MM-DDTHH:mm:SS").add({
      minutes: 30,
      hours: 5,
    });
    return updatedDateTime;
  };

  const fetchUserSleeps = async (userId) => {
    setShowError(false);
    const now = new Date();
    var oneWeekAgo = new Date();
    const weekAgo = now.getDate() - 7;
    oneWeekAgo.setDate(weekAgo);
    if (userId !== undefined && userId !== "" && userId !== null) {
      API.get(
        `/api/v1/sleep/list?userId=${userId}&fromTime=${Math.round(
          oneWeekAgo.getTime() / 1000
        )}&toTime=${Math.round(now.getTime() / 1000)}`
      )
        .then((results) => {
          const body = results.data;
          console.log(body);
          setUserSleeps(body);
          setShowContent(true);
        })
        .catch((error) => {
          setErrorMsg(error.response.data.Action);
          setShowError(true);
        });
    } else {
      setErrorMsg("Not yet paired");
      setShowError(true);
    }
    setShowLoader(false);
  };

  const autoCompleteDOZNumber = (deviceIdentifier) => {
    let auto_append_zeros = "0".repeat(4 - deviceIdentifier.length);
    if (auto_append_zeros.length > 0) {
      return `deviceInfoByName?devicename=${prefix}-${auto_append_zeros}${deviceIdentifier}`;
    } else {
      return `deviceInfoByName?devicename=${prefix}-${deviceIdentifier}`;
    }
  };

  const autoCompleteDPNumber = (deviceIdentifier) => {
    let auto_append_zeros = "0".repeat(5 - deviceIdentifier.length);
    if (auto_append_zeros.length > 0) {
      return `deviceInfoByName?devicename=${prefix}-${auto_append_zeros}${deviceIdentifier}`;
    } else {
      return `deviceInfoByName?devicename=${prefix}-${deviceIdentifier}`;
    }
  };
  const handleSubmit = () => {
    if (deviceIdentifier.length === 0) {
      setErrorMsg("Field cannot be empty");
      setShowError(true);
      return;
    }
    if (showContent) {
      setShowContent(false);
    }
    setShowLoader(true);
    let api_postfix = "";

    if (prefix === "ID") {
      api_postfix = `deviceInfoByID?deviceid=${deviceIdentifier}`;
    } else if (prefix === "DOZ") {
      api_postfix = autoCompleteDOZNumber(deviceIdentifier);
    } else {
      api_postfix = autoCompleteDPNumber(deviceIdentifier);
    }

    const fetchUserId = async () => {
      // const results = await
      API.get(`/api/v1/${api_postfix}`)
        .then((results) => {
          console.log(results);
          if (results.status !== 200) {
            setShowError(true);
            setErrorMsg(results.body);
          } else {
            const body = results.data;
            console.log(body);
            setInfo(body);
            console.log(body.OrgId);
            if (body["Device_Info"]["Paired"] === -1) {
              setShowError(true);
              setErrorMsg(body["Device_Info"]["Device_Name"]);
              setShowLoader(false);
              setShowContent(true);
              // setShowContent(false);
              return;
            } else if (body["Device_Info"]["Paired"] === 0) {
              setShowError(true);
              setErrorMsg("Not Yet Paired!");
              setShowLoader(false);
              setShowContent(true);
              // setShowContent(false);
              return;
            }
            const userId = body["UserID"];
            console.log(Object.keys(body));
            setUserId(userId);
            // fetchOrgDetails(userId);
            fetchUserSleeps(userId);
          }
        })
        .catch((error) => {
          setShowError(true);
          setErrorMsg(error.response.data.Action);
          setShowLoader(false);
          setShowContent(false);
          return;
        });
    };

    fetchUserId();
  };
  return (
    <div>
      <Typography color='textPrimary' variant='h4'>
        Sleep
      </Typography>
      <div className='sleep-body'>
        <div className='sleep-body__top'>
          <form className='sleep-body__topForm'>
            <div className='d-flex align-items-center w-100 justify-content-center'>
              <SearchNumber
                setShowError={setShowError}
                setPrefix={setPrefix}
                prefix={prefix}
                deviceIdentifier={deviceIdentifier}
                setDeviceIdentifier={setDeviceIdentifier}
                fromHardware={false}
              />
              <Button
                color='primary'
                variant='contained'
                type='sumit'
                className='ml-3'
                onClick={(e) => {
                  e.preventDefault();
                  localStorage.setItem("deviceIdentifier", deviceIdentifier);
                  handleSubmit();
                }}>
                Search
              </Button>
            </div>
          </form>
        </div>
        {showLoader && (
          <div className='sleep-body__bottom'>
            <CircularProgress />
          </div>
        )}
        {console.log(errorMsg === "Not Authorized to view this Page!!")}

        {showError && (
          <div className='d-flex align-items-center justify-content-center mt-2'>
            {errorMsg === "Not Authorized to view this Page!!" ? (
              <NotAuthorized />
            ) : (
              <Alert severity='error'>{errorMsg}</Alert>
            )}
          </div>
        )}
        {showContent && (
          <div className='sleep-body__bottom'>
            <div className='sleep-body__bottomButtons'>
              <Button
                disabled={
                  info["Device_Info"]["Paired"] === -1 ||
                  info["Device_Info"]["Paired"] === 0
                    ? true
                    : false
                }
                color='primary'
                variant='outlined'
                disabled={!userSleeps.length}
                onClick={() => {
                  // history.push(`/sleep-quality/${userId}`);

                  const win = window.open(
                    `#/sleep-quality/${userSleeps[0]["SleepId"]}`
                  );
                  win.focus();
                }}>
                Quality
              </Button>
              <Button
                disabled={
                  info["Device_Info"]["Paired"] === -1 ||
                  info["Device_Info"]["Paired"] === 0
                    ? true
                    : false
                }
                color='primary'
                variant='outlined'
                onClick={() => {
                  // history.push(`/sleep-data/${userId}`);
                  const win = window.open(`#/new-sleep-data/${userId}`);
                  win.focus();
                }}>
                Sleeps (Aurora)
              </Button>
              <Button
                color='primary'
                variant='outlined'
                onClick={() => {
                  // history.push(`/sleep-data/${userId}`);
                  const win = window.open(`#/sleep-data/${userId}`);
                  win.focus();
                }}>
                Sleeps (Datastore)
              </Button>
            </div>

            <div className='sleep-body__bottomColors'>
              {userSleeps.length ? (
                userSleeps.map((oneSleep, key) => (
                  <div
                    onClick={() => {
                      // history.push(`/sleep-quality/${userId}`);

                      const win = window.open(
                        `#/sleep-quality/${oneSleep["SleepId"]}`
                      );
                      win.focus();
                    }}
                    key={key}
                    className={
                      `colored_${
                        oneSleep["SleepAhis"][0]?.Color
                          ? oneSleep["SleepAhis"][0]?.Color
                          : "undef"
                      }` +
                      " colored_box d-flex align-items-center justify-content-center"
                    }>
                    <Typography variant='body1'>
                      <Moment format='Do MMM YY HH:mm'>
                        {convertTime(oneSleep["WakeupTime"])}
                      </Moment>
                    </Typography>
                  </div>
                ))
              ) : (
                <Alert severity='warning'>No recent sleeps found</Alert>
              )}
            </div>

            <div className='sleep-body__bottomInfo'>
              <table className=' table table-bordered table-striped'>
                <tbody className='table_body'>
                  <tr>
                    <td>Device Name:</td>
                    <td className='dataInfo'>
                      {info["Device_Info"]["Device_Name"]}
                    </td>
                    <td>Device Id:</td>
                    <td className='dataInfo'>
                      {info["Device_Info"]["Device_ID"]}
                    </td>
                  </tr>
                  <tr className='mb-1'>
                    <td>User Id:</td>
                    <td className='dataInfo'>
                      {info["UserID"] ? info["UserID"] : "--"}
                    </td>
                    <td>Name:</td>
                    <td className='dataInfo'>
                      {info["Personal"] ? info["Personal"]["Name"] : "--"}
                    </td>
                  </tr>
                  <tr>
                    <td>Email Id:</td>
                    <td className='dataInfo'>
                      {info["Personal"] ? info["Personal"]["Email"] : "--"}
                    </td>
                    <td>Date Of Pairing:</td>
                    <td className='dataInfo'>
                      {info["Device_Info"]["Paired"] === -1 ? (
                        "--"
                      ) : (
                        <Moment format='Do MMM YY'>
                          {new Date(info["Device_Info"]["Paired"] * 1000)}
                        </Moment>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>Orgnaization Id:</td>
                    <td className='dataInfo'>{info["OrgId"] || "--"}</td>
                    <td>Orgnaization Name:</td>
                    <td className='dataInfo'>{info["OrgName"] || "--"}</td>
                  </tr>
                  <tr>
                    <td>Sign up Date:</td>
                    <td className='dataInfo'>{"11th Jan 2021"}</td>
                    <td>Location:</td>
                    <td className='dataInfo'>{" -- "}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default Sleep;
