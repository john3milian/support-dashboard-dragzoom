import {
  Button,
  CircularProgress,
  FormControl,
  InputLabel,
  LinearProgress,
  MenuItem,
  Select,
  TextField,
  Tooltip,
  Typography,
} from "@material-ui/core";
import GetAppIcon from "@material-ui/icons/GetApp";
import React, { useEffect, useState } from "react";
import "../css/Alerts.css";
import ClearIcon from "@material-ui/icons/Clear";
import AlertTable from "../components/alerts/AlertTable";
import { Alert, Autocomplete } from "@material-ui/lab";
import AutorenewIcon from "@material-ui/icons/Autorenew";
import CsvDownload from "react-json-to-csv";

import {
  fetchAllOrgs,
  sortUsingColor,
  fetchData,
} from "../helpers/alertsHelper";
import NotAuthorized from "../components/central/NotAuthorized";

function Alerts() {
  const [filerAll, setFilterAll] = useState();
  const [filterFeedback, setFilterFeedback] = useState();
  const [filerUnAttnd, setFilterUnAttnd] = useState();
  const [sortByColor, setSortByColor] = useState();
  const [loading, setLoading] = useState();
  const [error, setError] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  const [userId, setUserId] = useState("");
  const [searchUserName, setSearchUserName] = useState("");
  const [orgId, setOrgId] = useState("");
  const [userIds, setUserIds] = useState([]);
  const [orgNameSelection, setOrgNameSelection] = useState([]);
  const [attendedFilter, setAttendedFilter] = useState("all");
  const [selectedOrgName, setSelectedOrgName] = useState("");

  const [selectedOption, setSelectedOption] = useState("");

  const [alertsData, setAlertsData] = useState([]);
  const [secondayAlertsData, setSecondayAlertsData] = useState([]);

  useEffect(() => {
    setFilterAll(false);
    setFilterFeedback(false);
    setFilterUnAttnd(false);
    setSortByColor(false);
    setLoading(false);
    setSelectedOption("name");
  }, []);

  useEffect(() => {
    try {
      setLoading(true);
      fetchAllOrgs(setOrgNameSelection, setError, setErrorMsg);
      fetchData(
        [],
        setLoading,
        setError,
        setAlertsData,
        setSecondayAlertsData,
        setSortByColor,
        setErrorMsg
      );
    } catch {
      setError(true);
    }
  }, []);

  const searchingUsername = (username) => {
    if (username !== "") {
      const tempAlertsData = secondayAlertsData.filter((eachItem) => {
        // return eachItem["OrganizationNames"].map((org) =>
        //   org.toLowerCase().includes(username)
        // );
        return eachItem["UserName"].toLowerCase().includes(username);
      });
      console.log(tempAlertsData);
      setAlertsData(tempAlertsData);
    } else {
      setAlertsData(secondayAlertsData);
    }
  };

  const filterBasedOnAttended = (selectedFilter) => {
    console.log(selectedFilter);
    if (selectedFilter === "unattended") {
      const newFiltered = secondayAlertsData
        .filter((eachItem) => {
          return Object.keys(eachItem["Attender"]).length === 0;
        })
        .sort((a, b) => {
          if (a["CreatedAt"] > b["CreatedAt"]) {
            return -1;
          } else {
            return 1;
          }
        });
      setAlertsData(newFiltered);
    } else if (selectedFilter === "attended") {
      const newFiltered = secondayAlertsData
        .filter((eachItem) => {
          return Object.keys(eachItem["Attender"]).length !== 0;
        })
        .sort((a, b) => {
          if (a["CreatedAt"] > b["CreatedAt"]) {
            return -1;
          } else {
            return 1;
          }
        });
      setAlertsData(newFiltered);
    } else {
      const newFiltered = secondayAlertsData;
      setAlertsData(newFiltered);
    }
  };

  const filterFeedbacksFunction = () => {
    const newFiltered = secondayAlertsData
      .filter(
        (eachItem) => {
          return eachItem["Remarks"]
            .map((oneRemark) => {
              if (oneRemark.remark.includes("Feedback")) {
                console.log(oneRemark);
              }
              return oneRemark.remark.includes("Feedback");
            })
            .includes(true);
        }
        //   if (eachItem["Remarks"].length > 0) {
        // }
      )
      .sort((a, b) => {
        if (a["CreatedAt"] > b["CreatedAt"]) {
          return -1;
        } else {
          return 1;
        }
      });

    console.log(newFiltered);
    setAlertsData(newFiltered);
  };

  return (
    <div>
      <Typography color='textPrimary' variant='h4'>
        Alerts
      </Typography>
      <div className='d-flex  justify-content-between flex-column mt-2'>
        <div className='d-flex flex-row'>
          <form className='input-div'>
            {/* http://139.59.20.84:2380/api/orgs/get */}
            <FormControl variant='outlined' size='small' style={{ outline: 0 }}>
              <Select
                style={{ outline: 0 }}
                native
                value={selectedOption}
                onChange={(e) => setSelectedOption(e.target.value)}>
                <option value={"id"}>Org Id</option>
                <option value={"name"}>Org Name</option>
              </Select>
            </FormControl>
            {selectedOption === "id" && (
              <TextField
                size='small'
                variant='outlined'
                className='userId-field w-25 mr-2 ml-2'
                placeholder='Organization-Id'
                value={orgId}
                onChange={(e) => setOrgId(e.target.value)}
              />
            )}

            {selectedOption === "name" && (
              <Autocomplete
                id='combo-box-demo'
                size='small'
                className='userId-field w-25 mr-2'
                // options={top100Films}
                noOptionsText={"No organisation found"}
                onChange={(e, newValue) => {
                  if (newValue) {
                    setOrgId(newValue?.OrganizationId);
                    setSelectedOrgName(newValue?.OrganizationName);
                  }
                }}
                options={orgNameSelection}
                getOptionLabel={(option) =>
                  option.OrganizationName.charAt(0).toUpperCase() +
                  option.OrganizationName.toLowerCase().slice(1)
                }
                style={{ width: 300 }}
                selectOnFocus
                renderInput={(params) => (
                  <TextField
                    {...params}
                    placeholder='Organisation Name'
                    variant='outlined'
                  />
                )}
              />
            )}
            <Button
              variant='contained'
              color='primary'
              className='ml-2'
              onClick={() =>
                fetchData(
                  [orgId],
                  setLoading,
                  setError,
                  setAlertsData,
                  setSecondayAlertsData,
                  setSortByColor,
                  setErrorMsg
                )
              }>
              Go
            </Button>
          </form>
          {/* <CsvDownload
            data={secondayAlertsData}
            filename='data.csv'
            className='MuiButtonBase-root MuiButton-root MuiButton-contained MuiButton-containedPrimary m-0 py-0'>
            <small>Download</small>{" "}
            <GetAppIcon fontSize='small' className='py-0' />
          </CsvDownload> */}
        </div>

        <form></form>

        <div className='d-flex justify-content-between flex-row-reverse  align-items-center'>
          <div className='w-25 d-flex'>
            <TextField
              onChange={(e) => searchingUsername(e.target.value)}
              variant='outlined'
              label='Search By Username'
              fullWidth
            />

            <Button
              style={{ float: "right", outline: 0 }}
              className='ml-2'
              onClick={() =>
                fetchData(
                  [],
                  setLoading,
                  setError,
                  setAlertsData,
                  setSecondayAlertsData,
                  setSortByColor,
                  setErrorMsg
                )
              }>
              <AutorenewIcon fontSize='large' />
            </Button>

            {/* <Tooltip arrow title='Download as CSV' placement='top'>
              <Button variant='contained' color='primary' className='px-4 py-2'>
                <GetAppIcon />
              </Button>
            </Tooltip> */}
          </div>
          <div className='alerts-body-filters mb-2'>
            <FormControl variant='outlined' style={{ width: "40%" }}>
              <InputLabel htmlFor='outlined-age-native-simple'>
                Filter based on
              </InputLabel>
              <Select
                native
                label='Filter based on'
                value={attendedFilter}
                onChange={(e) => {
                  setAttendedFilter(e.target.value);
                  filterBasedOnAttended(e.target.value);
                }}
                inputProps={{
                  name: "Filter based on",
                  id: "outlined-age-native-simple",
                }}>
                <option value={"all"}>All</option>
                <option value={"unattended"}>Unattended</option>
                <option value={"attended"}>Attended</option>
              </Select>
            </FormControl>
            <Button
              variant={sortByColor ? "contained" : "outlined"}
              endIcon={sortByColor && <ClearIcon />}
              color='primary'
              onClick={() => {
                if (!sortByColor) {
                  sortUsingColor(alertsData, setAlertsData);
                } else {
                  setAlertsData(secondayAlertsData);
                }
                setSortByColor(!sortByColor);
              }}>
              Sort By Color
            </Button>
            {/* <Button
              color='primary'
              variant={filterFeedback ? "contained" : "outlined"}
              endIcon={filterFeedback && <ClearIcon />}
              onClick={() => {
                if (!filterFeedback) {
                  filterFeedbacksFunction();
                } else {
                  setAlertsData(secondayAlertsData);
                }
                setFilterFeedback(!filterFeedback);
              }}>
              Filter Feedback
            </Button> */}
          </div>
        </div>
      </div>

      <div className='alerts-body-table'>
        {error ? (
          errorMsg === "Not Authorized to view this Page!!" ? (
            <NotAuthorized />
          ) : (
            <Alert severity='error'>{errorMsg}</Alert>
          )
        ) : loading ? (
          <LinearProgress variant='indeterminate' />
        ) : (
          <AlertTable
            alertsData={alertsData}
            selectedFilter={attendedFilter}
            loading={loading}
            fetchData={fetchData}
          />
        )}
      </div>
    </div>
  );
}

export default Alerts;
