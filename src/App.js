import "./App.css";

import Login from "./components/Login";
import {
  HashRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import ProtectedRoute from "./ProtectedRoute";
import NavDrawer from "./components/NavDrawer";
import { makeStyles, ThemeProvider } from "@material-ui/core";
import theme from "./theme";
import SleepsData from "./pages/SleepData";
import SleepDataPage from "./pages/SleepDataPage";
import NewSleepDataPage from "./pages/NewSleepDataPage";
import Ops from "./pages/Ops";
import SleepQuality from "./pages/SleepQuality";
import Sleep from "./pages/Sleep";
import Alerts from "./pages/Alerts";
import PageNotFound from "./pages/PageNotFound";

import HardwarePage from "./pages/Hardwarepage";
import Stats from "./pages/Stats";
import Licensing from "./pages/Licensing";
import Admin from "./pages/Admin"
import NotAuthorized from "./components/central/NotAuthorized";
import { Alert } from "@material-ui/lab";
import Home from "./pages/Home";
import Tasks from "./pages/Tasks";

const useStyles = makeStyles((theme) => ({
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
  },
  content: {
    paddingLeft: "90px",
    paddingRight: "20px",
    flexGrow: 1,
    padding: theme.spacing(3),
    paddingBottom: "5px",
  },
}));

function App() {
  const classes = useStyles();
  function isAuth() {
    const authToken = localStorage.getItem("authToken");
    const userEmailId = localStorage.getItem("userEmail");
    if (
      !authToken ||
      authToken === undefined ||
      !userEmailId ||
      userEmailId === undefined
    ) {
      return false;
    }
    return true;
  }
  function isAuthorized() {
    const authToken = localStorage.getItem("authTeam");
    if (!authToken || authToken === undefined) {
      return false;
    }
    return true;
  }
  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Switch>
          <div className='App'>
            <NavDrawer isAuth={isAuth} />
            <main className={classes.content}>
              <div className={classes.toolbar} />

              <Route
                exact
                path='/login'
                render={() => (isAuth() ? <Redirect to='/' /> : <Login />)}
              />

              <Route path='/licensing'>
                <Licensing />
              </Route>

              <Route path= '/admin'>
                <Admin />
              </Route>

              <ProtectedRoute
                exact
                isAuth={isAuth}
                path='/sleep'
                component={Sleep}
              />
              <ProtectedRoute
                exact
                isAuth={isAuth}
                path='/alerts'
                component={Alerts}
              />
              {/*BACKUP WITH SUPPORTX LAYOUT  */}
              <ProtectedRoute
                exact
                isAuth={isAuth}
                path='/sleep-backup'
                component={SleepsData}
              />
              <ProtectedRoute
                exact
                isAuth={isAuth}
                path='/new-sleep-data/:userId'
                component={NewSleepDataPage}
              />
              <ProtectedRoute
                exact
                isAuth={isAuth}
                path='/sleep-data/:userId'
                component={SleepDataPage}
              />
              <ProtectedRoute
                exact
                isAuth={isAuth}
                path='/ops-:deviceId'
                component={Ops}
              />
              <ProtectedRoute
                exact
                isAuth={isAuth}
                path='/sleep-quality/:sleepId'
                component={SleepQuality}
              />
              <ProtectedRoute
                exact
                isAuth={isAuth}
                path='/hardware'
                component={HardwarePage}
              />

              <ProtectedRoute
                exact
                isAuth={isAuth}
                path='/stats'
                component={Stats}
              />
              <ProtectedRoute
                exact
                isAuth={isAuth}
                path='/tasks'
                component={Tasks}
              />
              <ProtectedRoute
                exact
                isAuth={isAuth}
                path='/403'
                component={NotAuthorized}
              />
              {/* Default Home route */}
              <ProtectedRoute isAuth={isAuth} path='/' exact component={Home} />
              <Route path='/page-not-found' component={PageNotFound} />
            </main>
          </div>
        </Switch>
      </Router>
    </ThemeProvider>
  );
}

export default App;
