import { Fragment } from "react";
import { format } from "date-fns";
import moment from "moment";

export const COLUMNS = [
  {
    Header: "Uploaded time",
    accessor: "uploadedAt",
    Cell: ({ value }) => generateDateToSeconds(value),
  },
  {
    Header: "Time taken",
    accessor: "quality.processTime",
  },
  {
    Header: "Transaction #",
    accessor: "transactionId",
  },
  {
    Header: "Header time",
    accessor: "quality.headerTimeStamp",
    Cell: ({ value }) => generateDateToSeconds(value),
  },
  {
    Header: "Status",
    accessor: "status",
  },
  {
    Header: "Base value",
    accessor: "quality.baseValue",
  },
  {
    Header: "Saturation",
    accessor: "quality.saturation",
  },
  {
    Header: "Gain",
    accessor: "quality.gain",
  },
];

function isValidDate(date) {
  const dateValue = new Date(date).toString();
  if (dateValue !== "Invalid Date") {
    return true;
  }
  return false;
}

function isValidNumber(value) {
  return parseInt(value) > 1478009120 && !isNaN(value);
}

function getDate(value) {
  if (isValidDate(value) || isValidNumber(value)) {
    return generateDate(value);
  } else {
    return <></>;
  }
}

function generateDate(value) {
  const adjustedDate = parseInt(value) * 1000;
  const date = new Date(new Date(adjustedDate).toISOString());
  return format(date, "dd MMM yy");
}

function generateDateToSeconds(value) {
  const adjustedDate = parseInt(value) * 1000;
  const date = new Date(new Date(adjustedDate).toISOString());
  //   return "---";
  //   return moment(date).local().format("dd-MM-yy HH:mm:ss");
  return format(date, "dd-MM-yy HH:mm:ss");
}
