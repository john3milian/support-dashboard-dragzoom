const allLevels = [
  {
    level: "Software Admin",
    values: [
      "Home",
      "Quality",
      "Ops",
      "HW",
      "License",
      "Stats",
      "Sleep",
      "Alerts",
      "Admin",
    ],
  },
  {
    level: "Software Development - Sens",
    values: ["Home", "Quality", "Ops", "HW", "License"],
  },
  {
    level: "Software Development",
    values: ["Home", "Quality", "Ops", "HW", "License", "Stats", "Alerts"],
  },
  {
    level: "Data Science",
    values: ["Home", "Quality", "Ops", "HW", "License", "Alerts", "Sleep"],
  },
  {
    level: "Hardware Team",
    values: ["Home", "Quality", "Ops", "HW"],
  },
  {
    level: "Operations - Field",
    values: ["Home", "Quality", "Ops", "HW", "License"],
  },
  {
    level: "Operations - RCSE",
    values: ["Home", "Quality", "Ops", "HW", "License"],
  },
  {
    level: "Operations - Alerts",
    values: ["Home", "Quality", "Ops", "HW", "License", "Alerts"],
  },
  {
    level: "Operations - Super Admin",
    values: ["Home", "Quality", "Ops", "HW", "License"],
  },
  {
    level: "Support Team",
    values: ["Home", "Quality", "Ops", "HW", "License"],
  },
  {
    level: "Account Managers",
    values: ["Home", "Quality", "Ops", "HW", "License"],
  },
  {
    level: "Sales",
    values: [],
  },
  {
    level: "Product",
    values: ["Home", "Quality", "Ops", "HW", "Sleep"],
  },
  {
    level: "Finance Team",
    values: ["License"],
  },
  {
    level: "Management",
    values: [
      "Home",
      "Quality",
      "Ops",
      "Sleep",
      "Stats",
      "HW",
      "License",
      "Alerts",
    ],
  },
  {
    level: "QA Manager",
    values: [
      "Home",
      "Quality",
      "Ops",
      "Stats",
      "License",
      "Alerts",
      "Sleep",
      "HW",
    ],
  },
  {
    level: "QA Engineer",
    values: [
      "Home",
      "Quality",
      "Ops",
      "Stats",
      "License",
      "Alerts",
      "Sleep",
      "HW",
    ],
  },
];

export default allLevels;
