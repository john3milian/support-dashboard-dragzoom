import moment from "moment";

export const fetchData = (
  API,
  userId,
  setSleepDataList,
  setSleepIdList,
  setStatus,
  setAllData,
  setStagesData,
  to,
  from
) => {
  API.post(
    `/api/v1/sleep/list?userId=${userId}&fromTime=${from}&toTime=${to}`,
    {
      from,
      fields: ["sleepId", "WakeupTime", "SleepTime", "OnBedTime"],
    }
  )
    .then((res) => res.data)
    .then((res) => {
      if (!Array.isArray(res)) {
        throw new Error("No data");
      }
      if (res.length < 1) {
        throw new Error("No data");
      }
      res = res.slice(0, 5);
      setSleepDataList(res);
      const temp_res = [];

      res.map(({ SleepId, WakeupTime, SleepTime, Properties }) =>
        temp_res.push({
          sleepId: SleepId,
          WakeupTime: WakeupTime,
          SleepTime: SleepTime,
          OnBedTime: Properties["OnBedTime"],
        })
      );

      console.log(res);
      res = temp_res;
      res.sort((a, b) => (a.OnBedTime < b.OnBedTime ? 1 : -1));
      const slicedList = res.map(({ sleepId }) => sleepId).slice(0, 5);
      console.log(slicedList);
      setSleepIdList(slicedList);
      console.log(res);
      return res;
    })
    .then((arr) =>
      Promise.all([
        ...arr.map(({ OnBedTime, WakeupTime }) => {
          let from_date = new Date(moment.utc(OnBedTime)) / 1000;
          let to_date = new Date(moment.utc(WakeupTime)) / 1000;
          if (isNaN(to_date) || isNaN(from_date)) {
            return [];
          } else {
            return API.get(
              `/api/v1/sleeprecords?userId=${userId}&from=${from_date}&to=${to_date}`
            )
              .then((res) => res.data.reverse())
              .catch((err) => err);
          }
        }),
      ]).then(async (res) => {
        setStatus(true, true);
        setAllData(res);
      })
    )
    .catch((err) => {
      console.log(err);
      setStatus(true, false);
    });
};

export function createButton(arr, Button, setIndexSelected, indexSelected) {
  console.log("THIS ARE THE DATESSSSSSSS", arr);
  return arr.map((item, index) => {
    // const time = parseInt(item.bedTime) * 1000;
    const time = parseInt(item.WakeupTime);
    // const key=time;
    const label = new Date(item.WakeupTime)
      .toString()
      .split(" ")
      .slice(1, 3)
      .reverse()
      .join("-");
    return (
      <Button
        key={time + label + index}
        onClick={() => {
          console.log(index);
          setIndexSelected(index);
        }}
        color='primary'
        style={{ outline: 0 }}
        variant={index === indexSelected ? "contained" : "outlined"}
        className='ml-1'>
        {isNaN(time) ? "Invalid Date" : label}
      </Button>
    );
  });
}
