import API from "../api";

export const fetchAllOrgs = async (
  setOrgNameSelection,
  setError,
  setErrorMsg
) => {
  API.get(`/api/orgs/get`)
    .then((orgResults) => {
      const orgBody = orgResults.data;
      orgBody.unshift({ OrganizationId: "", OrganizationName: "All" });
      setOrgNameSelection(orgBody);
    })
    .catch((error) => {
      setError(true);
      setErrorMsg(error.response.data.Action);
    });
};

export const sortUsingColor = (alertsData, setAlertsData) => {
  let redList = [];
  let greenList = [];
  let yellowList = [];
  let noneList = [];
  alertsData.map((eachAlert) => {
    if (eachAlert?.ColorCode?.toLowerCase() === "red") {
      redList.push(eachAlert);
    } else if (eachAlert?.ColorCode?.toLowerCase() === "yellow") {
      yellowList.push(eachAlert);
    } else if (eachAlert?.ColorCode?.toLowerCase() === "green") {
      greenList.push(eachAlert);
    } else {
      noneList.push(eachAlert);
    }
  });

  redList.sort((a, b) => {
    if (a["CreatedAt"] > b["CreatedAt"]) {
      return -1;
    } else {
      return 1;
    }
  });
  yellowList.sort((a, b) => {
    if (a["CreatedAt"] > b["CreatedAt"]) {
      return -1;
    } else {
      return 1;
    }
  });
  greenList.sort((a, b) => {
    if (a["CreatedAt"] > b["CreatedAt"]) {
      return -1;
    } else {
      return 1;
    }
  });
  noneList.sort((a, b) => {
    if (a["CreatedAt"] > b["CreatedAt"]) {
      return -1;
    } else {
      return 1;
    }
  });

  const combinedList = [...redList, ...yellowList, ...greenList, ...noneList];

  setAlertsData(combinedList);
};

export const fetchData = async (
  orgId,
  setLoading,
  setError,
  setAlertsData,
  setSecondayAlertsData,
  setSortByColor,
  setErrorMsg
) => {
  setLoading(true);
  let postText = "";

  var date = new Date();
  date.setHours(date.getHours() - 4);
  var from = date.toISOString().slice(0, 19);

  orgId.map(
    (eachId) => (postText = postText + `&organizationId=${eachId.trim()}`)
  );
  try {
    API.post(
      // `https://alerts.senslabs.io/api/smartalerts/list?from=2021-04-10T00:00:00${postText}`
      `/api/smartalerts/list`,
      {
        Parameters: `from=${from}${postText}`,
      }
    )
      .then((results) => {
        let body = results.data;
        body = body
          .filter((eachItem) => {
            if (eachItem["OrganizationNames"].includes("COVID Care")) {
              return eachItem["OrganizationNames"].includes("Dozee");
            } else {
              return !eachItem["OrganizationNames"].includes("Dozee");
            }
          })
          .sort((a, b) => {
            if (a["CreatedAt"] > b["CreatedAt"]) {
              return -1;
            } else {
              return 1;
            }
          });

        const body2 = body.filter((eachItem) => {
          return (
            eachItem.Escalations?.Type &&
            Object.values(eachItem.Escalations)[1].toLowerCase() === "valid"
          );
          // return eachItem["UserName"].toLowerCase().includes(username);
        });

        setAlertsData(body2);
        setSecondayAlertsData(body2);
        setLoading(false);
        setSortByColor(false);
        localStorage.setItem("editedRemark", "");
      })
      .catch((error) => {
        setError(true);
        setErrorMsg(error.response.data.Action);
      });
  } catch {
    setError(true);
    setErrorMsg("Do not have access");
  }
};
