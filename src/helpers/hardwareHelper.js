import { Box, Typography } from "@material-ui/core";

export function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}>
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

export const getRSSData = (value) => {
  try {
    // Assume we give it "WIFICONNECTED rssi = -41"
    const test2 = value.split("="); // will give us ==> ["WIFICONNECTED rssi","-41"]

    /* const test3 = test2[1].split(')')
     */
    const test3 = parseInt(test2[1].trim()); // We change the " -41" to ==> "-41" (removing extra spaces)

    return -1 * test3; // finally taking modulus of the negative number
  } catch {
    return value;
  }
};

export const getAutoUploadStatus = (value) => {
  try {
    const test2 = value.split(" "); // gets value like ==> AUTOUPLOAD 12

    /* const test3 = test2[1].split(')')
     */

    return parseInt(test2[1].trim()); // returns ==> 12
  } catch {
    return value;
  }
};

export const getUploadQData = (value) => {
  // For comments purpose, assume we give it UPLOADQ(0)
  try {
    const stage2 = value.split("("); // This gives us result ==> ["UPLOADQ", "0)"]

    const test3 = stage2[1].split(")"); // This gives us result ==> ["0", ""]

    return parseInt(test3[0]);
  } catch {
    return value;
  }
};

export function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export const autoCompleteDOZNumber = (deviceIdentifier, prefix) => {
  let auto_append_zeros = "0".repeat(4 - deviceIdentifier.length);
  if (auto_append_zeros.length > 0) {
    return `deviceInfoByName?devicename=${prefix}-${auto_append_zeros}${deviceIdentifier}`;
  } else {
    return `deviceInfoByName?devicename=${prefix}-${deviceIdentifier}`;
  }
};

export const autoCompleteDPNumber = (deviceIdentifier, prefix) => {
  let auto_append_zeros = "0".repeat(5 - deviceIdentifier.length);
  if (auto_append_zeros.length > 0) {
    return `deviceInfoByName?devicename=${prefix}-${auto_append_zeros}${deviceIdentifier}`;
  } else {
    return `deviceInfoByName?devicename=${prefix}-${deviceIdentifier}`;
  }
};

export const formatData = (data, moment, setLogData, setShowLoader) => {
  let temp_list = [];
  let temp_pon_value = 100;
  let count = 0;
  data.map((eachValue) => {
    if (eachValue["_source"]["PowerStatus"]) {
      count = 0;
      temp_pon_value = 0;
    }
    count = count + 1;
    if (count > 2) {
      temp_pon_value = 100;
    }

    let temp_value = {
      "@timestamp": moment(eachValue["_source"]["@timestamp"])
        .local()
        .format("YYYY-MM-DD HH:mm:ss"),
      time: moment(eachValue["_source"]["@timestamp"])
        .local()
        // .format("Do-MMM h:mm"), // This gives us 6th-May 11:40 am
        .format("Do HH:mm"),
      AutoUploadStatus: getAutoUploadStatus(
        eachValue["_source"]["AutoUploadStatus"]
      ),
      UploadQueue: getUploadQData(eachValue["_source"]["UploadQueue"]),
      WiFiStatus: getRSSData(eachValue["_source"]["WiFiStatus"]),
      DozeeStatus: eachValue["_source"]["DozeeStatus"],
      PowerStatus: temp_pon_value,
    };

    temp_list.push(temp_value);
  });
  console.log(temp_list);

  temp_list.reverse();
  setLogData(temp_list);
  setShowLoader(false);
};
