import { Fragment } from "react";
// import {Link} from 'react-router-dom';
import { format } from "date-fns";

export const COLUMNS = [
  {
    Header: "Identifier",
    accessor: "identifier",
  },
  {
    Header: "Header Time",
    accessor: "headerTimeStamp",
    Cell: (data) => {
      if (!data.value) {
        return <></>;
      }
      const d = new Date(+data.value * 1000);
      const fixedDate = offSetFix(d);
      const fixedTime = getTimeStamp(fixedDate);
      // return <>{getDateStamp(offSetFix(d))} [{fixedTime}]</>
      return format(d, "dd/MMM/yy [HH:mm:ss]");
    },
  },
  {
    Header: "Uploaded @",
    accessor: "uploadedAt",
    Cell: (data) => {
      const d = new Date(data.value);
      const fixedDate = new Date(offSetFix(d));
      return format(fixedDate, "dd/MMM/yy - [HH:mm:ss]");
    },
  },
  {
    Header: "Processing Start",
    accessor: "startTime",
    Cell: (data) => {
      if (!data.value) return <></>;
      const uploaded = new Date(offSetFix(new Date(data.row.cells[2].value)));
      const uploaded_time = new Date(uploaded).getTime();
      const started = new Date(data.value).getTime();
      const secsToStart = Math.round((started - uploaded_time) / 1000);
      const mins = Math.floor(secsToStart / 60);
      const secs = Math.round(secsToStart % 60);
      let timeStr = `${mins ? mins : 0}m ${secs ? secs : 0}s`;
      return (
        <>
          +{timeStr} [{getTimeStamp(started)}]
        </>
      );
    },
  },
  {
    Header: "Processing End",
    accessor: "endTime",
    Cell: (data) => {
      if (!data.value) return <></>;
      const started = new Date(data.row.cells[3].value).getTime();
      const ended = new Date(data.value).getTime();
      const secsToEnd = Math.round((ended - started) / 1000);

      return (
        <>
          +{secsToEnd}s [{new Date(ended).toTimeString().split(" ")[0]}]
        </>
      );
      // if (!isNaN(ended))
      //   if (secsToEnd > 4) {
      //     return (
      //       <span className='secondsAreLessThan4'>
      //         +{secsToEnd}s [{new Date(ended).toTimeString().split(" ")[0]}]
      //       </span>
      //     );
      //   } else {
      //     return (
      //       <>
      //         +{secsToEnd}s [{new Date(ended).toTimeString().split(" ")[0]}]
      //       </>
      //     );
      //   }
      // return <></>;
    },
  },
  {
    Header: "DB Insert",
    accessor: "dbInsert",
  },
  {
    Header: "Status",
    accessor: "status",
  },
  {
    Header: "Sleep #",
    accessor: "sleepId",
  },
];

function isValidDate(date) {
  const dateValue = new Date(date).toString();
  if (dateValue !== "Invalid Date") {
    return true;
  }
  return false;
}

function isValidNumber(value) {
  return parseInt(value) > 1478009120 && !isNaN(value);
}

function getDate(value) {
  if (isValidDate(value) || isValidNumber(value)) {
    return generateDate(value);
  } else {
    return <></>;
  }
}

function generateDate(value) {
  const adjustedDate = parseInt(value) * 1000;
  // console.log('adjustedDate: ', adjustedDate);
  const date = new Date(new Date(adjustedDate).toISOString());
  return format(date, "dd MMM yy");
}

// function offSetFix(time){
// 	return new Date(new Date(time) + new Date().getTimezoneOffset())
// }

function offSetFix(time) {
  const t = new Date(time).getTime();
  const diff = new Date().getTimezoneOffset() * 60 * 1000 * -1;
  const fixedTime = t + diff;
  return fixedTime;
}

function getTimeStamp(time) {
  const t = new Date(time);
  return t.toString().split(" ")[4];
}

function getLast2Digits(str) {
  return str.slice(-2);
}

function getDateStamp(time) {
  const d = new Date(time);
  const dateStamp = `${d.toString().split(" ")[2]}/${
    d.toString().split(" ")[1]
  }/${getLast2Digits(d.toString().split(" ")[3])}`;
  return dateStamp;
}
