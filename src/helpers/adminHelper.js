import API from '../api'

export const getAllUsers = (setUsers, setError, setErrorMsg) => {
    API.get(`/api/v1/user/list`).then((response) => {
        if(response.status === 200) {
          const users = response.data;
          setUsers(users)
        }
        else if (response.status) {
          setError(true)
          setErrorMsg('There was a problem while fetching the Users List')
        }
      }).catch((error) => {
        setError(true)
        setErrorMsg('There was a problem while fetching the Users List')
      })
}

export const addNewUser = (Access, Cred, setError, setErrorMsg) => {

  const bodyObj = {
    Access: Access,
    Cred: Cred,
  };

  API.post(
    `api/v1/user/add`,bodyObj
  ).then((response) => {
    console.log(response)
    if(response.status === 200) {
      const body = response.data;
        }
        else if (response.status) {
          setError(true)
          setErrorMsg('The User was not Added.')
        }
    }).catch((error) => {
        setError(true)
        setErrorMsg('The User could not be Added.')
      })

};

export const deleteUser = (email, setError, setErrorMsg) => {
  
  const bodyObj = {
    Cred : email
  }

  API.post(
    `api/v1/user/delete`, bodyObj
  ).then((response) => {
    if(response.status === 200) {
      const body = response.data;
        }
        else if (response.status) {
          setError(true)
          setErrorMsg('There was a problem Deleting the User')
        }
  }).catch((error) => {
    setError(true)
    setErrorMsg('There was a problem Deleting the User')
  })
}