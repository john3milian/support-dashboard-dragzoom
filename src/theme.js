import { createMuiTheme } from "@material-ui/core";
import { deepOrange } from "@material-ui/core/colors";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#2b6ca3",
      // main: "#37667E",
      // main: "#2f5f7e",
    },
    secondary: deepOrange,
  },
  typography: {
    fontFamily: [
      '"Helvetica Neue"',
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
});

export default theme;
